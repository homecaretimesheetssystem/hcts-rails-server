require 'test_helper'

class PcasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pca = pcas(:one)
  end

  test "should get index" do
    get pcas_url
    assert_response :success
  end

  test "should get new" do
    get new_pca_url
    assert_response :success
  end

  test "should create pca" do
    assert_difference('Pca.count') do
      post pcas_url, params: { pca: { agency_id: @pca.agency_id, available: @pca.available, company_assigned_id: @pca.company_assigned_id, firstName: @pca.firstName, gender: @pca.gender, home_address: @pca.home_address, lastName: @pca.lastName, phoneNumber: @pca.phoneNumber, umpi: @pca.umpi, user_id: @pca.user_id, years_of_experience: @pca.years_of_experience } }
    end

    assert_redirected_to pca_url(Pca.last)
  end

  test "should show pca" do
    get pca_url(@pca)
    assert_response :success
  end

  test "should get edit" do
    get edit_pca_url(@pca)
    assert_response :success
  end

  test "should update pca" do
    patch pca_url(@pca), params: { pca: { agency_id: @pca.agency_id, available: @pca.available, company_assigned_id: @pca.company_assigned_id, firstName: @pca.firstName, gender: @pca.gender, home_address: @pca.home_address, lastName: @pca.lastName, phoneNumber: @pca.phoneNumber, umpi: @pca.umpi, user_id: @pca.user_id, years_of_experience: @pca.years_of_experience } }
    assert_redirected_to pca_url(@pca)
  end

  test "should destroy pca" do
    assert_difference('Pca.count', -1) do
      delete pca_url(@pca)
    end

    assert_redirected_to pcas_url
  end
end
