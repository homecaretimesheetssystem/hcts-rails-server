require 'test_helper'

class VerificationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @verification = verifications(:one)
  end

  test "should get index" do
    get verifications_url
    assert_response :success
  end

  test "should get new" do
    get new_verification_url
    assert_response :success
  end

  test "should create verification" do
    assert_difference('Verification.count') do
      post verifications_url, params: { verification: { address: @verification.address, archivedPca_id: @verification.archivedPca_id, archivedRecipient_id: @verification.archivedRecipient_id, current: @verification.current, end_date: @verification.end_date, first_time: @verification.first_time, frequency: @verification.frequency, latitude: @verification.latitude, longitude: @verification.longitude, recipientPca_id: @verification.recipientPca_id, start_date: @verification.start_date, time: @verification.time, verification_date: @verification.verification_date, verification_photo: @verification.verification_photo, verified: @verification.verified } }
    end

    assert_redirected_to verification_url(Verification.last)
  end

  test "should show verification" do
    get verification_url(@verification)
    assert_response :success
  end

  test "should get edit" do
    get edit_verification_url(@verification)
    assert_response :success
  end

  test "should update verification" do
    patch verification_url(@verification), params: { verification: { address: @verification.address, archivedPca_id: @verification.archivedPca_id, archivedRecipient_id: @verification.archivedRecipient_id, current: @verification.current, end_date: @verification.end_date, first_time: @verification.first_time, frequency: @verification.frequency, latitude: @verification.latitude, longitude: @verification.longitude, recipientPca_id: @verification.recipientPca_id, start_date: @verification.start_date, time: @verification.time, verification_date: @verification.verification_date, verification_photo: @verification.verification_photo, verified: @verification.verified } }
    assert_redirected_to verification_url(@verification)
  end

  test "should destroy verification" do
    assert_difference('Verification.count', -1) do
      delete verification_url(@verification)
    end

    assert_redirected_to verifications_url
  end
end
