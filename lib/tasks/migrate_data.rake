namespace :hcts do
  desc "Deserialize fields for association"
  task migrate_data: :environment do
    users = User.all
    if users.present?
      users.each do |user|
        user.role = Role.where(old_name: user.get_role)
      end
    end
  end
end