# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181129130241) do

  create_table "access_token", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.integer "device_id"
    t.string "token"
    t.string "expires_at"
    t.string "scope"
    t.integer "client_id"
  end

  create_table "admin", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "firstName"
    t.string "lastName"
  end

  create_table "agency", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "state_id"
    t.string "agency_name"
    t.string "week_start_date"
    t.string "start_time"
    t.string "phoneNumber"
    t.string "imageName"
    t.integer "plan"
    t.string "stripe_customer_id"
    t.string "stripe_source"
    t.string "stripe_subscription_id"
    t.datetime "updatedAt"
    t.string "stripe_sub_name"
    t.boolean "pca_finder_active"
  end

  create_table "agency_services", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "agency_id"
    t.integer "services_id"
  end

  create_table "auth_code", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "client_id"
    t.integer "user_id"
    t.string "token"
    t.string "redirect_uri"
    t.integer "expires_at"
    t.string "scope"
  end

  create_table "care_goals", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "recipient_id"
    t.boolean "set_by_recipient"
    t.integer "setByAgency_id"
    t.integer "setByAdmin_id"
  end

  create_table "caregoals_careoptions", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "caregoals_id"
    t.integer "careoptions_id"
  end

  create_table "careoption_timesheet", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "timesheet_id"
    t.integer "careOption_id"
  end

  create_table "careoptions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.boolean "is_iadl"
    t.string "care_option"
  end

  create_table "client", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "random_id"
    t.string "redirect_uris"
    t.string "secret"
    t.string "allowed_grant_types"
  end

  create_table "county", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
  end

  create_table "device", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.boolean "isAndroid"
    t.boolean "isIos"
    t.boolean "isActive"
    t.string "deviceToken"
  end

  create_table "email_settings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "agency_id"
    t.string "email"
  end

  create_table "files", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "timesheet_id"
    t.string "timesheetCsvFile"
    t.string "timesheetPdfFile"
    t.string "timesheetRecSig"
    t.string "timesheetPcaSig"
  end

  create_table "fos_user", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "agency_id"
    t.integer "admin_id"
    t.string "username"
    t.string "username_canonical"
    t.string "email"
    t.string "email_canonical"
    t.boolean "enabled", default: true
    t.string "salt"
    t.string "password"
    t.datetime "last_login"
    t.boolean "locked", default: false
    t.boolean "expired", default: false
    t.datetime "expires_at"
    t.string "confirmation_token"
    t.datetime "password_requested_at"
    t.string "roles"
    t.boolean "credentials_expired"
    t.datetime "credentials_expired_at"
    t.string "firstName"
    t.string "lastName"
    t.boolean "archived", default: false
  end

  create_table "image", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "path"
    t.string "originalName"
    t.string "mimeType"
  end

  create_table "pca", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.integer "agency_id"
    t.string "pca_name"
    t.string "firstName"
    t.string "lastName"
    t.string "umpi"
    t.string "company_assigned_id"
    t.string "phoneNumber"
    t.datetime "createdAt"
    t.datetime "updatedAt"
    t.string "home_address"
    t.boolean "available"
    t.integer "years_of_experience"
    t.integer "gender"
    t.string "profile_photo"
    t.string "about_me"
  end

  create_table "pca_county", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "pca_id"
    t.integer "county_id"
  end

  create_table "pca_qualifications", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "pca_id"
    t.integer "qualifications_id"
  end

  create_table "pca_services", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "pca_id"
    t.integer "services_id"
  end

  create_table "pca_signature", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "timesheet_id"
    t.datetime "pcaSigTime"
    t.string "pcaSigAddress"
    t.string "latitude"
    t.string "longitude"
    t.datetime "pcaPhotoTime"
  end

  create_table "qualifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "qualification"
  end

  create_table "ratio", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "ratio"
  end

  create_table "recipient", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.integer "agency_id"
    t.boolean "skip_verification"
    t.boolean "skip_gps"
    t.string "recipient_name"
    t.string "first_name"
    t.string "last_name"
    t.string "ma_number"
    t.string "company_assigned_id"
    t.integer "gender_preference"
    t.integer "county_id"
    t.string "home_address"
  end

  create_table "recipient_pca", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "pca_id"
    t.integer "recipient_id"
    t.boolean "is_temp"
  end

  create_table "recipient_services", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "recipient_id"
    t.integer "service_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recipient_signature", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "timesheet_id"
    t.datetime "recipientSigTime"
    t.string "recipientSigAddress"
    t.string "latitude"
    t.string "longitude"
    t.datetime "recipientPhotoTime"
  end

  create_table "refresh_token", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "client_id"
    t.integer "user_id"
    t.string "token"
    t.integer "expires_at"
    t.string "scope"
  end

  create_table "request", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "recipient_id"
    t.string "address"
    t.integer "gender_preference"
    t.datetime "start_shift_time"
    t.datetime "end_shift_time"
    t.integer "years_of_experience"
    t.integer "requested_id"
    t.string "description"
  end

  create_table "request_qualifications", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "request_id"
    t.integer "qualifications_id"
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "old_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rsvp", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "pca_id"
    t.integer "request_id"
    t.datetime "eta"
    t.boolean "accepted"
    t.boolean "declined"
    t.string "cancellation_reason"
    t.integer "sessionData_id"
  end

  create_table "services", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "serviceNumber"
    t.string "serviceName"
  end

  create_table "session_data", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "pca_id"
    t.integer "service_id"
    t.integer "ratio_id"
    t.integer "continueTimesheetNumber"
    t.integer "currentTimesheetNumber"
    t.boolean "flagForFurtherInvestigation"
    t.integer "currentTimesheetId"
    t.datetime "dateEnding"
    t.integer "timeIn_id"
    t.integer "timeOut_id"
    t.integer "sharedCareLocation_id"
  end

  create_table "shared_care_location", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "location"
  end

  create_table "states", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "stateName"
  end

  create_table "subscriptions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "time_in", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "timeIn"
    t.bigint "estimatedShiftLength"
    t.string "timeInAddress"
    t.string "latitude"
    t.string "longitude"
    t.datetime "timeInPictureTime"
  end

  create_table "time_out", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "timeOut"
    t.string "timeOutAddress"
    t.string "latitude"
    t.string "longitude"
    t.datetime "timeOutPictureTime"
    t.bigint "totalHours"
    t.bigint "billableHours"
  end

  create_table "timesheet", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "recipient_id"
    t.integer "verification_id"
    t.boolean "finished", default: false
    t.boolean "archived", default: false
    t.integer "sessionData_id"
  end

  create_table "unsignedtimesheetemail", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "timesheet_id"
    t.datetime "nextEmailSendDate"
  end

  create_table "verification", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "time"
    t.string "address"
    t.string "latitude"
    t.string "longitude"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "verification_date"
    t.integer "frequency"
    t.datetime "created_at"
    t.string "verification_photo"
    t.boolean "first_time"
    t.boolean "current"
    t.boolean "verified"
    t.integer "recipientPca_id"
    t.integer "archivedRecipient_id"
    t.integer "archivedPca_id"
  end

end
