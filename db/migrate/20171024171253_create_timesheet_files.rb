class CreateTimesheetFiles < ActiveRecord::Migration[5.1]
  def change
    create_table :files do |t|
      t.integer :timesheet_id
      t.string :timesheetCsvFile
      t.string :timesheetPdfFile
      t.string :timesheetRecSig
      t.string :timesheetPcaSig
    end
  end
end
