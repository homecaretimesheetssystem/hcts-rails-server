class CreateTimeOuts < ActiveRecord::Migration[5.1]
  def change
    create_table :time_out do |t|
      t.datetime :timeOut
      t.string :timeOutAddress
      t.string :latitude
      t.string :longitude
      t.datetime :timeOutPictureTime
      t.integer :totalHours, limit:8
      t.integer :billableHours, limit:8
    end
  end
end
