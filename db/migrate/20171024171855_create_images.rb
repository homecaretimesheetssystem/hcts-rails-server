class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :image do |t|
      t.string :path
      t.string :originalName
      t.string :mimeType
    end
  end
end
