class CreateCareoptions < ActiveRecord::Migration[5.1]
  def change
    create_table :careoptions do |t|
      t.boolean :is_iadl
      t.string :care_option
    end
  end
end
