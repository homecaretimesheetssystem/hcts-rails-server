class CreatePcaServices < ActiveRecord::Migration[5.1]
  def change
    create_table :pca_services do |t|
      t.integer :pca_id
      t.integer :services_id
    end
    remove_column :pca_services, :id
  end
end
