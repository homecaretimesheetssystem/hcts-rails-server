class CreateCaregoalsCareoptions < ActiveRecord::Migration[5.1]
  def change
    create_table :caregoals_careoptions do |t|
      t.integer :caregoals_id
      t.integer :careoptions_id
    end
    remove_column :caregoals_careoptions, :id
  end
end
