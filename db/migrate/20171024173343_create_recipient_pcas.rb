class CreateRecipientPcas < ActiveRecord::Migration[5.1]
  def change
    create_table :recipient_pca do |t|
      t.integer :pca_id
      t.integer :recipient_id
      t.boolean :is_temp
    end
  end
end
