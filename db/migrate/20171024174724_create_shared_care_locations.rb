class CreateSharedCareLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :shared_care_location do |t|
      t.string :location
    end
  end
end
