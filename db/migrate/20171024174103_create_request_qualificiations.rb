class CreateRequestQualificiations < ActiveRecord::Migration[5.1]
  def change
    create_table :request_qualifications do |t|
      t.integer :request_id
      t.integer :qualifications_id
    end
    remove_column :request_qualifications, :id
  end
end
