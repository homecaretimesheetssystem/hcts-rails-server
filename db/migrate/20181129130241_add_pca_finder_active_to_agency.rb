class AddPcaFinderActiveToAgency < ActiveRecord::Migration[5.1]
  def change
    add_column :agency, :pca_finder_active, :boolean
  end
end
