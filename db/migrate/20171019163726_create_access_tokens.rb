class CreateAccessTokens < ActiveRecord::Migration[5.1]
  def change
    create_table :access_token do |t|
      t.integer :user_id
      t.integer :device_id
      t.string :token
      t.string :expires_at
      t.string :scope
    end
  end
end
