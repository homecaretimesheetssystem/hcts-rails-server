class CreateAgencies < ActiveRecord::Migration[5.1]
  def change
    create_table :agency do |t|
      t.integer :state_id
      t.string :agency_name
      t.string :week_start_date
      t.string :start_time
      t.string :phoneNumber
      t.string :imageName
      t.integer :plan
      t.string :stripe_customer_id
      t.string :stripe_source
      t.string :stripe_subscription_id
      t.datetime :updatedAt
    end
  end
end
