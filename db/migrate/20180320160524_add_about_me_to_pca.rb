class AddAboutMeToPca < ActiveRecord::Migration[5.1]
  def change
    add_column :pca, :about_me, :string
    add_column :request, :description, :string
  end
end
