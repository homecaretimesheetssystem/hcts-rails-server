class CreateSessionData < ActiveRecord::Migration[5.1]
  def change
    create_table :session_data do |t|
      t.integer :pca_id
      t.integer :service_id
      t.integer :ratio_id
      t.integer :continueTimesheetNumber
      t.integer :currentTimesheetNumber
      t.boolean :flagForFurtherInvestigation
      t.integer :currentTimesheetId
      t.datetime :dateEnding
      t.integer :timeIn_id
      t.integer :timeOut_id
      t.integer :sharedCareLocation_id
    end
  end
end
