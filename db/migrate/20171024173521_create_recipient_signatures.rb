class CreateRecipientSignatures < ActiveRecord::Migration[5.1]
  def change
    create_table :recipient_signature do |t|
      t.integer :timesheet_id
      t.datetime :recipientSigTime
      t.string :recipientSigAddress
      t.string :latitude
      t.string :longitude
      t.datetime :recipientPhotoTime
    end
  end
end
