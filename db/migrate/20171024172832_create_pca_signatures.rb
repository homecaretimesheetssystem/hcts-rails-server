class CreatePcaSignatures < ActiveRecord::Migration[5.1]
  def change
    create_table :pca_signature do |t|
      t.integer :timesheet_id
      t.datetime :pcaSigTime
      t.string :pcaSigAddress
      t.string :latitude
      t.string :longitude
      t.datetime :pcaPhotoTime
    end
  end
end
