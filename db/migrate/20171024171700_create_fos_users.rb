class CreateFosUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :fos_user do |t|
      t.integer :agency_id
      t.integer :admin_id
      t.string :username
      t.string :username_canonical
      t.string :email
      t.string :email_canonical
      t.boolean :enabled
      t.string :salt
      t.string :password
      t.datetime :last_login
      t.boolean :locked
      t.boolean :expired
      t.datetime :expires_at
      t.string :confirmation_token
      t.datetime :password_requested_at
      t.string :roles
      t.boolean :credentials_expired
      t.datetime :credentials_expire_at
      t.string :firstName
      t.string :lastName
      t.boolean :archived
    end
  end
end
