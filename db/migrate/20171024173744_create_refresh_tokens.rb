class CreateRefreshTokens < ActiveRecord::Migration[5.1]
  def change
    create_table :refresh_token do |t|
      t.integer :client_id
      t.integer :user_id
      t.string :token
      t.integer :expires_at
      t.string :scope
    end
  end
end
