class CreateAdmins < ActiveRecord::Migration[5.1]
  def change
    create_table :admin do |t|
      t.string :firstName
      t.string :lastName
    end
  end
end
