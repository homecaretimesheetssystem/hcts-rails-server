class CreateEmailSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :email_settings do |t|
      t.integer :agency_id
      t.string :email
    end
  end
end
