class CreateUnsignedtimesheetemails < ActiveRecord::Migration[5.1]
  def change
    create_table :unsignedtimesheetemail do |t|
      t.integer :timesheet_id
      t.datetime :nextEmailSendDate
    end
  end
end
