class AddStripeSubNameToAgencies < ActiveRecord::Migration[5.1]
  def change
    add_column :agency, :stripe_sub_name, :string
  end
end
