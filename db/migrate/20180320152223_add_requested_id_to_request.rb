class AddRequestedIdToRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :request, :requested_id, :integer
  end
end
