class CreateTimeIns < ActiveRecord::Migration[5.1]
  def change
    create_table :time_in do |t|
      t.datetime :timeIn
      t.integer :estimatedShiftLength, limit: 8
      t.string :timeInAddress
      t.string :latitude
      t.string :longitude
      t.datetime :timeInPictureTime
    end
  end
end
