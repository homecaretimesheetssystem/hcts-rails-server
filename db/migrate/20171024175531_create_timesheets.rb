class CreateTimesheets < ActiveRecord::Migration[5.1]
  def change
    create_table :timesheet do |t|
      t.integer :recipient_id
      t.integer :verification_id
      t.boolean :finished
      t.boolean :archived
      t.integer :sessionData_id
    end
  end
end
