class CreateRecipients < ActiveRecord::Migration[5.1]
  def change
    create_table :recipient do |t|
      t.integer :user_id
      t.integer :agency_id
      t.boolean :skip_verification
      t.boolean :skip_gps
      t.string :recipient_name
      t.string :first_name
      t.string :last_name
      t.string :ma_number
      t.string :company_assigned_id
      t.integer :gender_preference
      t.integer :county_id
      t.string :home_address
    end
  end
end
