class CreateRecipientServices < ActiveRecord::Migration[5.1]
  def change
    create_table :recipient_services do |t|
      t.integer :recipient_id
      t.integer :service_id

      t.timestamps
    end
  end
end
