class CreateAgencyServices < ActiveRecord::Migration[5.1]
  def change
    create_table :agency_services do |t|
      t.integer :agency_id
      t.integer :services_id
    end
    remove_column :agency_services, :id
  end
end
