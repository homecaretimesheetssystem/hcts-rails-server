class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services do |t|
      t.integer :serviceNumber
      t.string :serviceName
    end
  end
end
