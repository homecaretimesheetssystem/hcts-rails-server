class CreatePcaQualifications < ActiveRecord::Migration[5.1]
  def change
    create_table :pca_qualifications do |t|
      t.integer :pca_id
      t.integer :qualifications_id
    end
    remove_column :pca_qualifications, :id
  end
end
