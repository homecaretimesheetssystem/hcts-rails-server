class AddEnabledDefaultToUsers < ActiveRecord::Migration[5.1]
  def change
    change_column :fos_user, :enabled, :boolean, default: true
  end
end
