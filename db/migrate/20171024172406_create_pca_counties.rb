class CreatePcaCounties < ActiveRecord::Migration[5.1]
  def change
    create_table :pca_county do |t|
      t.integer :pca_id
      t.integer :county_id
    end
    remove_column :pca_county, :id
  end
end
