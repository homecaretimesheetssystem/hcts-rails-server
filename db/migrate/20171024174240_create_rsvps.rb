class CreateRsvps < ActiveRecord::Migration[5.1]
  def change
    create_table :rsvp do |t|
      t.integer :pca_id
      t.integer :request_id
      t.datetime :eta
      t.boolean :accepted
      t.boolean :declined
      t.string :cancellation_reason
      t.integer :sessionData_id
    end
  end
end
