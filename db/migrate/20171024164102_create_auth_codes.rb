class CreateAuthCodes < ActiveRecord::Migration[5.1]
  def change
    create_table :auth_code do |t|
      t.integer :client_id
      t.integer :user_id
      t.string :token
      t.string :redirect_uri
      t.integer :expires_at
      t.string :scope
    end
  end
end
