class CreateVerifications < ActiveRecord::Migration[5.1]
  def change
    create_table :verification do |t|
      t.datetime :time
      t.string :address
      t.string :latitude
      t.string :longitude
      t.datetime :start_date
      t.datetime :end_date
      t.datetime :verification_date
      t.integer :frequency
      t.datetime :created_at
      t.string :verification_photo
      t.boolean :first_time
      t.boolean :current
      t.boolean :verified
      t.integer :recipientPca_id
      t.integer :archivedRecipient_id
      t.integer :archivedPca_id
    end
  end
end
