class AddDefaultsToUsers < ActiveRecord::Migration[5.1]
  def change
    change_column :fos_user, :locked, :boolean, default: false
    change_column :fos_user, :archived, :boolean, default: false
    change_column :fos_user, :expired, :boolean, default: false
  end
end
