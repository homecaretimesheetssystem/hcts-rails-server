class CreateDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :device do |t|
      t.integer :user_id
      t.boolean :isAndroid
      t.boolean :isIos
      t.boolean :isActive
      t.string :deviceToken
    end
  end
end
