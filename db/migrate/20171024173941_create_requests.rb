class CreateRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :request do |t|
      t.integer :recipient_id
      t.string :address
      t.integer :gender_preference
      t.datetime :start_shift_time
      t.datetime :end_shift_time
      t.integer :years_of_experience
    end
  end
end
