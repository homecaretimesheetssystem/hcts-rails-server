class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :client do |t|
      t.string :random_id
      t.string :redirect_uris
      t.string :secret
      t.string :allowed_grant_types
    end
  end
end
