class AddClientIdToAccessTokens < ActiveRecord::Migration[5.1]
  def change
    add_column :access_token, :client_id, :integer
  end
end
