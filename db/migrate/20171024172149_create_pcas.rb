class CreatePcas < ActiveRecord::Migration[5.1]
  def change
    create_table :pca do |t|
      t.integer :user_id
      t.integer :agency_id
      t.string :pca_name
      t.string :firstName
      t.string :lastName
      t.string :umpi
      t.string :company_assigned_id
      t.string :phoneNumber
      t.datetime :createdAt
      t.datetime :updatedAt
      t.string :home_address
      t.boolean :available
      t.integer :years_of_experience
      t.integer :gender
      t.string :profile_photo
    end
  end
end
