class CreateCareGoals < ActiveRecord::Migration[5.1]
  def change
    create_table :care_goals do |t|
      t.integer :recipient_id
      t.boolean :set_by_recipient
      t.integer :setByAgency_id
      t.integer :setByAdmin_id
    end
  end
end
