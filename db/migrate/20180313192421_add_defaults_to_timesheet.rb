class AddDefaultsToTimesheet < ActiveRecord::Migration[5.1]
  def change
    change_column :timesheet, :archived, :boolean, default: false
    change_column :timesheet, :finished, :boolean, default: false
  end
end
