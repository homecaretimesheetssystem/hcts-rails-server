# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
roles = Role.create([{name: 'Admin', old_name:'ROLE_ADMIN'}, {name:'Agency', old_name:'ROLE_AGENCY'}, {name:'PCA', old_name:'ROLE_PCA'}, {name:'Recipient', old_name:'ROLE_RECIPIENT'}])
