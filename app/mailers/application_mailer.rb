class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@homecaretimesheetsapp.com'
  layout 'mailer'
end
