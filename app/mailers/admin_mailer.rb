class AdminMailer < ApplicationMailer

 def send_password(user)
   @user = user
   mail(to:@user.email, subject:"HTCS Reset Requested")
 end

 def send_username(user)
   @user = user
   mail(to:@user.email, subject:"HCTS Authorization Request")
 end

end