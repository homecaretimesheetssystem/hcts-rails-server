class TimesheetMailer < ApplicationMailer


  def send_pdf_and_csv(timesheet)
    @emails = timesheet.agency.get_emails
    @subject = "Timesheet Pdf and Csv File"
    build_name = "#{timesheet.recipient.name}-#{timesheet.pca.name}-#{Timesheet.get_date_range(timesheet)}".gsub!(" ","")
    attachments["#{build_name}.pdf"] = File.read(timesheet.get_pdf[0])
    attachments["#{build_name}.csv"] = File.read(timesheet.get_csv[0])
    mail(to:@emails, subject:@subject)
  end

end
