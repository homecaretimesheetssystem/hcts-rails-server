class CareController < ApplicationController
  before_action :set_agency
  before_action :set_cache_headers, only: [:index]

  def index
    if params.has_key?("bar") && params[:bar] == "true"
      @bar = true
      if @recipient.present?
        @data = @recipient.bar_cares(@recipient.timesheets.active.graphable.pluck(:id))
      elsif params.has_key?("recipient_ids") && params[:recipient_ids].present?
        @data = Recipient.bar_cares(Timesheet.by_recipient(params[:recipient_ids]).active.graphable.pluck(:id))
      elsif params.has_key?("timesheet_ids") && params[:timesheet_ids].present?
        @data = Recipient.bar_cares(params[:timesheet_ids])
      elsif @agency.present?
        @data = @agency.bar_cares
      end
      @options = Recipient.barOptions
    else
      if params.has_key?("timesheet_ids") && params[:timesheet_ids].present?
        @timesheets = Timesheet.by_agency(@agency.id).where(id: params[:timesheet_ids]).active.finished.order_by_time_in
      elsif @recipient.present?
        @timesheets = @recipient.timesheets.active.finished.order_by_time_in
      elsif @agency.present?
        @timesheets = @agency.timesheets.active.finished.order_by_time_in
      else
        @timesheets = Timesheet.active.finished.order_by_time_in
      end
    end
  end




private
  def set_agency
    if params.has_key?("agency_id")          
      @agency = Agency.find_by(id:params[:agency_id])
    end
    if !@agency.present?
      if @current_user.is_admin?
        @agency = Agency.find_by(id:session[:current_agency_id])
      else
        @agency = @current_user.agency
      end
    end
    if params.has_key?("recipient_id") && params[:recipient_id].present?
      @recipient = Recipient.find_by(id:params[:recipient_id])
    end
  end
  
  def set_users
    if @agency.present?
      @recipients = @agency.recipients.needs_goals.sort{|x,y| x.name <=> y.name}
      @pcas = @agency.pcas.sort{|x,y| x.name <=> y.name}
    else
      @recipients = Recipient.needs_goals
      @pcas = Pca.all
    end
  end

  def set_cache_headers
    response.headers["Cache-Control"] = "no-cache, no-store"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

end
