class TimesheetsController < ApplicationController
  before_action :set_timesheet, only: [:show, :edit, :update, :destroy, :archive, :get_pca_sig, :get_rec_sig, :download_pdf, :download_csv, :download_pca_sig, :download_rec_sig]
  before_action :set_agency, only: [:new, :edit, :create, :update, :index]
  before_action :set_users, only: [:index, :new, :create, :edit, :update]

  # GET /timesheets
  # GET /timesheets.json
  def index
    @limit = 10
    if @agency.present?
      @timesheets = @agency.timesheets.viewable.order("session_data.dateEnding DESC")
    else
      @timesheets = Timesheet.viewable.order("session_data.dateEnding DESC")
    end
    if params.has_key?("archived") && params[:archived].present?
      case params[:archived]
         when "No"   
           @timesheets = @timesheets.active
         when "Yes"
           if @agency.present?
             @timesheets = @agency.timesheets.viewable_w_archive.order("session_data.dateEnding DESC")
           else
             @timesheets = Timesheet.viewable_w_archive.order("session_data.dateEnding DESC")
           end
         else
           if @agency.present?
             @timesheets = @agency.timesheets.archived.joins(:session_datum).order("session_data.dateEnding DESC")
           else
             @timesheets = Timesheet.archived.joins(:session_datum).order("session_data.dateEnding DESC")
           end
        end
    else
      @timesheets = @timesheets.active
    end
    if params.has_key?("finished") && params[:finished].present? && params[:finished] == "Yes"
      @timesheets = @timesheets.finished
    end
    if params.has_key?("verified") && params[:verified].present?
      if params[:verified] == "Yes"
        @timesheets = @timesheets.joins(:verification) 
      end
    end
    if params.has_key?("recipient") && params[:recipient].present?
      @timesheets = @timesheets.where(recipient_id: params[:recipient])
    end
    if params.has_key?("worker") && params[:worker].present?
      @timesheets = @timesheets.joins(:session_datum).where("session_data.pca_id = ?", params[:worker])
    end
    if params.has_key?("service") && params[:service].present?
      @timesheets = @timesheets.joins(:session_datum).where("session_data.service_id = ?", params[:service])
    end
    if params.has_key?("days_count") && params[:days_count].present?
      days_count = params[:days_count].to_i
      @timesheets = @timesheets.joins(:session_datum).where("session_data.dateEnding > ?", Time.now - days_count.days)
    end
    if params.has_key?("start_range") && params[:start_range].present? && params.has_key?("end_range") && params[:end_range].present?
      start_date = "#{params[:start_range][:year]}-#{params[:start_range][:month]}-#{params[:start_range][:day]}"
      @start_range = Date.parse(start_date)
      end_date = "#{params[:end_range][:year]}-#{params[:end_range][:month]}-#{params[:end_range][:day]}"
      @end_range = Date.parse(end_date)
      @timesheets = @timesheets.joins(:session_datum).where("session_data.dateEnding < ?", @end_range).where("session_data.dateEnding > ?", @start_range)
    end
    @count = @timesheets.count
    if @count == 0
       flash.now.alert = "No Time Entries found for your criteria"
    end
    if params.has_key?("button")
      redirect_to timesheets_path(utf8:true,finished:"Yes")
    end
    if params.has_key?("page")
      @page = Integer(params[:page]) rescue 1
    else
      @page = 1
    end 
    offset = (@page - 1) * @limit
    @timesheets = @timesheets.limit(@limit).offset(offset)
  end

  # GET /timesheets/1
  # GET /timesheets/1.json
  def show
  end

  # GET /timesheets/new
  def new
    if @agency.present? 
      @timesheet = Timesheet.new
      @sessionDatum = @timesheet.build_session_datum
      @sessionDatum.build_time_in(estimatedShiftLength:0)
    elsif @current_user.is_admin?
      redirect_to agencies_path, notice: "Select Agency to create Timesheets"
    else
      redirect_to root_path, alert: "Unauthorized"
    end
  end

  # GET /timesheets/1/edit
  def edit
    if @timesheet.session_datum.present?
      if @timesheet.session_datum.time_in.present?
         if @timesheet.session_datum.time_in.estimatedShiftLength.is_a?(Integer)
            logger.debug(@timesheet.session_datum.time_in.estimatedShiftLength)
            @timesheet.session_datum.time_in.estimatedShiftLength = @timesheet.session_datum.time_in.estimatedShiftLength.to_i/60
            logger.debug(@timesheet.session_datum.time_in.estimatedShiftLength)          
         end
      end
    end
  end

  # POST /timesheets
  # POST /timesheets.json
  def create
    @timesheet = Timesheet.new(timesheet_params)
    @timesheet.session_datum.continueTimesheetNumber = 4
    @timesheet.session_datum.time_in.estimatedShiftLength = @timesheet.session_datum.time_in.estimatedShiftLength * 60
    respond_to do |format|
      if @timesheet.save
        format.html { redirect_to timesheets_path, notice: 'Timesheet was successfully created.' }
        format.json { render :show, status: :created, location: @timesheet }
      else
        format.html { render :new }
        format.json { render json: @timesheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /timesheets/1
  # PATCH/PUT /timesheets/1.json
  def update
    if params[:timesheet][:session_datum_attributes][:time_in_attributes][:estimatedShiftLength].present?
       esl = params[:timesheet][:session_datum_attributes][:time_in_attributes][:estimatedShiftLength].to_i
       if esl.is_a?(Integer)
         params[:timesheet][:session_datum_attributes][:time_in_attributes][:estimatedShiftLength] = esl * 60
       end
    end
    respond_to do |format|
      if @timesheet.update(timesheet_params)
        format.html { redirect_to timesheets_path, notice: 'Timesheet was successfully updated.' }
        format.json { render :show, status: :ok, location: @timesheet }
      else
        format.html { render :edit }
        format.json { render json: @timesheet.errors, status: :unprocessable_entity }
      end
    end
  end

  def archive
    @timesheet.update(archived:true)
    respond_to do |format|
      format.html { redirect_to timesheets_path, notice: 'Timesheet was successfully archived.' }
      format.json { head :no_content }
    end
  end

  def archive_all
    @timesheets = Timesheet.where(id:params[:timesheet_ids])
    @timesheets.update_all(archived:true)
    respond_to do |format|
      format.html { redirect_to timesheets_url, notice: 'Timesheets were successfully archived.' }
      format.json { head :no_content }
    end
  end

  # DELETE /timesheets/1
  # DELETE /timesheets/1.json
  def destroy
    @timesheet.destroy
    respond_to do |format|
      format.html { redirect_to timesheets_url, notice: 'Timesheet was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def get_pca_sig
     @pca_sig = @timesheet.get_pca_sig
     if @pca_sig.present?
       data = open(@pca_sig[0])
       send_data data.read, filename:@pca_sig[1], type: @pca_sig[2]
     else
       render json: {errors:"No Signature Present"}, status: :unprocessable_entity
     end
  end

  def get_rec_sig
     @rec_sig = @timesheet.get_rec_sig
     if @rec_sig.present?
       data = open(@rec_sig[0])
       send_data data.read, filename:@rec_sig[1], type: @rec_sig[2]
     else
       render json: {errors:"No Signature Present"}, status: :unprocessable_entity
     end
  end

  def download_pdf
    @pdf = @timesheet.get_pdf
    if @pdf.present?
      data = open(@pdf[0])
      send_file data, filename:@pdf[1], type: @pdf[2], disposition: 'attachment'
    else
      render json: {errors:"No PDF Present"}, status: :unprocessable_entity
    end
  end

  def download_csv
    @csv = @timesheet.get_csv
    if @csv.present?
      data = open(@csv[0])
      send_file data, filename:@csv[1], type: @csv[2], disposition: 'attachment'
    else
      render json: {errors:"No CSV Present"}, status: :unprocessable_entity
    end
  end

  def download_pca_sig
     @pca_sig = @timesheet.get_pca_sig
     if @pca_sig.present?
       data = open(@pca_sig[0])
       send_file data, filename:@pca_sig[1], type: @pca_sig[2], disposition: 'attachment'
     else
       render json: {errors:"No Signature Present"}, status: :unprocessable_entity
     end
  end


  def download_rec_sig
     @rec_sig = @timesheet.get_rec_sig
     if @rec_sig.present?
       data = open(@rec_sig[0])
       send_file data, filename:@rec_sig[1], type: @rec_sig[2], disposition: 'attachment'
     else
       render json: {errors:"No Signature Present"}, status: :unprocessable_entity
     end
  end


  def download_zip
    if params.has_key?("timesheet_ids") && params[:timesheet_ids].present? && params.has_key?("file_type")
      is = ImageService.new
      file_type = params[:file_type].present? ? params[:file_type].to_i : 0
      logger.debug(params[:timesheet_ids])
      @zip_file = is.get_zip(params[:timesheet_ids], file_type)
      if @zip_file.present?
        filename = @zip_file.split("/").last
        data = open(@zip_file)
        send_file data, filename:filename, type:'application/zip', disposition: 'attachment'
      else 
        render json: {errors:"No Files Present"}, status: :unproccessable_entity
      end
    else
      render json: {errors:"No Time Entries Present"}, status: :unproccessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_timesheet
      @timesheet = Timesheet.find(params[:id])
    end

  def set_users
    if @agency.present?
      @recipients = @agency.recipients.nonarchived
      @pcas = @agency.pcas.nonarchived
      @recipients = @recipients.sort{|x,y| x.name <=> y.name}

      @pcas = @pcas.sort{|x,y| x.name <=> y.name}
      inAddresses = TimeIn.by_agency(@agency.id).get_all("timeInAddress")
      outAddresses = TimeOut.by_agency(@agency.id).get_all("timeOutAddress")
      @timeInAddresses = (inAddresses + outAddresses).uniq - [nil]
      @timeOutAddresses = @timeInAddresses
    else
      @recipients = Recipient.all
      @pcas = Pca.all
      inAddresses = TimeIn.get_all("timeInAddress")
      outAddresses = TimeOut.get_all("timeOutAddress")
      @timeInAddresses = (inAddresses + outAddresses).uniq - [nil]
      @timeOutAddresses = @timeInAddresses
    end
    @services = Service.nondemand
    @locations = SharedCareLocation.all
    @ratios = Ratio.all
    @days_count = [["30 days", 30],["60 days", 60],["90 days", 90],["180 days", 180],["365 days", 365]]
    @archived = ["Yes", "No", "Only Archived"]
    @verified = ["Yes", "No"]
    @finished = ["Yes", "No"]
  end

  def set_agency
    if params.has_key?("agency_id")          
      @agency = Agency.find_by(id:params[:agency_id])
    end
    if !@agency.present?
      if @current_user.is_admin?
        @agency = Agency.find_by(id:session[:current_agency_id])
      else
        @agency = @current_user.agency
      end
    end
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def timesheet_params
      params.require(:timesheet).permit(:recipient_id, :verification_id, :finished, :archived, session_datum_attributes: [:id, :pca_id, :service_id, :ratio_id, :flagForFurtherInvestigation, :currentTimesheetId, :sharedCareLocation_id, time_in_attributes: [:id, :timeIn, :estimatedShiftLength, :timeInAddress], time_out_attributes: [:id, :timeOut, :timeOutAddress, :totalHours, :billableHours, :_destroy] ])
    end
end
