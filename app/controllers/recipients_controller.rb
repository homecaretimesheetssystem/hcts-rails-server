class RecipientsController < ApplicationController
  before_action :set_recipient, only: [:show, :edit, :update, :destroy]
  before_action :set_agency, only: [:new, :create, :index]
  # GET /recipients
  # GET /recipients.json
  def index
    if @agency.present?
      @recipients = @agency.recipients
    else
      @recipients = Recipient.all
    end
  end

  # GET /recipients/1
  # GET /recipients/1.json
  def show
  end

  # GET /recipients/new
  def new
    if @agency.present?
      @recipient = Recipient.new(agency_id:@agency.id)
    else
      @recipient = Recipient.new
    end
    @recipient.build_user
    @services = Service.all
  end

  # GET /recipients/1/edit
  def edit
    @services = Service.all
  end

  # POST /recipients
  # POST /recipients.json
  def create
    @services = Service.all
    @recipient = Recipient.new(recipient_params)
    @recipient.set_user
    if !@agency.present?
      @agency = Agency.find_by(id:@recipient.agency_id)
    end
    respond_to do |format|
      if @recipient.save
        format.html { redirect_to users_path, notice: 'Recipient was successfully created.' }
        format.json { render :show, status: :created, location: @recipient }
      else
        format.html { render :new }
        format.json { render json: @recipient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recipients/1
  # PATCH/PUT /recipients/1.json
  def update
    @services = Service.all
    respond_to do |format|
      if @recipient.update(recipient_params)
        format.html { redirect_to users_path, notice: 'Recipient was successfully updated.' }
        format.json { render :show, status: :ok, location: @recipient }
      else
        format.html { render :edit }
        format.json { render json: @recipient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipients/1
  # DELETE /recipients/1.json
  def destroy
    @recipient.destroy
    respond_to do |format|
      format.html { redirect_to users_path, notice: 'Recipient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recipient
      @recipient = Recipient.find(params[:id])
    end

    def set_agency
      if params.has_key?("agency_id")          
        @agency = Agency.find_by(id:params[:agency_id])
      end
      if !@agency.present?
        if @current_user.is_admin?
          @agency = Agency.find_by(id:session[:current_agency_id])
        else
          @agency = @current_user.agency
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def recipient_params
      params.require(:recipient).permit(:id, :user_id, :agency_id, :skip_verification, :skip_gps, :first_name, :last_name, :ma_number, :company_assigned_id, :gender_preference, :county_id, :home_address, :user_attributes => [:id, :username, :email, :enabled, :password, :password_confirmation, :locked, :expired, :roles, :firstName, :lastName, :archived], service_ids: [])
    end
end
