class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :archive, :dearchive, :reset_password, :set_password]
  before_action :set_agencies, only: [:new, :create, :edit, :update]
  before_action :set_agency, only: [:index, :new, :create, :edit, :update]

  # GET /users
  # GET /users.json
  def index
    
    if @current_user.is_recipient?
      @agency = @current_user.get_agency
      @users = @agency.pcas
      @page_title = "All PCAs"
      if params.has_key?(:gender)
        @users = @users.where(gender: params[:gender])
        case params[:gender]
        when "1"
          @page_title = "Female PCAs"
        when "2"
          @page_title = "Male PCAs"
        else
          @page_title = "PCAs"
        end
      end
      
      if params.has_key?(:q)
        @page_title = "Results for #{params[:q]}"
        # Search should go through name and address fields
        @our_pcas = []
        
        @users.each do |pca|
          if (pca.firstName.present? && pca.firstName.include?(params[:q])) || (pca.lastName.present? && pca.lastName.include?(params[:q])) || (pca.homeAddress.present? && pca.homeAddress.include?(params[:q]))
            @our_pcas << pca.id
          end
        end
        
        @users = @agency.pcas.where(id: @our_pcas)
      end
      
      @users = @users.get_users
      
    else # Logic for a non-recipient user
      if params.has_key?("archived") && params[:archived] == "true"
        @archived = true
      else
        @archived = false
      end
      if @agency.present?
        @users = []
        if @agency.present? && !@archived
          if params.has_key?("admins")
            @users = User.nonarchived.where("roles like '%ADMIN%'")
            @admins =  true
          elsif params.has_key?("pcas")
            @users = @agency.pcas.get_users
            @pcas = true
          elsif params.has_key?("agents")
            @users = @agency.users.nonarchived
            @agents = true
          else
            @users = @agency.recipients.get_users
            @recips = true
          end
        elsif @current_user.is_admin?
          if params.has_key?("admins")
            @users = User.archived.where("roles like '%ADMIN%'")
            @admins =  true
          elsif params.has_key?("pcas")
            @users = @agency.pcas.get_archived_users
            @pcas = true
          elsif params.has_key?("agents")
            @users = @agency.users.archived
            @agents = true
          else
            @users = @agency.recipients.get_archived_users
            @recips = true
          end
        end
      else
        if !@archived
          if params.has_key?("admins")
            @users = User.nonarchived.where("roles like '%ADMIN%'")
            @admins =  true
          elsif params.has_key?("pcas")
            @users = Pca.all.get_users
            @pcas = true
          elsif params.has_key?("agents")
            @users = User.nonarchived.where.not(agency_id:nil)
            @agents = true
          else
            @users = Recipient.all.get_users
            @recips = true
          end
        else
          if params.has_key?("admins")
            @users = User.archived.where("roles like '%ADMIN%'")
            @admins =  true
          elsif params.has_key?("pcas")
            @users = Pca.all.get_archived_users
            @pcas = true
          elsif params.has_key?("agents")
            @users = User.archived.where.not(agency_id:nil)
            @agents = true
          else
            @users = Recipient.all.get_archived_users
            @recips = true
          end
        end
      end
    end
    
    @limit = 10
    @count = @users.count
    if params.has_key?("page")
      @page = Integer(params[:page]) rescue 1
    else
      @page = 1
    end 
    offset = (@page - 1) * @limit
    @users = @users.limit(@limit).offset(offset)
  end

  # GET /users/1
  # GET /users/1.json
  def show
   # if @current_user.agency.present?
   #   redirect_to @current_user.agency
   # end
  end

  # GET /users/new
  def new
    if params.has_key?("admin") && @current_user.is_admin?
      @user = User.new(roles:User::ADMIN_ROLE)
    else
      @user = User.new
    end
  end

  # GET /users/1/edit
  def edit
    @user.roles = User::AGENCY_ROLE
    if @current_user.is_admin?
      @roles = @user.roles
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.roles = User::AGENCY_ROLE
    
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def archive
    agency = get_agency(@user)
    @user.update(archived:true)
    if @user.is_pca?
      @user.pca.timesheets.update_all(archived:true)
    end
    if @user.is_recipient?
      @user.recipient.timesheets.update_all(archived:true)
    end
    redirect_to request.referrer
  end


  def dearchive
    agency = get_agency(@user)
    @user.update(archived:false)
    redirect_to request.referrer
  end

  def reset_password
  end

  def set_password
    errors = []
    if @user.present?
      is_pca = @user.is_pca?
      is_recip = @user.is_recipient?
      is_agent = @user.is_agent?
      change_user = false
      # @current_user.is_agent? || @current_user == @user || (params.has_key?("current_password") && @user.authorize_password(params[:current_password]))
      if @current_user.is_agent? || @current_user.is_admin?
        change_user = true
      elsif @user == @current_user
        change_user = true
      elsif params.has_key?("current_password") && @user.authorize_password(params[:current_password])
        change_user = true
      end
      logger.debug("Change User: #{change_user}")
      if change_user
        if @user.update(user_params) 
          if @current_user.is_agent? || @current_user.is_admin?
            if is_agent
              redirect_to users_path(agents:true), notice: "Password successfully changed"
            elsif is_pca
              redirect_to users_path(pcas:true), notice: "Password successfully changed"
            elsif is_recip
              redirect_to users_path(recipients:true), notice: "Password successfully changed"
            else
              redirect_to users_path, notice: "Password successfully changed"
            end
          elsif @current_user == @user
            if @user.is_pca?
              redirect_to edit_pca_path(@user.pca), notice: "Password successfully changed"
            elsif @user.is_recipient?
              redirect_to edit_recipient_path(@user.recipient), notice: "Password successfully changed"
            elsif @user.is_agent?
              redirect_to users_path(agents:true), notice: "Password successfully changed"
            end
          end
        else
          render 'reset_password'
        end
      else
        redirect_to users_path, notice: "Unauthorized" 
      end
    else
      render 'reset_password'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def set_agencies
      if @current_user.is_agent?
        @agencies = Agency.where(id: @current_user.agency_id)
      else
        @agencies = Agency.all
      end
    end

    def set_agency
      if params.has_key?("agency_id")
        @agency = Agency.find_by(id:params[:agency_id])
      end
      if @agency.nil?
        if @current_user.is_admin?
          @agency = Agency.find_by(id:session[:current_agency_id])
        else
          @agency = @current_user.agency
        end
      end
    end

    def get_agency(user)
      if user.is_agent?
        return user.agency
      elsif user.is_recipient?
        return user.recipient.agency
      elsif user.is_pca?
        return user.pca.agency
      elsif user.is_admin?
        return nil
      else
        return @current_user.agency
      end
      
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:agency_id, :admin_id, :username, :email, :enabled, :password, :password_confirmation, :locked, :expired, :roles, :firstName, :lastName, :current_password)
    end
end
