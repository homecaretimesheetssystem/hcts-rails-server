class HomeController < ApplicationController
 skip_before_action :set_current_user, only: [:login, :login_post, :privacy_policy, :forgot, :forgot_post, :reset_pass, :reset_pass_post, :signup]
 before_action :check_current_user, only: [:login] 

 def login
  
 end

 def signup
   redirect_to new_agency_url
 end

 def login_post
  @errors = []
  if params.has_key?("username") && params.has_key?("password")
    pass = params[:password]
    username = params[:username]
    @user = User.find_by(username:username)
    if @user.present? && @user.authenticate_password(pass)
      is_agent = @user.is_agent?
      is_admin = @user.is_admin?
      if is_agent || is_admin
          @current_user = @user
          @user.last_login = DateTime.now
          @user.save
          session[:current_user_id] = @user.id
        if is_agent
          redirect_to dashboard_path
        elsif is_admin
          redirect_to agencies_path
        else
          @errors << "Unauthorized"
          render 'login'
        end
      elsif @user.is_pca?
        @current_user = @user
        @user.last_login = DateTime.now
        @user.save
        session[:current_user_id] = @user.id
        redirect_to edit_pca_path(@user.pca)
      elsif @user.is_recipient? && @user.recipient.agency.present? && @user.recipient.agency.pca_finder_active
        @current_user = @user
        @user.last_login = DateTime.now
        @user.save
        session[:current_user_id] = @user.id
        redirect_to users_path
      else
        @errors << "Unauthorized"
        render 'login'
      end 
    else
      @errors << "Username or password incorrect"
      render 'login'
    end
  else
    @errors << "Username and password required"
    render 'login' 
  end
 end

 def logout
   session.destroy
   redirect_to login_path
 end


 def dashboard
   if @current_user.is_admin?
     @agency = Agency.find_by(id:session[:current_agency_id])
   end
   
   if !@agency.present?
     @agency = @current_user.agency
   end
 end

 def forgot
 end
  
 def forgot_post
   @errors = []
   if params.has_key?("email") && params[:email].present?
     get_username = params.has_key?("username") && params[:username].present?
     get_pass = params.has_key?("password") && params[:password].present?
     @user = User.find_by(email:params[:email])
     if get_username || get_pass
       if @user.present?
         if get_username
           AdminMailer.send_username(@user).deliver_now
         end
         if get_pass
           @user.gen_pass_token
           AdminMailer.send_password(@user).deliver_now
         end
       else
         @errors << "No User found"
       end
     else
       @errors << "Select Either Username or Password"
     end
   else
     @errors << "Email Required"
   end 
   if @errors.empty?
     redirect_to login_path, notice: "Email Sent"
   else
     render 'forgot'
   end
 end

 def reset_pass
   message = nil
   if params.has_key?("token") && params[:token].present?
     @token = params[:token]
     @user = User.find_by(confirmation_token:params[:token])
     if !@user.present? 
       message = "No User Found"
     elsif @user.password_requested_at < Time.now - 24.hours
       message = "Request Expired"
     end
   else
     redirect_to login_path
   end
   if message.present?
     redirect_to forgot_path, alert: message
   end
 end

 def reset_pass_post
   message = nil
   @errors = []
   if params.has_key?("token") && params[:token].present?
     @user = User.find_by(confirmation_token:params[:token])
     if !@user.present?
       message = "Request Not Found"
     elsif @user.password_requested_at < Time.now - 24.hours
       message = "Request Expired"
     else
       if params.has_key?("password") && params[:password].present?
         if params.has_key?("password_confirmation") && params[:password_confirmation] == params[:password]
           @user.password = params[:password]
           @user.password_confirmation = params[:password]
           @user.save
         else
           @errors << "Password Confirmation does not match"
         end
       end
     end
   else
     message = "Request Not Found"
   end
   if message.present?
     redirect_to forgot_path, alert: message
   elsif @errors.empty?
     redirect_to login_path, notice: "Password Successfully Reset"
   else
     render 'reset_pass'
   end
 end

 def testpage

 end

 def privacy_policy
 end
 
 def settings_redirect
   if @current_user.is_agent?
     redirect_to edit_agency_path(@current_user.agency)
   elsif @current_user.is_pca?
     redirect_to @current_user
   elsif @current_user.is_recipient?
     redirect_to @current_user
   elsif @current_user.is_admin?
     if session[:current_agency_id].present?
       redirect_to edit_agency_path(id:session[:current_agency_id])
     else
       redirect_to agencies_path
     end
   else
     redirect_to dashboard_path
   end
 end
 
 def recipient_dashboard
 end


private 

  def check_current_user
    if session[:current_user_id].present?
      user = User.find_by(id:session[:current_user_id])
      if user.present?
        @current_user = user
      end
    end
    if @current_user.present?
      if @current_user.is_admin?
        redirect_to agencies_path
      else
        redirect_to dashboard_path
      end
    end
  end

end
