class GoalsController < ApplicationController

before_action :set_agency
before_action :set_careoptions, except: [:index]
before_action :set_users, except: [:index]
before_action :verify_admin
  

  def index
    @care_goals = true
    if params.has_key?("graph") && params[:graph] == "true"
      @graph = true
      if @recipient.present?
        rec_data = [@recipient.graph_care_goals(@recipient.timesheets.active.graphable.pluck(:id))]
      elsif params.has_key?("recipient_ids") && params[:recipient_ids].present?
        rec_data = []
        params[:recipient_ids].each do |rec_id|
          rec = Recipient.find_by(id:rec_id)
          if rec.present?
            rec_data << rec.graph_care_goals(rec.timesheets.graphable.active.pluck(:id))
          end
        end
      elsif params.has_key?("timesheet_ids") && params[:timesheet_ids].present?
        rec_data = Recipient.graph_care_goals(params[:timesheet_ids])
      else
        rec_data = @agency.graph_care_goals
      end
      @data = {datasets:rec_data}
      @options = Recipient.graphOptions
    else
      if params.has_key?("timesheet_ids") && params[:timesheet_ids].present?
        @timesheets = Timesheet.compare_to_goals(@agency.timesheets.where(id:params[:timesheet_ids]))
      elsif @recipient.present?
        @timesheets = @recipient.compare_care_goals(@recipient.timesheets.active.finished.order_by_time_in)
      else
        @recipients = @agency.recipients.has_goals
      end
    end
  end

  def compare

  end


  def new
    if params.has_key?("recipient_id") && params[:recipient_id].present?
      @recipient = Recipient.find_by(id:params[:recipient_id])
      if @recipient.present? && !@recipient.care_goals.any?
        @care_goal = CareGoal.new(recipient_id:@recipient.id)
      else  
        redirect_to edit_goal_path(id:@recipient.care_goals.first.id)
      end
    end
    if !@care_goal.present?
      @care_goal = CareGoal.new
    end
    if @agency.present?
      @recipients = @agency.recipients.needs_goals.sort{|x,y| x.name <=> y.name}
    end
  end

  def create
    @care_goal = CareGoal.new(care_goal_params)
    care_option_ids = care_goal_params[:goal_care_option_ids].reject { |c| c.empty? }
    if @current_user.is_agent?
      @care_goal.setByAgency_id = @current_user.agency_id
    elsif @current_user.is_recipient?
      @care_goal.set_by_recipient = @current_user.recipient.id
    elsif @current_user.is_admin? && !@care_goal.setByAgency_id.present?
      @care_goal.setByAdmin_id = @current_user.id
    end
    if @care_goal.save
      redirect_to goals_index_path(recipient_id:@care_goal.recipient_id), notice: "Care Goals successfully created"
    else
      render 'new'
    end
  end

  def edit
    @care_goal = CareGoal.find_by(id:params[:id])
    if !@care_goal.present?
      redirect_to goals_index_url, notice: "Care Goals not found"
    end
  end

  def update
    @care_goal = CareGoal.find_by(id:params[:id])
    if @care_goal.update(care_goal_params)
      redirect_to goals_index_url(recipient_id:@care_goal.recipient_id), notice: "Care Goals successfully updated"
    else
      render 'edit'
    end
  end


private

  def set_agency
    if params.has_key?("agency_id")          
      @agency = Agency.find_by(id:params[:agency_id])
    end
    if !@agency.present?
      if @current_user.is_admin?
        @agency = Agency.find_by(id:session[:current_agency_id])
      else
        @agency = @current_user.agency
      end
    end
    if params.has_key?("recipient_id") && params[:recipient_id].present?
      @recipient = Recipient.find_by(id:params[:recipient_id])
    end
  end
  
  def set_careoptions
    @care_options = Careoption.all
  end  

  def set_users
    if @agency.present?
      @recipients = @agency.recipients.sort{|x,y| x.name <=> y.name}
      @pcas = @agency.pcas.sort{|x,y| x.name <=> y.name}
    else
      @recipients = Recipient.all
      @pcas = Pca.all
    end
  end

  def care_goal_params
    params.require(:care_goal).permit(:id, :recipient_id, :set_by_recipient, :setByAgency_id, :setByAdmin_id, :goal_care_option_ids => [])
  end



end