class HctsApiController < ApplicationController
skip_before_action :verify_authenticity_token
skip_before_action :set_current_user
 before_action :set_api_user, except: [:login]
before_action :set_default_response_format


# {"access_token":"ODEzNzcyNGFjYTgxY2M5ZGFhOWFhNGFkNzQ1YmZhNGEzMTRiMmNkNzNlMzZlZmJjODYxMmRiYjMwMzE2ZTY4MQ","expires_in":604800,"token_type":"bearer","scope":null,"refresh_token":"ZDUzZTcxNTM1Mjk4NmYyNGYwNWY4MjlmOWU1MzUwZWExOTFjNzE1ZWFlNTU3NDczNjVlMTJiYmI2ZWJmYzg5Zg"}

 def login
   @errors = []
   if params.has_key?("username") && params[:username].present? && params.has_key?("password") && params[:password].present?
     @user = User.find_by(username:params[:username])
     if @user.present?
       if @user.enabled
         if @user.authenticate_password(params[:password])
           @session = @user.set_login
           if @session.length < 2 || !@session[0].present? || !@session[1].present?
             @errors << ["invalid_grant", "User not enabled"]          
           end
         else
           @errors << ["invalid_grant", "Invalid username and password combination"]
         end
       else
         @errors << ["invalid_grant", "User not enabled"]
       end
     else
       @errors << ["invalid_grant", "Invalid username and password combination"]
     end
   elsif params.has_key?("refresh_token") && params[:refresh_token].present?
     @refresh = RefreshToken.find_by(token:params[:refresh_token])
     if @refresh.present?
       if Time.now.to_i <= @refresh.expires_at
         if @refresh.user.enabled
           @session = @refresh.user.set_login
           if @session.length < 2 || !@session[0].present? || !@session[1].present?
             @errors << ["invalid_grant", "User not enabled"]
           end
         else
           @errors << ["invalid_grant", "User not enabled"]
         end
       else
         @errors << ["invalid_grant", "Refresh token expired"]
       end
     else
       @errors << ["invalid_grant", "Invalid refresh token"]
     end
   else
     @errors << ["invalid_request","Missing parameters. \"username\" and \"password\" required"]
   end
   if @errors.empty? && @session.present? 
     render json: {access_token:@session[0].token, expires_in:@session[0].expires_in, token_type:"bearer", scope:nil, refresh_token:@session[1].token}, status: :ok
   else
     logger.debug(@errors)
     render json: {error:@errors[0][0], error_description:@errors[0][1]}, status: :unprocessable_entity
   end
 end

 def get_data
 #  {sessionData:[{id, continueTimesheetNumber, currentTimesheetNumber, flagForFurtherInvestigation, pca: { id, first_name, last_name, umpi, company_assigned_id, profilePhoto, home_address, qualifications:[{id, qualification}], available, services:[{id, service_number, service_name}], years_of_experience, gender, counties:[{id, name}] }s =u, service:{id, service_number, service_name}, timeIn:{id, timeIn, estimatedShiftLength, timeInAddress, latitude, longitude, timeInPictureTime}, timesheets:[{id, recipient:{id, skip_verification, skip_gps, first_name, last_name, ma_number}, careOptionTimesheets:[  ], finished}], ratio:{id, ratio}, sharedCareLocation:{id, location} }], user:{id, userType, email}, pca: {id, first_name, last_name, umpi, company_assigned_id, profilePhoto, home_address, qualifications:[{id, qualification}], available, services:[ {id, service_number, service_name} ], years_of_experience, gender, counties:[{id, name}] }, agency:{id, agency_name, week_start_date, start_time:{hour, minute, period}, phone_number, state:{id, state_name}, image_name, services:[ ], recipients:[{id, skip_verification, skip_gps, first_name, last_name, ma_number, counties:[{id, name}]}], careOptions[{id, is_iadl, care_option}], ratios:[{id, ratio}], sharedCareLocations:[{id, location}], amazonUrl, services[{id, service_number, service_name}]} 

#{"sessionData":[{"id":4112,"continueTimesheetNumber":6,"currentTimesheetNumber":1,"flagForFurtherInvestigation":false,"pca":{"id":52,"first_name":"ramp","last_name":"sorlen","umpi":"0","company_assigned_id":"4","profilePhoto":"52id9zylg1495916154.jpg","home_address":"337 Roslyn Place Minneapolis MN 55419","qualifications":[{"id":2,"qualification":"CPR Certified"},{"id":12,"qualification":"Experience with Behaviors"},{"id":32,"qualification":"Experience making transfers"},{"id":52,"qualification":"Has driver license"}],"available":true,"services":[{"id":2,"service_number":1,"service_name":"personal care service"},{"id":12,"service_number":2,"service_name":"homemaking"},{"id":102,"service_number":11,"service_name":"temporary pca (on demand)"}],"years_of_experience":10,"gender":2,"counties":[{"id":12,"name":"Anoka County"},{"id":262,"name":"Hennepin County"}]},"service":{"id":2,"service_number":1,"service_name":"personal care service"},"timeIn":{"id":422,"timeIn":"2017-12-22T15:36:13-0600","estimatedShiftLength":900,"timeInAddress":"337 ROSLYN PL, MINNEAPOLIS, MN 55419, USA","latitude":"44.8952114","longitude":"-93.2716256","timeInPictureTime":"2017-12-22T15:36:13-0600"},"timeOut":{"id":262,"timeOut":"2017-12-22T15:51:13-0600","timeOutAddress":"337 ROSLYN PL, MINNEAPOLIS, MN 55419, USA","latitude":"44.8952114","longitude":"-93.2716256","timeOutPictureTime":"2017-12-22T15:51:13-0600","totalHours":900,"billableHours":0},"timesheets":[{"id":4122,"recipient":{"id":42,"skip_verification":false,"skip_gps":false,"first_name":"carrie","last_name":"hennessy","ma_number":"5","company_assigned_id":"lkjin","gender_preference":0,"county":{"id":262,"name":"Hennepin County"}},"file":{"id":312,"timesheetPcaSig":"312idh3avc1513980861.png"},"careOptionTimesheets":[{"careOption":{"id":12,"is_iadl":false,"care_option":"Grooming"}},{"careOption":{"id":22,"is_iadl":false,"care_option":"Bathing"}},{"careOption":{"id":42,"is_iadl":false,"care_option":"Transfers"}}],"finished":0,"pca_signature":{"id":92,"pcaSigTime":"2017-12-22T16:14:18-0600","pcaSigAddress":"337 ROSLYN PL, MINNEAPOLIS, MN 55419, USA","latitude":"44.8952287","longitude":"-93.2716448"}}],"dateEnding":"2017-12-22T15:51:13-0600","ratio":{"id":2,"ratio":"1:1"},"sharedCareLocation":{"id":2,"location":"N\/A"}}],"user":{"id":102,"userType":"recipient","email":"mallory.kassulke@example.net","skipVerification":false},"recipient":{"id":42,"skip_verification":false,"skip_gps":false,"first_name":"carrie","last_name":"hennessy","ma_number":"5","company_assigned_id":"lkjin","gender_preference":0,"county":{"id":262,"name":"Hennepin County"}},"pcas":[{"pca":{"id":32,"first_name":"scott","last_name":"mahonis","umpi":"9","company_assigned_id":"0","profilePhoto":"32id8kgm21491773216.jpg","qualifications":[{"id":2,"qualification":"CPR Certified"},{"id":12,"qualification":"Experience with Behaviors"},{"id":32,"qualification":"Experience making transfers"},{"id":52,"qualification":"Has driver license"}],"available":true,"services":[],"years_of_experience":5,"gender":1,"counties":[{"id":2,"name":"Aitkin County"},{"id":12,"name":"Anoka County"},{"id":262,"name":"Hennepin County"}]},"verification":[],"is_temp":false},{"pca":{"id":42,"first_name":"peter","last_name":"solberg","umpi":"7","company_assigned_id":"2","qualifications":[],"services":[],"years_of_experience":4,"gender":2,"counties":[]},"verification":[],"is_temp":false},{"pca":{"id":52,"first_name":"ramp","last_name":"sorlen","umpi":"0","company_assigned_id":"4","profilePhoto":"52id9zylg1495916154.jpg","home_address":"337 Roslyn Place Minneapolis MN 55419","qualifications":[{"id":2,"qualification":"CPR Certified"},{"id":12,"qualification":"Experience with Behaviors"},{"id":32,"qualification":"Experience making transfers"},{"id":52,"qualification":"Has driver license"}],"available":true,"services":[{"id":2,"service_number":1,"service_name":"personal care service"},{"id":12,"service_number":2,"service_name":"homemaking"},{"id":102,"service_number":11,"service_name":"temporary pca (on demand)"}],"years_of_experience":10,"gender":2,"counties":[{"id":12,"name":"Anoka County"},{"id":262,"name":"Hennepin County"}]},"verification":[],"is_temp":false}],"agency":{"id":2,"agency_name":"rob\u2019s test agency","week_start_date":"Monday","start_time":{"hour":12,"minute":"00","period":"AM"},"phone_number":"9526491914","state":{"id":2,"state_name":"Alabama"},"image_name":"58af65033a232.jpg","services":[]},"amazonUrl":"https:\/\/s3.amazonaws.com\/hc-timesheets-demo\/","careOptions":[{"id":2,"is_iadl":false,"care_option":"Dressing"},{"id":12,"is_iadl":false,"care_option":"Grooming"},{"id":22,"is_iadl":false,"care_option":"Bathing"},{"id":32,"is_iadl":false,"care_option":"Eating"},{"id":42,"is_iadl":false,"care_option":"Transfers"},{"id":52,"is_iadl":false,"care_option":"Mobility"},{"id":62,"is_iadl":false,"care_option":"Positioning"},{"id":72,"is_iadl":false,"care_option":"Toileting"},{"id":82,"is_iadl":false,"care_option":"Health Related"},{"id":92,"is_iadl":false,"care_option":"Behavior"},{"id":102,"is_iadl":true,"care_option":"Light Housekeeping"},{"id":112,"is_iadl":true,"care_option":"Laundry"},{"id":122,"is_iadl":true,"care_option":"Other"}],"services":[{"id":2,"service_number":1,"service_name":"personal care service"},{"id":12,"service_number":2,"service_name":"homemaking"},{"id":22,"service_number":3,"service_name":"respite"},{"id":32,"service_number":4,"service_name":"caregiving expense"},{"id":42,"service_number":5,"service_name":"environmental modifications"},{"id":52,"service_number":6,"service_name":"personal support"},{"id":62,"service_number":7,"service_name":"self-direction support"},{"id":72,"service_number":8,"service_name":"consumer support"},{"id":82,"service_number":9,"service_name":"personal assistance"},{"id":92,"service_number":10,"service_name":"treatment and training"},{"id":102,"service_number":11,"service_name":"temporary pca (on demand)"}]}
   headers['Last-Modified'] = Time.now.httpdate
   if @current_user.present?
     if @current_user.is_pca?
       @sessionData = @current_user.pca.session_data.unfinished.for_api
       @agency = @current_user.pca.agency
     else
       @sessionData = @current_user.recipient.session_data.unfinished.for_api.ready_to_sign
       @agency = @current_user.recipient.agency
     end
     @start_time = @agency.get_start_time_hash
     @care_options = Careoption.all
     @ratios = Ratio.all
     @care_locations = SharedCareLocation.all
     @services = Service.nondemand
     render 'hcts_api/get_data.json.jbuilder'
   else
     render json: {error:@current_user.username}, status: :unprocessable_entity
   end
 end

 def create_session_data
   @session_data = @current_user.pca.session_data.new
   if @session_data.save(validate:false) && params.has_key?("homecare_homecarebundle_sessiondata")
     render json: {"createdSessionData":{"id":@session_data.id} }, status: :created
   else
     render json: {error:'hello pretty', error_description:""}, status: :unprocessable_entity
   end
 end

 def update_session_data
   @session_data = SessionDatum.find_by(id:params[:id])
   if @session_data.present?
     if params.has_key?("homecare_homecarebundle_sessiondata")
       session_params = params["homecare_homecarebundle_sessiondata"]
       session_params.each do |k,v|
         if k == "pca" 
           @session_data.pca_id = v
         elsif k == "service"
           @session_data.service_id = v
         elsif k == "ratio"
           @session_data.ratio_id = v
         elsif k == "sharedCareLocation"
           @session_data.sharedCareLocation_id = v
         elsif k == "timeIn"
           @time_in = TimeIn.find_by(id:v)
           @session_data.timeIn_id = v
         elsif k == "timeOut"
           @session_data.timeOut_id = v
         else
           @session_data[k] = v
         end
       end
       if @session_data.save
         if !@session_data.dateEnding.present?
           @session_data.update(dateEnding: @session_data.get_date_ending)
         end
         head :no_content
       else
         render json: {"error":@session_data.errors.full_messages[0], "error_description":""}, status: :unprocessable_entity
       end
     else
       render json: {"error":"homecare_homecarebundle_sessiondata not found", "error_description":""}, status: :unprocessable_entity
     end
   else
     render json: {"error":"session data not found", "error_description":""}, status: :unprocessable_entity
   end
 end

 def create_timesheet
   @timesheet = Timesheet.new(finished:false)
   if @timesheet.save(validate:false) && params.has_key?("homecare_homecarebundle_timesheet")
     render json: {"createdTimesheet":{"id":@timesheet.id} }
   else
     render json: {"error":"hello pretty", error_description:""}, status: :unprocessable_entity
   end
 end

 def update_timesheet
   @timesheet = Timesheet.find_by(id:params[:id])
   if @timesheet.present? && params.has_key?("homecare_homecarebundle_timesheet")
     timesheet_params = params["homecare_homecarebundle_timesheet"]
     timesheet_params.each do |k,v|
        if k == "recipient"
          @timesheet.recipient_id = v
        elsif k == "sessionData"
          @timesheet.sessionData_id = v
        elsif k == "verification"
          @timesheet.verification_id = v
        else 
          @timesheet[k] = v
        end
     end
     if @timesheet.save(validate:false)
       head :no_content
     else
       render json: {"error":@timesheet.errors.full_messages[0], error_description:""}, status: :unprocessable_entity
     end
   else
     render json: {"error":"homecare_homecarebundle_timesheet required", error_description:""}, status: :unprocessable_entity
   end
 end

 def create_time_in
   @time_in = TimeIn.new
   if @time_in.save(validate:false) && params.has_key?("homecare_homecarebundle_timein")
     render json: {"createdTimeIn": {"id":@time_in.id} }, status: :created
   else
     render json: {"error":"hello pretty", error_description:""}, status: :unprocessable_entity
   end
 end

 def update_time_in
   @time_in = TimeIn.find_by(id:params[:id])
   if @time_in.present? && params.has_key?("homecare_homecarebundle_timein")
     time_in_params = params["homecare_homecarebundle_timein"]
     time_in_params.each do |k,v|
       @time_in[k] = v
     end
     if @time_in.save
       if @time_in.session_datum.present?
         @time_in.session_datum.update(dateEnding:@time_in.timeIn)
       end
       render json: {"seconds":time_in_params["estimatedShiftLength"] }, status: :ok
     else
       render json: {"error":"hello pretty", error_description:""}, status: :unprocessable_entity
     end
   else
     render json: {"error":"hello pretty", error_description:""}, status: :unprocessable_entity
   end
 end

 def create_time_out
   @time_out = TimeOut.new
   if @time_out.save(validate:false) && params.has_key?("homecare_homecarebundle_timeout")
     render json: {"createdTimeOut": {"id":@time_out.id} }, status: :created
   else
     render json: {"error":"hello pretty", error_description:""}, status: :unprocessable_entity
   end
 end

 def update_time_out
   @time_out = TimeOut.find_by(id:params[:id])
   if @time_out.present? && params.has_key?("homecare_homecarebundle_timeout")
     time_out_params = params["homecare_homecarebundle_timeout"]
     time_out_params.each do |k,v|
       @time_out[k] = v
     end
     if @time_out.save
       render json: {}, status: :no_content
     else
       render json: {"error":"hello pretty", error_description:""}, status: :unprocessable_entity
     end
   else
     render json: {"error":"hello pretty", error_description:""}, status: :unprocessable_entity
   end
 end

 def empty_careoptions
   @errors = []
   @timesheet = Timesheet.find_by(id:params[:id])
   if @timesheet.present?
     @timesheet.careoption_ids = []
     if !@timesheet.save
       @errors << "Save Failed"
     end
   else
     @errors << "Timesheet not found"
   end
   if @errors.empty?
     render json: {"message":"all care options for timesheet Id #{@timesheet.id} have been deleted"}, status: :ok
   else
     render json: {"error":@errors[0], "error_description":""}, status: :unprocessable_entity
   end  
 end

 def set_careoptions
   if params.has_key?("homecare_homecarebundle_careoptiontimesheet")
     careoption_params = params["homecare_homecarebundle_careoptiontimesheet"]
     if careoption_params.has_key?("timesheet") && careoption_params.has_key?("careOptions")
       options = careoption_params["careOptions"]
       @timesheet = Timesheet.find_by(id:careoption_params["timesheet"])
       if @timesheet.present? && options.is_a?(Array)
         @timesheet.careoption_ids = options
         render json: {"careOptionTimesheetsCreated":{"ids":options} }, status: :ok
       else
         render json: {"error":"hello pretty1", error_description:""}, status: :unprocessable_entity
       end      
     else
       render json: {"error":"hello pretty2", error_description:""}, status: :unprocessable_entity
     end
   else
     render json: {"error":"hello pretty3", error_description:""}, status: :unprocessable_entity
   end
 end

 def create_timesheet_files
   @timesheet_files = TimesheetFile.new
   if @timesheet_files.save(validate:false)
     render json: {"createdFilesId":@timesheet_files.id}, status: :ok
   else
    render json: {"error":"hello pretty", error_description:""}, status: :unprocessable_entity
   end
 end



 def create_pca_sign
   if params.has_key?("homecare_homecarebundle_pcasignature")
     @pca_sig = PcaSignature.new
     sig_params = params["homecare_homecarebundle_pcasignature"]
     sig_params.each do |k,v|
       if k == "timesheet"
         @pca_sig.timesheet_id = v
       else
         @pca_sig[k] = v
       end
     end
     if @pca_sig.save
       render json: {"createdPcaSignature":{"id":@pca_sig.id} }, status: :created
     else
       render json: {"error":@pca_sig.errors.full_messages[0], error_description:""}, status: :unprocessable_entity
     end
   else
     render json: {"error":"homecare_homecarebundle_pca_signature required", error_description:""}, status: :unprocessable_entity
   end
 end 

 def create_recip_sign
   if params.has_key?("homecare_homecarebundle_recipientsignature")
     @recip_sig = RecipientSignature.new
     sig_params = params["homecare_homecarebundle_recipientsignature"]
     sig_params.each do |k,v|
       if k == "timesheet"
         @recip_sig.timesheet_id = v
       else
         @recip_sig[k] = v
       end
     end
     if @recip_sig.save
       render json: {"createdRecipientSignature":{"id":@recip_sig.id} }, status: :created
     else
       render json: {"error":@recip_sig.errors.full_messages[0], error_description:""}, status: :unprocessable_entity
     end
   else
     render json: {"error":"homecare_homecarebundle_recipientsignature required", error_description:""}, status: :unprocessable_entity
   end      
 end
 
 def update_timesheet_files
   resp = false
   @errors = []
   @timesheet_files = TimesheetFile.find_by(id:params[:id])
   if @timesheet_files.present?
     if params.has_key?("timesheet") && @timesheet_files.timesheet_id.nil?
       @timesheet_files.update(timesheet_id: params["timesheet"])
     end
     if params.has_key?("timesheetPcaSig")
       filename = "timesheetPcaSig"
       resp = TimesheetFile.add_file(params["timesheetPcaSig"], @timesheet_files, filename)
     end
     if params.has_key?("timesheetRecSig")
       filename = "timesheetRecSig"
       resp = TimesheetFile.add_file(params["timesheetRecSig"], @timesheet_files, filename)
     end
     if params.has_key?("timesheetPdfFile")
       filename = "timesheetPdfFile"
       resp = TimesheetFile.add_file(params["timesheetPdfFile"], @timesheet_files, filename)
     end
     if params.has_key?("timesheetCsvFile")
       filename = "timesheetCsvFile"
       resp = TimesheetFile.add_file(params["timesheetCsvFile"], @timesheet_files, filename)
     end
     @ts_files = TimesheetFile.find(@timesheet_files.id)
     if !resp
       @errors << "upload failed"
     end
   else
     @errors << "invalidFileId"
   end
   if @errors.empty?
     render json: {"uploadedFileLocation":[resp] }, status: :ok
   else
     render json: {"error":@errors[0], error_description:""}, status: :unprocessable_entity
   end
 end

 def upload_pca_sign
   @errors = []
   @timesheet_files = TimesheetFile.find_by(id:params[:id])
   if @timesheet_files.present?
     @timesheet_files.update(timesheet_id: params["timesheet"])
     if resp
       @timesheet_files = TimesheetFile.find_by(id:params[:id])
       @loc = @timesheet_files.timesheetPcaSig
       render json: {"uploadedFileLocation":[@loc] }, status: :ok
     else
       render json: {"error":"hello pretty", error_description:""}, status: :unprocessable_entity
     end
   else
     render json: {"error":"hello pretty", error_description:""}, status: :unprocessable_entity
   end     
 end

 def upload_recip_sign

 end

#{"verification_needed":"false"}
 def need_verif
   @need_verif = false
   @assignment = RecipientPca.find_by(recipient_id:params[:recipient_id],pca_id:params[:pca_id])
   if @assignment.present?
     if !@assignment.recipient.skip_verification
       @verifications = @assignment.verifications
       if @verifications.present?
         @verification = @verifications.where(current:true).where(verified:false).first
         if @verification.first_time
           @need_verif = true
         elsif @verification.verification_date < Time.now
           @need_verif = true
         end
       else
         @verification = Verification.new(recipientPca_id:@assignment.id, frequency:90, first_time:true, current:true, verified:false, verification_date:Time.now)
         if @verification.save
           @need_verif = true
         end
       end
     end
   end
   if @need_verif
     render json: {"verification_needed":@need_verif.to_s, id:@verification.id}, status: :ok
   else
     render json: {"verification_needed":@need_verif.to_s}, status: :ok
   end
 end

#{"homecare_homecarebundle_verification":{"address":"337 ROSLYN PL, MINNEAPOLIS, MN 55419, USA ","latitude":44.8952583,"longitude":-93.2716411,"time":"2018-03-13"}}
  def verify
    @verification = Verification.find_by(id:params[:verif_id])
    if @verification.present?
      if params.has_key?("homecare_homecarebundle_verification")
        verif_params = params[:homecare_homecarebundle_verification]
        verif_params.each do |k,v|
          if k == "time"
            @verification.time = Time.now
          else
            @verification[k] = v
          end
        end
        if @verification.save
          if !@verification.recipient.skip_verification && @verification.verification_photo.present?
            @days = @verification.frequency
            if @days > 10
              @days = @days - 10
            end
            @days = rand(@days) + 10
            @date = Time.now + @days.days
            @v = Verification.new(recipientPca_id:@verification.recipientPca_id, frequency:90, first_time:false, current:true, verified:false, verification_date:@date)
            if @v.save
              @verification.update(current: false, verified:true) 
            end
          end
        else
          @error = @verification.errors.full_messages.first
        end
      else
        @error = "homecare_homecarebundle_verification required"
      end
    else
      @error = "No verification found"
    end
    if !@error.present?
      head :no_content
    else
      render json: {"error":@error, "error_description":""}, status: :unprocessable_entity
    end
  end

 def verification_photo
   @verification = Verification.find_by(id:params[:verif_id])
   if @verification.present?
     if params.has_key?("VerificationPhoto") || params.has_key?("verificationPhoto")
       if params[:verification_photo].present?
         photo = params[:verification_photo]
       elsif params[:verificationPhoto].present?
         photo = params[:verificationPhoto]
       else
         photo = params[:VerificationPhoto]
       end
       if @verification.add_image(photo)
         if !@verification.recipient.skip_verification && @verification.time.present?
           @days = @verification.frequency
           if @days > 10
             @days = @days - 10
           end
           @days = rand(@days) + 10
           @date = Time.now + @days.days
           @v = Verification.new(recipientPca_id:@verification.recipientPca_id, frequency:90, first_time:false, current:true, verified:false, verification_date:@date)
           if @v.save
             @verification.update(current: false, verified:true) 
           end
         end
       else
         @error = "Verification Image Upload Failed"
       end
     else
       if !params[:verification_photo].present? 
         @error = "Verification photo not found"
       else
         @error = "Parameter not found"
       end
     end
   else
     @error = "No Verification Found"
   end
   if !@error.present?
      head :no_content
   else
      render json: {"error":@error, "error_description":""}, status: :unprocessable_entity
   end
 end

 def finish_timesheets
   @errors = []
   if params.has_key?("homecare_homecarebundle_timesheet")
     session_params = params["homecare_homecarebundle_timesheet"]
     if session_params.has_key?("sessionId")
       @sessionData = SessionDatum.find_by(id:session_params["sessionId"])
       if @sessionData.present?
         @sessionData.timesheets.each do |ts|
           ts.update(finished:true)
           if @sessionData.pca.agency.get_emails.present?
             TimesheetMailer.send_pdf_and_csv(ts).deliver_now
           end
         end
       else
         @errors << "invalidSessionId"
       end
     else
       @errors << "sessionId required"
     end
   else
     @errors << "homecare bundle required"
   end
   if @errors.empty?
     render json: {"message":"timesheets have been marked as finished"}, status: :created
   else
     render json: {"error":@errors[0], error_description:""}, status: :unprocessable_entity
   end   
 end

 def delete_timesheets
   if params.has_key?("sessionId")
     @sessionData = SessionDatum.find_by(id:params[:sessionId])
     if @sessionData.present?
       @sessionData.timesheets.each do |ts|
         ts.destroy
       end
     else
       render json: {"error":"hello pretty", error_description:"1"}, status: :unprocessable_entity
     end
   elsif params.has_key?("homecare_homecarebundle_timesheet")
     @ts_params = params[:homecare_homecarebundle_timesheet]
     if @ts_params.has_key?("sessionId")
       @sessionData = SessionDatum.find_by(id:@ts_params[:sessionId])
       if @sessionData.present?
         @sessionData.timesheets.each do |ts|
           ts.destroy
         end
      else
       render json: {"error":"hello pretty", error_description:"2"}, status: :unprocessable_entity
      end
     else
       render json: {"error":"hello pretty", error_description:"3"}, status: :unprocessable_entity
     end
   else
     render json: {"error":"hello pretty", error_description:"4"}, status: :unprocessable_entity
   end     
 end

 def verify_recipient_pass
   @errors = []
   @error_messages = []
   if params.has_key?("password") && params.has_key?("recipientId") && params.has_key?("pcaId")
      @recipient = Recipient.find_by(id: params[:recipientId])
      if @recipient.present? 
        if !@recipient.user.authenticate_password(params[:password])
          @errors << "invalidPassword"
          @error_messages << "password is incorrect"
        end
      else
        @errors << "invalidRecipient"
        @error_messages << "recipient not found"
      end
   else
     if !params.has_key?("password")
       @errors << "missingPassword"
       @error_messages << "password required"
     elsif !params.has_key?("recipientId")
       @errors << "missingRecipientId"
       @error_messages << "recipientId required"
     elsif !params.has_key?("pcaId")
       @errors << "missingPcaId"
       @error_messages << "pcaId required"
     else
       @errors << "unknownError"
       @error_messages << "unknown error occurred"
     end
   end
   if @errors.empty?
     render json: {"status":"success", "message":"recipient password has been successfully verified"}, status: :ok
   else
     render json: {"error":@errors[0], "errorMessage":@error_messages[0], postedPassword:"" }, status: :ok
   end
 end


# {"id":52,"first_name":"ramp","last_name":"sorlen","umpi":"0","company_assigned_id":"4","profilePhoto":"52id9zylg1495916154.jpg","home_address":"337 Roslyn Place Minneapolis MN 55419","qualifications":[{"id":2,"qualification":"CPR Certified"},{"id":12,"qualification":"Experience with Behaviors"},{"id":32,"qualification":"Experience making transfers"},{"id":52,"qualification":"Has driver license"}],"available":true,"services":[{"id":2,"service_number":1,"service_name":"personal care service"},{"id":12,"service_number":2,"service_name":"homemaking"},{"id":102,"service_number":11,"service_name":"temporary pca (on demand)"}],"years_of_experience":10,"gender":2,"counties":[{"id":12,"name":"Anoka County"},{"id":262,"name":"Hennepin County"}]}

  def get_pca
    @errors = []
    if params.has_key?("id")
      @pca = Pca.find_by(id:params[:id])
      if !@pca.present?
        @errors << "No Worker Found"
      end
    end
    if !@errors.empty?
      render json: {"error":@errors[0], errorMessage:""}, status: :unprocessable_entity
    end
  end

#{"homecare_profile":{"qualifications":["2","12","32","52"],"firstName":"Ramp ","lastName":"Sorlen ","counties":["12","262"],"gender":2,"yearsOfExperience":"10"}}

  def update_pca
    @errors = []
    if params.has_key?("pca_id") && params[:pca_id].present?
      @pca = Pca.find_by(id:params[:pca_id])
      if @pca.present?
        if params.has_key?("homecare_profile") && params[:homecare_profile].present?
          profile_params = params[:homecare_profile]
          profile_params.each do |k,v|
            if k == "counties"
              @pca.county_ids = v
            elsif k == "qualifications"
              @pca.qualification_ids = v
            elsif k == "yearsOfExperience"
              if !v.present? 
                v = 0
              end
              @pca.years_of_experience = v
            elsif k == "homeAddress"
              @pca.home_address = v
            elsif k == "aboutMe" || k == "about_me"
              @pca.about_me = v
            else
              @pca[k] = v
            end
          end
          if !@pca.save
            @errors = @pca.errors.full_messages
          end
        else
          @errors << "homecare_profile required"
        end
      else
        @errors << "pca not found"
      end
    else
      @errors << "pca id required"
    end
    if @errors.empty?
      render json: {}, status: :no_content
    else
      render json: {"error":@errors[0], errorMessage:""}, status: :unprocessable_entity
    end
  end

  def set_profile_image
    @errors = []
    if params.has_key?("pca_id") && params[:pca_id].present?
      @pca = Pca.find_by(id:params[:pca_id])
      if @pca.present?
        if params.has_key?("profilePhoto") && params[:profilePhoto].present?
          @pca.photo = params[:profilePhoto]
          if @pca.save
            resp = @pca.profile_photo
          else
            @errors = @pca.errors.full_messages
          end
        else
          @errors << "profilePhoto required"
        end
      else
        @errors << "pca not found"
      end
    else
      @errors << "pca_id required"
    end
    if @errors.empty?
      render json: {"uploadedFileLocation":[resp]}, status: :ok
    else
      render json: {"error":@errors[0], error_description:""}, status: :unprocessable_entity
    end
  end

  def get_counties
    @counties = County.all
    #v2
    # @agency = @current_user.agency
    # @counties = County.joins(:recipient => :agency).where("agency.id = ?", @agency.id)
  end

  def get_qualifications
    @qualifications = Qualification.all
    #v2 
    # @agency = @current_user.agency
    # @qualifications = Qualification.joins(:pcas => :agency).where("agency.id = ?", @agency.id)
  end

# {"homecare_device":{"isActive":true,"isAndroid":true,"isIos":false,"user":102,"deviceToken":"edVBw4O4UKQ:APA91bEmOvIlPv2KpXWAr5ZPwupLJUf_hLFNbX2L-0qF6zu3jZ98xfQTVOkantVtWpHtaqb3H1qcN9X9WX67WkkJVTb4u2H_St9DniqL65P2bBBPXKcOiUzSY1sx8naYEgBcWqFZqpGq"}}

  def set_device
    @errors = []
    if params.has_key?("homecare_device") && params[:homecare_device].present?
      device_params = params[:homecare_device]
      if !device_params.has_key?("isAndroid") || !device_params.has_key?("isIos") || device_params[:isAndroid] == device_params[:isIos]
        @errors << "device type required (isAndroid & isIos)"
      elsif !device_params.has_key?("isActive")
        @errors << "isActive required"
      elsif !device_params.has_key?("user")
        @errors << "user required"
      elsif !device_params.has_key?("deviceToken")
        @errors << "deviceToken required"
      else
        @devices = Device.where(deviceToken: device_params[:deviceToken]).where.not(user_id: device_params[:user])
        if @devices.any?
          @devices.destroy_all
        end
        @device = Device.find_by(deviceToken:device_params[:deviceToken],user_id:device_params[:user])
        if !@device.present?          
          @device = Device.new
          device_params.each do |k,v|
            if k == "user"
              @device.user_id = v
            else
              @device[k] = v
            end
          end
          if !@device.save
            @errors << @device.errors.full_messages
          end
        end
      end
    else
      @errors << "homecare_device required"
    end
    if @errors.empty? 
      render json: {}, status: :created
    else
      render json: {"error":@errors[0], "errorMessage":""}, status: :unprocessable_entity
    end
  end


# {"homecare_request":{"recipient":42,"address":"337 Roslyn Pl, Minneapolis, MN 55419, USA","startShiftTime":"2018-01-10T14:20:00-0600","endShiftTime":"2018-01-10T17:20:00-0600","genderPreference":0,"yearsOfExperience":1,"qualifications":[2]}}

  def set_request
    @errors = []
    if params.has_key?("homecare_request") && params[:homecare_request].present?
      request_params = params[:homecare_request]
      if @current_user.is_recipient? && request_params.has_key?("recipient") && request_params[:recipient].present? && @current_user.recipient.id == request_params[:recipient]
        @request = @current_user.recipient.requests.new
        if request_params.has_key?("address") && request_params[:address].present?
          @request.address = request_params[:address]
        else
          @errors << "address required"
        end
        if request_params.has_key?("startShiftTime") && request_params[:startShiftTime].present?
          @startTime = Time.parse(request_params[:startShiftTime])
          if @startTime.present?
            @request.start_shift_time = @startTime
          else
            @errors << "startShiftTime format incorrect"
          end
        else
          @errors << "startShiftTime required"
        end
        if request_params.has_key?("endShiftTime") && request_params[:endShiftTime].present?
          @endTime = Time.parse(request_params[:endShiftTime])
          if @endTime.present?
            @request.end_shift_time = @endTime
          else
            @errors << "endShiftTime format incorrect"
          end
        else
          @errors << "endShiftTime required"
        end
        if request_params.has_key?("genderPreference")
          if request_params[:genderPreference].in?([0,1,2])
            @request.gender_preference = request_params[:genderPreference]
          else
            @errors << "genderPreference out of range"
          end
        else
          @errors << "genderPreference required"
        end 
        if request_params.has_key?("yearsOfExperience")
          @request.years_of_experience = request_params[:yearsOfExperience].is_a?(Integer) ? request_params[:yearsOfExperience] : 0
        else
          @errors << "yearsOfExperience required"
        end
        if request_params.has_key?("qualifications") && request_params[:qualifications].is_a?(Array)
          @request.qualification_ids = request_params[:qualifications]
        end
        if request_params.has_key?("additional_requirements")
          @request.description = request_params[:additional_requirements]
        elsif request_params.has_key?("additionalRequirements")
          @request.description = request_params[:additionalRequirements]
        end
        if request_params.has_key?("requested_id") && Pca.exists?(request_params[:requested_id])
          @request.requested_id = request_params[:requested_id]
        end
      else
        @errors << "Recipient required and must match current user"
      end    
    else
      @errors << "homecare_request required"
    end
    if @errors.empty? && @request.present?
      if @request.save
        @request.send_pca_notifications
        render json: {}, status: :created
      else
        render json: {"error":@request.errors.full_messages[0], "errorMessage":""}, status: :unprocessable_entity
      end
    else
      render json: {"error":@errors[0], "errorMessage":""}, status: :unprocessable_entity
    end
  end


# {"id":5492,"recipient":{"id":42,"skip_verification":false,"skip_gps":false,"first_name":"carrie","last_name":"hennessy","ma_number":"5","company_assigned_id":"lkjin","gender_preference":0,"county":{"id":262,"name":"Hennepin County"}},"address":"337 Roslyn Pl, Minneapolis, MN 55419, USA","rsvps":[{"id":4772,"pca":{"id":52,"first_name":"ramp","last_name":"sorlen","umpi":"0","company_assigned_id":"4","profilePhoto":"52id9zylg1495916154.jpg","home_address":"337 Roslyn Place Minneapolis MN 55419","qualifications":[{"id":2,"qualification":"CPR Certified"},{"id":12,"qualification":"Experience with Behaviors"},{"id":32,"qualification":"Experience making transfers"},{"id":52,"qualification":"Has driver license"}],"available":true,"services":[{"id":2,"service_number":1,"service_name":"personal care service"},{"id":12,"service_number":2,"service_name":"homemaking"},{"id":102,"service_number":11,"service_name":"temporary pca (on demand)"}],"years_of_experience":10,"gender":2,"counties":[{"id":12,"name":"Anoka County"},{"id":262,"name":"Hennepin County"}]},"accepted":true,"declined":false,"session_data_id":4162}],"gender_preference":0,"startShiftTime":"2018-01-18T15:00:00-0600","endShiftTime":"2018-01-18T17:00:00-0600","years_of_experience":0}

  def get_request
    @errors = []
    if params.has_key?("id") && params[:id].present?
      @request = Request.find_by(id:params[:id])
      if !@request.present?
        @errors << "No request found"
      elsif @current_user.is_pca? && @request.requested_id.present? && @request.requested_id != @current_user.pca.id
        @errors << "Direct Request for different worker"
      end
    else
      @errors << "Id required"
    end
    if !@errors.empty?
      render json: {"error":@errors[0], "errorMessage":""}, status: :unprocessable_entity
    end
  end

  def delete_request
    @errors = []
    if params.has_key?("id") && params[:id].present?
      @request = Request.find_by(id:params[:id])
      if @request.present?
        @request.send_recipient_cancelled
        if !@request.destroy
          @errors = @request.errors.full_messages
        end
      else
        @errors << "No request found"
      end
    else
      @errors << "Id required"
    end
    if @errors.empty?
      render json: {}, status: :no_content
    else
      render json: {"error":@errors[0], "errorMessage":""}, status: :unprocessable_entity
    end
  end



# [{"id":5472,"recipient":{"id":42,"skip_verification":false,"skip_gps":false,"first_name":"carrie","last_name":"hennessy","ma_number":"5","company_assigned_id":"lkjin","gender_preference":0,"county":{"id":262,"name":"Hennepin County"}},"address":"337 Roslyn Pl, Minneapolis, MN 55419, USA","rsvps":[],"gender_preference":0,"startShiftTime":"2018-01-10T14:20:00-0600","endShiftTime":"2018-01-10T17:20:00-0600","years_of_experience":1}]

  def get_requests_by_recipient
    @errors = []
    @recipient = Recipient.find_by(id:params[:id])
    if @recipient.present?
      @requests = @recipient.requests.in_future
    else
      @errors << "Recipient not found"
    end
    if !@errors.empty?
      render json: {"error":@errors[0], "errorMessage":""}, status: :unprocessable_entity
    end
  end


#[{"id":5492,"recipient":{"id":42,"skip_verification":false,"skip_gps":false,"first_name":"carrie","last_name":"hennessy","ma_number":"5","company_assigned_id":"lkjin","gender_preference":0,"county":{"id":262,"name":"Hennepin County"}},"address":"337 Roslyn Pl, Minneapolis, MN 55419, USA","rsvps":[],"gender_preference":0,"startShiftTime":"2018-01-18T15:00:00-0600","endShiftTime":"2018-01-18T17:00:00-0600","years_of_experience":0}]
[{"id":5463,"recipient":{"id":42,"skip_verification":false,"skip_gps":false,"first_name":"Carrie","last_name":"Hennessy","ma_number":"5","company_assigned_id":"lkjin","gender_preference":0,"county":{"id":262,"name":"Hennepin County"},"address":"337 Roslyn Pl, Minneapolis, MN 55419, USA"},"rsvps":[],"gender_preference":0,"startShiftTime":"2018-01-18T21:00:00-0500","endShiftTime":"2018-01-18T23:00:00-0500","years_of_experience":0}]

  def get_available_requests
    @errors = []
    if params.has_key?("pca_id") && params[:pca_id].present?
      @pca = Pca.find_by(id:params[:pca_id])
      if @pca.present?
        @requests = @pca.available_requests
      else
        @errors << "No Pca found"
      end
    else
      @errors << "Id required"
    end
    if !@errors.empty?
      render json: {"error":@errors[0], "errorMessage":""}, status: :unprocessable_entity    
    end
  end

  def get_accepted_requests
    @errors = []
    if params.has_key?("pca_id") && params[:pca_id].present?
      @pca = Pca.find_by(id:params[:pca_id])
      if @pca.present?
        @requests = @pca.requests.in_future.accepted_by(@pca)
      else
        @errors << "Pca not found"
      end
    else
      @errors << "pca_id required"
    end
    if !@errors.empty?
      render json: {"error":@errors[0], "errorMessage":""}, status: :unprocessable_entity    
    end
  end

#params {"homecare_rsvp":{"pca":52,"request":5492,"accepted":true}}
#response {"accepted":true,"rsvp_id":4772}
  def accept_request
    @errors = []
    if params.has_key?("homecare_rsvp") && params[:homecare_rsvp].present?
      rsvp_params = params[:homecare_rsvp]
      has_pca = rsvp_params.has_key?("pca") && rsvp_params[:pca].present?
      has_request = rsvp_params.has_key?("request") && rsvp_params[:request].present?
      accepts = rsvp_params.has_key?("accepted")
      if has_pca && has_request && accepts
        @pca = Pca.find_by(id:rsvp_params[:pca])
        if @pca.present?
          @request = Request.find_by(id:rsvp_params[:request])
          if @request.present? 
            if rsvp_params[:accepted].present? && rsvp_params[:accepted]
              if @request.rsvps.is_accepted.any?
                @errors << "request already accepted"
              else
                @sessionData = SessionDatum.new(pca_id:@pca.id, service_id:Service.first.id, ratio_id:Ratio.first.id, continueTimesheetNumber:4, sharedCareLocation_id:SharedCareLocation.first.id)
                if @sessionData.save(validate:false)
                  @timesheet = Timesheet.new(sessionData_id: @sessionData.id, recipient_id:@request.recipient_id, finished:0)
                  @timesheet.save(validate:false)
                  @eta = rsvp_params.has_key?("eta") && rsvp_params[:eta].present? ? rsvp_params[:eta] : Time.now
                  @rsvp = Rsvp.new(pca_id:@pca.id, request_id:@request.id, eta:@eta, accepted:true, declined:false, sessionData_id:@sessionData.id)
                  if !@rsvp.save
                    @errors = @rsvp.errors.full_messages
                  else
                    @request.send_pca_accepted
                  end
                else
                  @errors = @sessionData.errors.full_messages
                end             
              end
            else
              if @request.requested_pca.present?
                @request.send_pca_declined
              end
              @rsvp = Rsvp.new(pca_id:@pca.id, request_id:@request.id, eta:Time.now, accepted:false, declined:true)
              @rsvp.save
            end
          else
            @errors << "request not found"
          end
        else
          @errors << "pca not found"
        end
      else
        @errors << "homecare_rsvp requires pca, request and accepted"
      end
    else
      @errors << "homecare_rsvp required"
    end
    if @errors.empty?
      render json: {"accepted":@rsvp.accepted, "rsvp_id":@rsvp.id}, status: :ok
    else
      render json: {"error":@errors[0], "errorMessage":""}, status: :unprocessable_entity
    end
  end

  def decline_request
    @errors = []
    if params.has_key?("rsvp_id") && params[:rsvp_id].present?
      @rsvp = Rsvp.find_by(id:params[:rsvp_id])
      @pca = @rsvp.pca
      @request = @rsvp.request
      if @rsvp.present?
        if @rsvp.sessionDatum.present?
          @rsvp.sessionDatum.destroy
        end
        if !@rsvp.destroy
          @errors = @rsvp.errors.full_messages
        else
          @request.send_pca_cancelled(@pca)
        end
      else
        @errors << "rsvp not found"
      end
    else
      @errors << "rsvp id required"
    end
    if @errors.empty?
      render json: {}, status: :no_content
    else
      render json: {"error":@errors[0], "errorMessage":""}, status: :unprocessable_entity
    end
  end


  def get_pca_contacts
    @errors = []
    if @current_user.is_recipient?
      @pcas = @current_user.recipient.agency.pcas.nonarchived.sort{|x,y| x.name <=> y.name }
    else
      @errors << "Must be recipient"
    end
    if !@errors.empty?
      render json: {"error":@errors[0], "errorMessage":""}, status: :unprocessable_entity
    end
  end

private

  def set_api_user 
    pattern = /^Bearer /
    header  = request.headers["Authorization"]
    token = header.gsub(pattern, '') if header && header.match(pattern)
    @access_token = AccessToken.find_by(token:token)
    if @access_token.present? && Time.at(@access_token.expires_at.to_i) > DateTime.now
      @current_user = @access_token.user
    end
    if !@current_user.present?
      render json: {errors:["Unauthorized"] }, status: :unauthorized
    end
  end


  def set_default_response_format
    request.format = :json
  end

end