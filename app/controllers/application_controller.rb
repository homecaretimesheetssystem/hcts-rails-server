class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_current_user

  





 def set_current_user
   if session[:current_user_id].present?
     user = User.find_by(id:session[:current_user_id])
     if user.present?
       @current_user = user
     end
   end
   if !@current_user.present?
     redirect_to login_path, notice: "Please login to continue"
   end
 end
 
 def verify_admin
   set_current_user
   if !@current_user.is_admin? && !@current_user.is_pca?
     redirect_to root_path, error: "Whoops! You are not allowed to view that page."
   end
 end


private

rescue_from ActionController::InvalidAuthenticityToken, with: :redirect_to_referer_or_path

def redirect_to_referer_or_path
  flash[:notice] = "Please try again."
  if request.referer
    redirect_to request.referer
  else
    redirect_to root_url
  end
end

end
