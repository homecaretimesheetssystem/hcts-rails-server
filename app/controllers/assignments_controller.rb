class AssignmentsController < ApplicationController
  before_action :set_assignment, only: [:edit, :update, :destroy]
  before_action :set_agency, only: [:new, :create, :index]
  before_action :set_users, only: [:index, :new, :create, :edit, :update]

  def index
    if params.has_key?("page")
      @page = Integer(params[:page]) rescue 1
    else
      @page = 1
    end 
    @limit = 10
    offset = (@page - 1) * @limit
    if params.has_key?("button")
      redirect_to assignments_path(utf8:"true")
    elsif @agency.present?
      @assignments = @agency.assignments
      if params.has_key?("recipient") && params[:recipient].present?
        recip_id = Integer(params[:recipient]) rescue false
        if recip_id.present?
           @assignments = @assignments.select{|x| x.recipient_id == recip_id }
        end
      end
      if params.has_key?("worker") && params[:worker].present?
        pca_id = Integer(params[:worker]) rescue false
        if pca_id.present?
          @assignments = @assignments.select{|x| x.pca_id == pca_id}
        end
      end
      @count = @assignments.count
      get_limit = (offset + @limit) > @count ? @count - 1 : offset + @limit - 1 
      @assignments = @assignments[offset..get_limit]
    else
      @assignments = RecipientPca.all
      if params.has_key?("agency") && params[:agency].present?
        @assignments = Agency.find(params[:agency]).assignments
        if params.has_key?("recipient") && params[:recipient].present?
          recip_id = Integer(params[:recipient]) rescue false
          if recip_id.present?
             @assignments = @assignments.select{|x| x.recipient_id == recip_id }
          end
        end
        if params.has_key?("worker") && params[:worker].present?
          pca_id = Integer(params[:worker]) rescue false
          if pca_id.present?
            @assignments = @assignments.select{|x| x.pca_id == pca_id}
          end
        end
        @count = @assignments.count
        get_limit = (offset + @limit) > @count ? @count - 1 : offset + @limit - 1 
        @assignments = @assignments[offset..get_limit]
      else
        if params.has_key?("recipient") && params[:recipient].present?
          @assignments = @assignments.where(recipient_id: params[:recipient_id])
        end
        if params.has_key?("worker") && params[:worker].present?
          @assignments = @assignments.where(pca_id:params[:worker])
        end
        @count = @assignments.count
        @assignments = @assignments.limit(@limit).offset(offset)
      end
    end
    if @count == 0
      flash.now.alert = "No Assignments match your criteria"
    end
  end

  def new
    @assignment = RecipientPca.new
  end

  def create
    @assignment = RecipientPca.new(assignment_params)
    respond_to do |format|
      if RecipientPca.where(recipient_id:@assignment.recipient_id, pca_id:@assignment.pca_id).any? || @assignment.save
        format.html { redirect_to assignments_url, notice: 'Assignment successfully created' }
        format.json { render @assignment, status: :created }
      else
        format.html { render :new }
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit

  end
 
  def update
    respond_to do |format|
      if @assignment.update(assignment_params)
        format.html { redirect_to assignments_url, notice: 'Assignment successfully updated' }
        format.json { render @assignment, status: :ok }
      else
        format.html { render 'edit' }
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @assignment.destroy
    respond_to do |format|
      format.html { redirect_to assignments_url, notice: 'Assignment successfully deleted' }
      format.json { render json: {deleted:true}, status: :ok } 
    end 
  end

  

private

  def set_assignment
    @assignment = RecipientPca.find(params[:id])
  end

  def set_agency
    if params.has_key?("agency_id")          
      @agency = Agency.find_by(id:params[:agency_id])
    end
    if !@agency.present?
      if @current_user.is_admin?
        @agency = Agency.find_by(id:session[:current_agency_id])
      else
        @agency = @current_user.agency
      end
    end
  end
  
  def set_users
    if @agency.present?
      @recipients = @agency.recipients.sort{|x,y| x.name <=> y.name}
      @pcas = @agency.pcas.sort{|x,y| x.name <=> y.name}
    else
      @recipients = Recipient.all
      @pcas = Pca.all
    end
  end

  def assignment_params
    params.require(:recipient_pca).permit(:pca_id, :recipient_id, :is_temp)
  end


end