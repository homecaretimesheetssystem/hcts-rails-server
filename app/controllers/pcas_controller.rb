class PcasController < ApplicationController
  before_action :set_pca, only: [:show, :edit, :update, :destroy]
  before_action :set_agency, only: [:new, :create, :edit, :index]
  before_action :set_services, only: [:edit, :update, :new, :create]

  # GET /pcas
  # GET /pcas.json
  def index
    if @agency.present?
      @pcas = @agency.pcas
    else
      @pcas = Pca.all
    end
  end

  # GET /pcas/1
  # GET /pcas/1.json
  def show
  end

  # GET /pcas/new
  def new
    if @agency.present?
      @pca = Pca.new(agency_id:@agency.id)
    else
      @pca = Pca.new
    end
    @pca.build_user
  end

  # GET /pcas/1/edit
  def edit
  end

  # POST /pcas
  # POST /pcas.json
  def create
  #  params[:pca][:user_attributes][:firstName] = params[:pca][:firstName]
  #  params[:pca][:user_attributes][:lastName] = params[:pca][:lastName]
  #  params[:pca][:pca_name] = "#{params[:pca][:firstName]} #{params[:pca][:lastName]}"
  #  params[:pca][:user_attributes][:roles] = User::PCA_ROLE
    if !@agency.present?
      @agency = Agency.find_by(id:@pca.agency_id)
    end
    @pca = Pca.new(pca_params)
    @pca.set_user
    respond_to do |format|
      if @pca.save
        format.html { redirect_to users_path(pcas:true), notice: 'Worker was successfully created.' }
        format.json { render :show, status: :created, location: @pca }
      else
        format.html { render :new }
        format.json { render json: @pca.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pcas/1
  # PATCH/PUT /pcas/1.json
  def update
    respond_to do |format|
      if @pca.update(pca_params)
        format.html {
          if @current_user.is_pca?
            redirect_to edit_pca_path(@pca), notice: 'Worker was successfully updated.' 
          else  
            redirect_to users_path(pcas:true), notice: 'Worker was successfully updated.' 
          end
        }
        format.json { render :show, status: :ok, location: @pca }
      else
        format.html { render :edit }
        format.json { render json: @pca.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pcas/1
  # DELETE /pcas/1.json
  def destroy
    @pca.destroy
    respond_to do |format|
      format.html { redirect_to pcas_url, notice: 'Pca was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pca
      @pca = Pca.find(params[:id])
      @agency = @pca.agency
    end
  
    def set_services
      @services = Service.all
    end

    def set_agency
      if params.has_key?("agency_id")          
        @agency = Agency.find_by(id:params[:agency_id])
      end
      if @current_user.is_admin?
        @agency = Agency.find_by(id:session[:current_agency_id])
      elsif @current_user.is_pca?
        @agency = @current_user.pca.agency
      elsif !@agency.present?
        @agency = @current_user.agency
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pca_params
      params.require(:pca).permit(:id, :user_id, :agency_id, :firstName, :lastName, :umpi, :company_assigned_id, :phoneNumber, :home_address, :available, :years_of_experience, :gender, :photo, :service_ids => [], :user_attributes => [:id, :username, :email, :enabled, :password, :password_confirmation, :locked, :expired, :roles, :firstName, :lastName, :archived])
    end
end
