class AgenciesController < ApplicationController
  before_action :set_agency, only: [:show, :edit, :update, :destroy, :email_settings, :email_submit, :email_delete, :get_image, :subscription, :cancel_subscription, :subscription_submit]
  skip_before_action :set_current_user, only: [:new, :create]

  # GET /agencies
  # GET /agencies.json
  def index
    if @current_user.is_admin?
      session[:current_agency_id] = nil
      @agencies = Agency.all
    else
      redirect_to users_path, alert: "Unauthorized"
    end
  end

  # GET /agencies/1
  # GET /agencies/1.json
  def show
    if @current_user.is_admin?
      session[:current_agency_id] = params[:id]
      redirect_to dashboard_path
    else
      redirect_to users_path, alert: "Unauthorized"
    end

  end

  # GET /agencies/new
  def new
    @agency = Agency.new
    @agency.users.new
  end

  # GET /agencies/1/edit
  def edit
  end

  # POST /agencies
  # POST /agencies.json
  def create
    @agency = Agency.new(agency_params)
    a_params = params[:agency]
    if a_params[:accept_terms].present?        
      if @agency.save
        if @agency.set_source_subscription(params[:subscription], params[:stripeToken])
          @user = @agency.users.first
          if @user.present?
            @user.last_login = DateTime.now
            @user.save
            @current_user = @user
            session[:current_user_id] = @user.id
            redirect_to dashboard_path, notice: 'Successfully signed up'
          else
            redirect_to root_path, notice: 'Please contact support'            
          end
        else
          redirect_to root_path, notice: 'Please contact support'            
        end
      else
        render :new
      end
    else
      @agency.errors.add(:base, :invalid, message:"Terms must be accepted")
      render :new
    end
  end

  # PATCH/PUT /agencies/1
  # PATCH/PUT /agencies/1.json
  def update
    respond_to do |format|
      if @agency.update(agency_params)
        format.html { redirect_to edit_agency_path(@agency), notice: 'Agency was successfully updated.' }
        format.json { render :show, status: :ok, location: @agency }
      else
        format.html { render :edit }
        format.json { render json: @agency.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /agencies/1
  # DELETE /agencies/1.json
  def destroy
    @agency.destroy
    respond_to do |format|
      format.html { redirect_to agencies_url, notice: 'Agency was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def email_settings
    @email_setting = @agency.email_settings.build
    @email_settings = Agency.find_by(id:@agency.id).email_settings

  end

  def email_submit
    @email_setting = EmailSetting.new(email_params)
    if @email_setting.save
      redirect_to agency_email_settings_path(@agency)
    else
      render 'email_settings'
    end
  end
  
  def email_delete
    @email_setting = EmailSetting.find_by(id:params[:id])
    if @email_setting.destroy
      redirect_to agency_email_settings_path(@agency)
    else
      render 'email_settings'
    end
  end

  def subscription
    source = @agency.get_source
    @subscription_name = @agency.subscription_name    
    if source.present?
      @card = source.sources.data.first
    end   
  end

  def cancel_subscription
    @subscriptions = @agency.get_subscriptions
    if @subscriptions.present?
      @subscriptions.each do |sub|
        @agency.cancel_subscription(sub.id)
      end
    end
    redirect_to agency_subscription_settings_path(@agency), notice: "Subscription Cancelled" 
  end


  def subscription_submit
    if params.has_key?("subscription") || params.has_key?("stripeToken")
      source = @agency.get_source
      if params[:subscription].present?
        if @agency.subscription_name != params[:subscription]
          @subscriptions = @agency.get_subscriptions
          if @subscriptions.present?
            @subscriptions.each do |sub|
              if sub.plan.name != params[:subscription]
                @agency.cancel_subscription(sub.id)
              end
            end
          end 
          if !@agency.get_subscriptions.present?
            create_sub = @agency.create_subscription(params[:subscription])
          end
          if create_sub.present?
            redirect_to agency_subscription_settings_url(@agency), notice: "Subscription Updated"
          else
            @subscription_name = @agency.subscription_name    
            if source.present?
              @card = source.sources.data.first
            end 
            render 'subscription', alert: "Error 1"
          end
        else
          redirect_to agency_subscription_settings_url(@agency), notice: "Subscription Updated"
        end
     elsif params[:stripeToken].present?
       update_card = @agency.replace_card(params[:stripeToken])
       if update_card
         redirect_to agency_subscription_settings_url(@agency), notice: "Card Updated"
       else
        if source.present?
          @card = source.sources.data.first
        end
        render 'subscription', alert: "Error 2"
       end 
      else
        @subscription_name = @agency.subscription_name    
        if source.present?
          @card = source.sources.data.first
        end 
        render 'subscription', alert: "Error 3"
      end
    else
      @subscription_name = @agency.subscription_name    
      if source.present?
        @card = source.sources.data.first
      end 
      render 'subscription', alert: "Error"
    end
  end


  def get_image
    @img_arr = @agency.get_image
    if @img_arr.present?
      data = open(@img_arr[0])
      send_data data.read, filename: @img_arr[1], type: @img_arr[2]
    else
      render json: {errors:"No file set"}, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_agency
      if params.has_key?(:id)
        @agency = Agency.find(params[:id])
      elsif params.has_key?(:agency_id)
        @agency = Agency.find_by(id:params[:agency_id])
      elsif @current_user.is_admin?
        @agency = Agency.find_by(id:session[:current_agency_id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def agency_params
      the_params = params.require(:agency).permit(:agency_name, :state_id, :week_start_date, :start_time, :phoneNumber, :imageName, :image, :pca_finder_active, :users_attributes => [:id, :firstName, :lastName, :username, :email, :password, :password_confirmation, :roles])
      
      if @current_user.is_admin?
        return the_params
      end
      
      the_params.except(:pca_finder_active)
    end

    def email_params
      params.require(:email_setting).permit(:email, :agency_id)
    end


end
