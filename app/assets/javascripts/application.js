// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require Chart.bundle.min
//= require_tree .



function jumpTo(page) {
  switch(page) {
    case 1:
      location.replace(BASE_URL);
      break;
   }
}


function addTimeOutForm() {
  var hidden_delete = document.getElementById("timesheet_session_datum_attributes_time_out_attributes__destroy");
  var hidden_rows = document.getElementsByClassName("time_out");
  var add_button = document.getElementById("addTimeOut");
  var remove_button = document.getElementById("removeTimeOut");
  for(var i=0;i<hidden_rows.length;i++) {
    hidden_rows[i].style.display = "table-row"; 
  }
  hidden_delete.value = false;
  remove_button.style.display = "block";
  add_button.style.display = "none";
}

function removeTimeOutForm() {
  var hidden_delete = document.getElementById("timesheet_session_datum_attributes_time_out_attributes__destroy");
  var hidden_rows = document.getElementsByClassName("time_out");
  var add_button = document.getElementById("addTimeOut");
  var remove_button = document.getElementById("removeTimeOut");
  for(var i=0;i<hidden_rows.length;i++) {
    hidden_rows[i].style.display = "none"; 
  }
  hidden_delete.value = true;
  remove_button.style.display = "none";
  add_button.style.display = "block";
}

function headbtnFunction() {
    document.getElementById("btnDropdown").classList.toggle("show");
}

function pcaFilterableOption1Function() {
    document.getElementById("pcaFilterableOption1Container").classList.toggle("show");
}

function newBtnClose(event) {
  var dropdowns = document.getElementsByClassName("dropdown-content");
  var i;
  for (i = 0; i < dropdowns.length; i++) {
    var openDropdown = dropdowns[i];
    if (openDropdown.classList.contains('show')) {
      openDropdown.classList.remove('show');
    }
  }
}

/*-- Assignments page dropdown form field  ----*/

function toggleFilters() {
  var filterDiv = document.getElementById("filters");
   filterDiv.classList.toggle("hidden");
  var caret = document.getElementById("filterCaret");
  if (filterDiv.classList.contains("hidden")) {
    caret.innerHTML = "&#9660;";
} else {
    caret.innerHTML = "&#9650;";
  }
}


/*Assignment Tools dropdown menu */

function showTools() {
  var btn = document.getElementById("toolBtn");
  if (btn.getAttribute("disabled") != "disabled") {
    var dropDown = document.getElementById("toolsDropdown")
    if (dropDown.style.display != "block") {
      dropDown.style.display = "block";
    }
    else {
      dropDown.style.display = "none";
    }
  }
}

function setTools(checked) {
  var checkboxes = document.querySelectorAll('input[type="checkbox"]:enabled');
  var numChecked = 0;
  var all_check = document.getElementById("timesheet_all");
  for (var i=0; i < checkboxes.length;i++) {
    if (checkboxes[i].checked && checkboxes[i] != all_check) {
      numChecked += 1;
    }
  }

  if (numChecked != checkboxes.length - 1 || numChecked == 0) {
    all_check.checked = false;
  }
  else {
    all_check.checked = true;
  }
  var btn = document.getElementById("toolBtn");
  if (numChecked > 0) {
    btn.setAttribute("disabled", "");
    if (btn.hasAttribute("title")) {
      btn.removeAttribute("title");
    }
  }
  else {
    btn.setAttribute("disabled","disabled");
    btn.setAttribute("title","You must select at least one Time Entry to use Tools");
  }
}

function closeTools(event)  {
  var tools = document.getElementById("toolsDropdown");
  if (tools != null) {
    document.getElementById("toolsDropdown").style.display = "none";
  }
}


function buildModal(title, content) {
var modal = document.getElementById('modal'); 
  if(!modal){modal=document.createElement('div');}
  modal.id = "modal";
  modal.position = "fixed";
  modal.style.margin = "100px auto";
  modal.style.display = "block";
  modal.style.width = "350px"
  modal.style.height = "250px";
  modal.style.textAlign = "center";
  modal.style.verticalAlign = "middle";
  modal.style.backgroundColor = "rgba(255,255,255,1)";
  var head = document.getElementById('modal-head');
  if(!head){head=document.createElement('div');}
  else{head.innerHTML = "";}
  head.id = "modal-head";
  head.innerHTML = title;
  head.style.height = "2rem";
  head.style.backgroundColor = "#3F7798";
  head.style.color = "#fff";
  head.style.padding = "5px";
  head.style.fontSize = "1.2rem";  
  closeButton = document.createElement("a");
  closeButton.innerHTML = "&#x2716;";
  closeButton.style.color = "#fff";
  closeButton.style.float = "right";
  closeButton.style.cursor = "pointer";
  closeButton.onclick = function() { closeModal(); }
  head.appendChild(closeButton);
  modal.appendChild(head);
  var body = document.createElement("div");
  body.style.padding = "10px";
  body.style.fontSize = "1rem";
  body.id = "modal-body";
  body.appendChild(content);
  modal.appendChild(body);
  return modal;
}

function makeSpacer() {
  var spacer = document.createElement("div");
  spacer.style.width = "5rem";
  spacer.style.display = "inline-block";
  return spacer;
}

function getTimesheets() {
  var checkboxes = document.querySelectorAll('input[type="checkbox"]:enabled');
  var numChecked = 0;
  var timesheets = [];
  for (var i=0; i < checkboxes.length;i++) {
    if (checkboxes[i].checked && checkboxes[i].value != 0) {
      timesheets.push(checkboxes[i].value);
      numChecked += 1;
    }
  }
  return [numChecked, timesheets];
}


function compareTimesheets() {
  var timesheetArray = getTimesheets();
  var numChecked = timesheetArray[0];
  var timesheets = timesheetArray[1];
  if (numChecked > 0) {
    var href = "goals?";
    for (var i=0;i<timesheets.length;i++) {
      if (i>0) { href += "&"; }
      href += "timesheet_ids[]=" + timesheets[i];
    }
    window.location = href;
  }
}

function graphTimesheets() {
  var timesheetArray = getTimesheets();
  var numChecked = timesheetArray[0];
  var timesheets = timesheetArray[1];
  if (numChecked > 0) {
    var href = "cares?";
    for (var i=0;i<timesheets.length;i++) {
      if (i>0) { href += "&"; }
      href += "timesheet_ids[]=" + timesheets[i];
    }
    window.location = href;
  }
}


function downloadTimesheets() {
  var timesheetArray = getTimesheets();
  var numChecked = timesheetArray[0];
  var timesheets = timesheetArray[1];
  if (numChecked > 0) {
    var body = document.createElement("div");
    var p = document.createElement("p");
    p.innerHTML = "Choose how you want to download the selected files. Either choose PDF or CSV format. Your selected timesheets will be placed inside a zip file";
    var spacer = makeSpacer();
    var pdfHref = "timesheets/get-zip?file_type=2"; 
    var csvHref = "timesheets/get-zip?file_type=1";
    for (var i=0;i<timesheets.length;i++) {
       pdfHref += "&timesheet_ids[]=" + timesheets[i];
       csvHref += "&timesheet_ids[]=" + timesheets[i];
    }
    var pdfButton = makeButton("PDF");
    pdfButton.href = pdfHref;
    pdfButton.target = "_blank";
    var csvButton = makeButton("CSV");
    csvButton.href = csvHref;
    csvButton.target = "_blank";
    body.appendChild(p);
    body.appendChild(pdfButton);
    body.appendChild(spacer);
    body.appendChild(csvButton);
    var modal = buildModal("Timesheet Download", body);
    show_img(modal, false);
  }
}


function archiveTimesheets() {
  var timesheetArray = getTimesheets();
  var numChecked = timesheetArray[0];
  var timesheets = timesheetArray[1];
  if (numChecked > 0) {
    var body = document.createElement("div");

    var p = document.createElement("p");
    p.innerHTML = "Are you sure you want to archive these timesheets?"
    body.appendChild(p);

    var archiveHref = "timesheets/archive-all?";
    for (var i=0;i<timesheets.length;i++) {
      if (i>0) {
        archiveHref += "&";
      }
      archiveHref += "timesheet_ids[]=" + timesheets[i];
    }
    var archiveButton = makeButton("Archive");
    archiveButton.href = archiveHref;
    body.appendChild(archiveButton);

    var modal = buildModal("Archive Timesheets", body);
    show_img(modal, false);
  }
}

function archiveTimesheet(id) {
  var body = document.createElement("div");
  var p = document.createElement("p");
  p.innerHTML = "Are you sure you want to archive this timesheet?"
  body.appendChild(p);
  var archiveHref = "timesheets/archive-all?timesheet_ids[]=" + id;
  var archiveButton = makeButton("Archive");
  archiveButton.href = archiveHref;
  body.appendChild(archiveButton);
  var modal = buildModal("Archive Timesheet", body);
  show_img(modal, false);
}

function showContact() {
  var body = document.createElement("div");
  var p1 = document.createElement("p");
  var l1 = document.createElement("a");
  l1.href = "https://google.com/maps/search/?api=1&query=2562+7th+Ave+E+%23201,+North+St+Paul,+MN+55109";
  l1.innerHTML = "2562 7th Avenue Suite 201,<br>North Saint Paul, MN 55109";
  l1.target = "_blank";
  p1.appendChild(l1);
  body.appendChild(p1);
  var p2 = document.createElement("p");
  p2.innerHTML = "Phone: <a href='tel:6128684512'>612-868-4512</a>";
  body.appendChild(p2);
  var p3 = document.createElement("p");
  p3.innerHTML = "Fax: 763-592-7960";
  body.appendChild(p3);
  var p4 = document.createElement("p");
  var l2 = document.createElement("a");
  l2.href = "mailto:andre@pcapartners.net";
  l2.innerHTML = "Email Us";
  p4.appendChild(l2);
  body.append(p4);
  var title = "Contact PCA Partners Inc";
  var modal = buildModal(title,body);
  show_img(modal,false);
}

function makeButton(text) {
 var button = document.createElement("a");
 button.innerHTML = text;
 button.style.height = "2rem";
 button.style.minWidth = "3.5rem";
 button.style.padding = "0.2rem";
 button.style.color = "#fff";
 button.style.backgroundColor = "#3F7798";
 button.style.textAlign = "center";
 button.style.margin = "0 auto";
 button.style.border = "1px solid #3F7798";
 button.style.borderRadius = "0.2rem";
 button.style.cursor = "pointer";
 return button;
}

function closeModal() {
  var _ = document.getElementById("_");
  if (_ != null) {
    document.body.removeChild(_);
  }
}


function setModalToImage(height, width) {
  var modal = document.getElementById("modal");
  modal.style.width = 50 + width + 'px';
  modal.style.height = 50 + height + 'px';
}

function removeLoader() {
   document.getElementById("modal-body").removeChild(document.getElementById("loader"));
}


function showSig(timesheetId, which) {
 var div = document.createElement("div");
 div.className = "loader";
 div.id = "loader";
 if (which) {
   var modal = buildModal("Recipient Signature", div);
 }
 else {
   var modal = buildModal("Worker Signature", div);
 }
 show_img(modal, true);
 var img = new Image();
 img.onload = function() {
   removeLoader();
   setModalToImage(this.height, this.width);
   var canvas = document.getElementById("sigcanvas");
   canvas.width = this.width;
   canvas.height = this.height;
   var ctx = canvas.getContext('2d');
   ctx.drawImage(this,0,0,this.width,this.height,0,0,canvas.width,canvas.height);
 }
 if (which) {
   var str = "timesheets/" + timesheetId + "/recipient-sig"
 }
 else {
   var str = "timesheets/" + timesheetId + "/worker-sig"
 }
 var canvas = document.createElement("canvas");
 canvas.id = "sigcanvas"; 
 document.getElementById("modal-body").appendChild(canvas);
 img.src = str;
}



function showVerifImage(id) {
 var div = document.createElement("div");
 div.className = "loader";
 div.id = "loader";
 var modal = buildModal("Verification Photo", div);
 show_img(modal, false);
 var img = new Image();
 img.onload = function() {
   removeLoader();
   setModalToImage(this.height, this.width);
   var canvas = document.getElementById("imgcanvas");
   canvas.width = this.width;
   canvas.height = this.height;
   var ctx = canvas.getContext('2d');
   ctx.drawImage(this,0,0,this.width,this.height,0,0,canvas.width,canvas.height);
 }
 var canvas = document.createElement("canvas");
 canvas.id = "imgcanvas";
 document.getElementById("modal-body").appendChild(canvas);
 var str = "verifications/" + id  + "/get-image";
 img.src = str;
}

/* responsive side nav slide out  --*/
function openNav() {
  document.getElementById("mySidenav").style.width = "190px";
  document.getElementById("main").style.marginLeft = "190px";

}

function closeNav(event) {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft = "0";
}

var lastFile = 0;
function fileFunction(which) {
  if (lastFile == which) {
    timesheetClose();
  }
  else { 
    if (lastFile != 0) {
      timesheetClose();
    } 
    lastFile = which;
    document.getElementById("content_timesheet_" + which).style.display = "block";
  }
}

function timesheetClose() {
 if (lastFile != 0) {
   document.getElementById("content_timesheet_" + lastFile).style.display = "";
   lastFile = 0;
 }
}

function checkAll(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]:enabled');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
    setTools(source.checked);
}


function dayRange() {
 document.getElementById("daySpan").style.textDecoration = "none";
 document.getElementById("dateSpan").style.textDecoration = "underline";
 var dayRange = document.getElementById("dayRange")
 setDisabled(dayRange, false);
 var rangeStart = document.getElementById("dateRangeStart")
 rangeStart.style.display = "none";
 setDisabled(rangeStart, true);
 var rangeEnd = document.getElementById("dateRangeEnd")
 rangeEnd.style.display = "none";
 setDisabled(rangeEnd, true);
}


function setDisabled(ele, disable) {
  var selects = ele.getElementsByTagName("SELECT");
  selects.forEach(function() {
    select.disabled = disable;
  });
}


function dateRange() {
 document.getElementById("daySpan").style.textDecoration = "underline";
 document.getElementById("dateSpan").style.textDecoration = "none";
 var dayRange = document.getElementById("dayRange")
 setDisabled(dayRange, true);
 var rangeStart = document.getElementById("dateRangeStart");
 rangeStart.style.display = "inline-block";
 setDisabled(rangeStart, false);
 var rangeEnd = document.getElementById("dateRangeEnd")
 rangeEnd.style.display = "inline-block";
 setDisabled(rangeEnd, false);
}


function hideNotice() {
  var noticeP = document.getElementById("notice");
  if (noticeP != null) {
    noticeP.style.display = "none";
  }
}

function hideAlert() {
  var alertP = document.getElementById("alert");
  if (alertP != null) {
    alertP.style.display = "none";
  }
}

function show_img(ele, display_img) {
 var _=document.getElementById('_'); 
 if(!_){_=document.createElement('div');document.body.appendChild(_);}
 var w = Math.max(document.documentElement.clientWidth || 0);
 var h = Math.max(document.documentElement.clientHeight, document.body.offsetHeight || 0);
 _.id='_';
 _.style.top=0;
 _.style.left=0;
 if (w != 0) {
   _.style.width = w + "px";
 }
 if (h != 0) {
   _.style.height = h + "px";
 }
 _.style.zIndex=9999; 
 _.style.position='fixed';
  _.style.margin="auto";
  _.style.backgroundColor="rgba(238,238,238,0.6)";
  _.style.textAlign="center";
  _.style.verticalAlign="middle";
  _.innerHTML = "";
  if (display_img) {
    ele.style.display="none";
    _.appendChild(ele);
  //  var img = ele.getElementsByTagName("IMG")[0];
 //   console.log(img.clientWidth + "x" + img.clientHeight);
//    ele.style.width =  + "px";
//    ele.style.height = img.offsetHeight + "px";
    ele.style.display = "block";
    console.log("attached");
 //   img = document.getElementById("_").getElementsByTagName("IMG")[0];
 //   console.log(img);
 //   console.log(img.clientWidth + "x" + img.clientHeight);

  }
  else {
    _.appendChild(ele);
  }
}

(function() {

  window.addEventListener("resize", resizeThrottler, false);

  var resizeTimeout;
  function resizeThrottler() {
    // ignore resize events as long as an actualResizeHandler execution is in the queue
    if ( !resizeTimeout ) {
      resizeTimeout = setTimeout(function() {
        resizeTimeout = null;
        actualResizeHandler();
     
       // The actualResizeHandler will execute at a rate of 15fps
       }, 66);
    }
  }

  function actualResizeHandler() {
    // handle the resize event
    _ = document.getElementById("_");
    if (_ != null) { 
      var w = Math.max(document.documentElement.clientWidth || 0);
      var h = Math.max(document.documentElement.clientHeight, document.body.offsetHeight || 0);
      if (w != 0) {
        _.style.width = w + "px";
      }
      if (h != 0) {
        _.style.height = h + "px";
      }
    }
  }

}());


/* Close dropdown if user clicks outside  */
window.onclick = function(event) {
  var eTarget;
  if (event.target.matches("i")) {
    eTarget = event.target.parentNode;
  }
  else {
    eTarget = event.target;
  }
  if (eTarget.matches("input[type=checkbox]") && !eTarget.matches("#timesheet_all")) {
   var tools = document.getElementById("toolBtn");
    if (tools != null) {
      setTools(eTarget.checked);
    }
  }

  if (!eTarget.matches('#newOpen') && !eTarget.matches('#newOpenCaret')) {
    newBtnClose(event);
  }
  if (!eTarget.matches("[id^=timesheet]") && !eTarget.parentNode.matches("[id^=content_timesheet]")) {
    timesheetClose(event);
  }
  if (!eTarget.matches('.tools-btn') && !eTarget.parentNode.matches('.tools-btn') && !eTarget.parentNode.matches('#toolsDropDown')) {
    closeTools();
  }
}
