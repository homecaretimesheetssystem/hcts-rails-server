var card, sform, pform;
window.onload = function(e) {  
  if (document.getElementById("card-element") != null) {
    var elements = stripe.elements();
    var style = {
      base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

// Create an instance of the card Element
  card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
  card.mount('#card-element');
  card.addEventListener('change', function(event) {
    var displayError = document.getElementById('card-errors');
    if (event.error) {
      displayError.textContent = event.error.message;
    } else {
      displayError.textContent = '';
    }
  });
 }
 if (document.getElementById("payment-form") != null) { 
   pform = document.getElementById('payment-form');
   pform.addEventListener('submit', function(event) {
     event.preventDefault();
     var errorElement = document.getElementById('card-errors');
     errorElement.textContent = "";
       stripe.createToken(card).then(function(result) {
         if (result.error) {
           // Inform the customer that there was an error
           errorElement.textContent = result.error.message;
         } else {
           // Send the token to your server
           stripeTokenHandler(result.token);
         }
       });

   });
 }
 if (document.getElementById("new_agency") != null) { 
   pform = document.getElementById('new_agency');
   pform.addEventListener('submit', function(event) {
     event.preventDefault();
     var errorElement = document.getElementById('card-errors');
     errorElement.textContent = "";
     var subCheck = checkSub();
     if (subCheck) {
       stripe.createToken(card).then(function(result) {
         if (result.error) {
           // Inform the customer that there was an error
           errorElement.textContent = result.error.message;
         } else {
           // Send the token to your server
           stripeTokenHandler(result.token);
         }
       });
     }
   });
 }
 if (document.getElementById("sub-form") != null) {
   sform = document.getElementById("sub-form");
   sform.addEventListener('submit', function(event) {
     event.preventDefault();
     var subDiv = document.getElementById("subscription");
     subDiv.parentNode.classList.remove("field_with_errors", "middle");
     var subscription = subDiv.value;
     var errorElement = document.getElementById('sub-errors');
     errorElement.textContent = "";     
     if (subscription != "No Active Subscription") {
        sform.submit();
     }
     else {
       subDiv.parentNode.classList.add("field_with_errors", "middle");
       errorElement.textContent = "Select a subscription";
     }
   });
 }
}

function checkSub() {
  resp = true;
  var subDiv = document.getElementById("subscription");
  subDiv.parentNode.classList.remove("field_with_errors");
  var subscription = subDiv.value;
  var errorElement = document.getElementById('card-errors');
  console.log(subscription);
  if (subscription == "No Active Subscription") {
    resp = false;
    subDiv.parentNode.classList.add("field_with_errors");
    errorElement.textContent = "Select a subscription";
  }
  return resp;
}

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form;
  if (document.getElementById('payment-form') != null) {
    form = document.getElementById('payment-form');
  }
  if (document.getElementById('new_agency') != null) {
    form = document.getElementById('new_agency');
  }
  if (form != null) {
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('id', 'stripeToken');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    // Submit the form
    form.submit();
  }
}


function changeSubscription(showMe) {
  var div = document.getElementById("change_sub");
  if (div != null) {
    var changeSub = document.getElementById("changeSub");
    var changeC = document.getElementById("card_change");
    if (showMe) {
      div.classList.remove("collapse");
      changeSub.classList.add("collapse");
      changeC.classList.add("collapse");
    }
    else {
      div.classList.add("collapse");
      changeSub.classList.remove("collapse");
      changeC.classList.remove("collapse");
    }
  }
}

function changeCard(showMe) {
  var div = document.getElementById("change_card");
  if (div != null) {
    var changeDiv = document.getElementById("changeCard");
    var subB = document.getElementById("sub_change");
    if (showMe) {
      div.classList.remove("collapse");
      changeDiv.classList.add("collapse");
      subB.classList.add("collapse");
    }
    else {
      div.classList.add("collapse");
      changeDiv.classList.remove("collapse");
      subB.classList.remove("collapse");
    }
  }


}