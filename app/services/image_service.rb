require 'rubygems'
require 'aws-sdk'
require 'zip'

class ImageService

  def buildS3
    Aws::S3::Resource.new
  end

  def buildClient
    Aws::S3::Client.new
  end
 
  def bucket_name
    "hc-timesheets-demo"
  end

  def get_bucket
    s3 = buildS3
    return s3.bucket(bucket_name)
  end

  def set_image(img, object, param) 
    client = buildClient
    content_type = Agency.get_mime(img)
    key = Agency.get_key(img)
    resp = client.put_object({bucket: bucket_name, body:img, content_type: content_type, key: key})
    object[param] = key
  end

  def get_image(filename)
    bucket = get_bucket
    imgObj = bucket.object(filename)
    image = imgObj.get({})
    content_type = image.content_type
    uri = imgObj.presigned_url(:get, expires_in:3600)
    temp = scale_image(uri, content_type)
    return [temp, content_type]
  end

  def scale_image(image, content_type)
    type = content_type.split("/")[1]
    stringIo = open(image)
    temp = Tempfile.new(["image",".#{type}"])
    temp2 = Tempfile.new(["image",".#{type}"])
    temp2.binmode
    temp2.write stringIo.read
    temp2.close
    system("convert -resize '420x420\>' #{temp2.path} #{temp.path}")
    return temp.path
  end

  def rotate_image(image, type)
    stringIo = open(image)
    temp = Tempfile.new(["image",".#{type}"])
    temp2 = Tempfile.new(["image",".#{type}"])
    temp2.binmode
    temp2.write stringIo.read
    temp2.close
    system("convert -resize '420x420\>' -rotate '90<' #{temp2.path} #{temp.path}")
    return temp.path
  end

  def get_file(filename)
    s3 = buildS3
    bucket = s3.bucket("hc-timesheets-demo")
    fileObj = bucket.object(filename)
    file = fileObj.get({})
    content_type = file.content_type
    uri = fileObj.presigned_url(:get, expires_in:3600)
    stringIo = open(uri)
    ext = "#{content_type.split('/')[1]}"
    if ext == "plain"
      ext = "csv"
    end
    temp = Tempfile.new(["file",".#{ext}"])
    temp.binmode
    temp.write stringIo.read
    temp.close
    return [temp.path, content_type]
  end


  def get_zip(timesheets, types)
    resp = nil
    if timesheets.is_a?(Array) && types.is_a?(Integer)
      files = []
      file_type = ""
      puts types
      timesheets.each do |ts|
        timesheet = Timesheet.find_by(id:ts)
        if timesheet.present?
          case types
            when 1
              csv = timesheet.get_csv
              if csv.present?
                files << csv
              end
              file_type = "CSV"
            when 2
              pdf = timesheet.get_pdf
              if pdf.present?
                files << pdf
              end
              file_type = "PDF"
            else
              pdf = timesheet.get_pdf
              if pdf.present?
                files << pdf
              end
              csv = timesheet.get_csv
              if csv.present?
                files << csv
              end
              file_type = "All"
          end
        end
      end
      if files.present?
        dates = files.map{|f| f[1]}.join("").gsub(/[^\d,\.]/, "").split(".").sort
        puts(dates)
        if dates.length > 1 && types > 0
          date_range = "#{dates[0]}-#{dates[dates.length-1]}"
        elsif dates.length == 1 || types == 0
          date_range = dates[0]
        else
          date_range = ""
        end
        filename = "/tmp/TimeEntry-#{file_type}-#{date_range}.zip"
        Zip.setup do |c|
          c.on_exists_proc = true
          c.continue_on_exists_proc = true
        end
        Zip::File.open(filename, Zip::File::CREATE) do |zipfile|
          files.each do |file|
            zipfile.add(file[1],file[0])
          end
        end
        resp = filename
      end
    end
    return resp
  end


end
