require 'fcm'
class FirebaseService

  def build_fcm
    FCM.new(Rails.application.secrets.firebase_api_key)
  end

  def send_pca_notification(f_ids, request)
    return false unless f_ids.present?
    fcm = build_fcm
    title = "HCTS On Demand"
    message = "New Request Available"
    notification = {title:title, body:message}
    data = {"push_type":1, "request_id":request.id}
    options = {"notification":notification, "data":data}
    response = fcm.send(f_ids, options)
    deactivate_fails(response)
    resp_body = JSON.parse(response[:body])
    return resp_body["success"]
  end

  def send_direct_notification(f_ids, request)
    return false unless f_ids.present?
    fcm = build_fcm
    title = "HCTS On Demand"
    message = "New Direct Request Available"
    notification = {title:title, body:message}
    data = {"push_type":5, "request_id":request.id}
    options = {"notification":notification, "data":data}
    response = fcm.send(f_ids, options)
    deactivate_fails(response)
    resp_body = JSON.parse(response[:body])
    return resp_body["success"]
  end

  def send_pca_accepted(f_ids, request, pca)
    return false unless f_ids.present?
    fcm = build_fcm
    title = "HCTS On Demand"
    message = "Worker #{pca.name} has accepted your listing"
    notification = {title:title, body:message}
    data = {"push_type":2, "request_id":request.id}
    options = {"notification":notification, "data":data}
    response = fcm.send(f_ids, options)
    deactivate_fails(response)
    resp_body = JSON.parse(response[:body])
    return resp_body["success"]
  end

  def send_recipient_cancelled(f_ids, request)
    return false unless f_ids.present?
    recip = request.recipient
    fcm = build_fcm
    title = "HCTS On Demand"
    message = "Recipient #{recip.name} has cancelled the listing"
    notification = {title:title, body:message}
    data = {"push_type":3, "request_id":request.id}
    options = {"notification":notification, "data":data}
    response = fcm.send(f_ids, options)
     deactivate_fails(response)
    resp_body = JSON.parse(response[:body])
    return resp_body["success"]
  end

  def send_pca_cancelled(f_ids, request, pca)
    return false unless f_ids.present?
    fcm = build_fcm
    title = "HCTS On Demand"
    message = "Worker #{pca.name} has cancelled"
    notification = {title:title, body:message}
    data = {"push_type":4, "request_id":request.id}
    options = {"notification":notification, "data":data}
    response = fcm.send(f_ids, options)
    deactivate_fails(response)
    resp_body = JSON.parse(response[:body])
    return resp_body["success"]
  end

  def send_pca_declined(f_ids, request, pca)
    return false unless f_ids.present?
    fcm = build_fcm
    title = "HCTS On Demand"
    message = "Worker #{pca.name} declined your direct request"
    notification = {title:title, body:message}
    data = {"push_type":4, "request_id":request.id}
    options = {"notification":notification, "data":data}
    response = fcm.send(f_ids, options)
    deactivate_fails(response)
    resp_body = JSON.parse(response[:body])
    return resp_body["success"]
  end



  def deactivate_fails(resp)
    if resp[:not_registered_ids].present?
      resp[:not_registered_ids].each do |d_id|
        dev = Device.find_by(deviceToken:d_id)
        if dev.present?
          dev.update(isActive:false)
        end
      end
    end
  end 
      

end