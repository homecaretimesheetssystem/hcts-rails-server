require 'stripe'

class StripeService

 def get_customer(id,name="")
   if id.nil?
     cust = Stripe::Customer.create(description:name)
   else
     cust = Stripe::Customer.retrieve(id) 
   end
   return cust
 end 
 

 def get_subscriptions(customer)
   customer.subscriptions.data
 end

 def cancel_subscription(subscription_id)
   sub = Stripe::Subscription.retrieve(subscription_id)
   if sub.delete
     return true
   else
     return false
   end
 end

 def create_subscription(cust, plan_name)
   plans = get_plans
   plan_id = nil
   if !plans.empty?
     plans.each do |plan|
       if plan.name == plan_name
         plan_id = plan.id
       end
     end
   end
   if plan_id.present?
     Stripe::Subscription.create(customer:cust.id, items:[{plan:plan_id}])
   else
     nil
   end
 end

 def get_plans
   plans = Stripe::Plan.list.data
   plans.reverse!
   return plans
 end

 def get_plan_names
   plans = get_plans
   names = []
   plans.each do |plan|
     names << plan.name
   end
   return names 
 end

 def add_card(cust, token) 
   resp = cust.sources.create({source: token})
   if resp.id.present?
     return resp.id
   else
     return false
   end
 end

 def remove_card(cust, id) 
   resp = cust.sources.retrieve(id).delete
   if resp.present? && resp.deleted.present?
     return true
   else
     return false
   end
 end

end