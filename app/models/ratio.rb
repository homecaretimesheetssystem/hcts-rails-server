class Ratio < ApplicationRecord
 self.table_name = "ratio"
 has_many :timesheets
 has_many :session_data

 after_find :decrypt_attrs
 before_save :encrypt_attrs
 
 def name
   self.ratio
 end

private

 def decrypt_attrs
   self.ratio = Ratio.uncrypt(self.ratio)
 end

 def encrypt_attrs
   self.ratio = Ratio.encrypt(self.ratio)
 end

end
