class TimeIn < ApplicationRecord
  self.table_name = "time_in"
  has_one :session_datum, foreign_key: "timeIn_id"

  after_find :decrypt_attrs
  before_save :encrypt_attrs

  scope :by_agency, ->(agency_id) { joins(:session_datum => [:timesheets => [:recipient]]).where("recipient.agency_id = ?", agency_id) }



private
  def decrypt_attrs
    self.timeInAddress = TimeIn.uncrypt(self.timeInAddress)
    self.latitude = TimeIn.uncrypt(self.latitude)
    self.longitude = TimeIn.uncrypt(self.longitude)
  end

  def encrypt_attrs
    self.timeInAddress = TimeIn.encrypt(self.timeInAddress)
    self.latitude = TimeIn.encrypt(self.latitude)
    self.longitude = TimeIn.encrypt(self.longitude)
  end

end
