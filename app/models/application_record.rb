class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  require 'mcrypt'
  require 'digest/md5'
  require 'filemagic'


  def self.graphColors
   [["rgba(64,167,115,1)", "rgba(64,167,115,0.5)"], ["rgba(148,232,189,1)", "rgba(148,232,189,0.5)"], ["rgba(212,232,88,1)", "rgba(212,232,88,0.5)"], ["rgba(149,198,227,1)", "rgba(149,198,227,0.5)"], ["rgba(51,73,86,1)", "rgba(51,73,86,0.5)"], ["rgba(77,83,165,1)", "rgba(77,83,165,0.5)"], ["rgba(159,164,231,1)", "rgba(159,164,231,0.5)"], ["rgba(239,172,91,1)", "rgba(239,172,91,0.5)"], ["rgba(239,126,91,1)", "rgba(239,126,91,0.5)"], ["rgba(135,109,78,1)", "rgba(135,109,78,0.5)"], ["rgba(239,199,91,1)", "rgba(239,199,91,0.5)"], ["rgba(255,230,163,1)", "rgba(255,230,163,0.5)"]] 
   # [["rgba(220, 20, 20, 1)","rgba(220, 120, 120, 0.5)"],["rgb(255, 99, 132, 1)","rgb(255, 99, 132, 0.5)"],["rgb(255, 159, 64, 1)","rgb(255, 159, 64, 0.5)"],["rgb(255, 205, 86, 1)","rgb(255, 205, 86, 0.5)"],["rgb(75, 192, 192, 1)","rgb(75, 192, 192, 0.5)"],["rgb(54, 162, 235, 1)","rgb(54, 162, 235, 0.5)"],["rgb(153, 102, 255, 1)","rgb(153, 102, 255, 0.5)"],["rgb(231,133,137, 1)","rgb(231,133,137, 0.5)"]]
  end
  
  def self.graphOptions
                    {scales: {
                            xAxes: [{
                                       type: 'time',
                                       time: {
                                                  tooltipFormat: 'lll',
                                                  minUnit: 'hour',
                                                  displayFormats: {
                                                           second: "MMM D",
                                                           minute: "MMM D",
                                                           hour: "MMM D",
                                                           day: 'MMM D',
                                                           month: 'MMM YYYY',
                                                           quarter: 'MMM YYYY'
                                                  }
                                       }                  
                            }],
                            yAxes: [{
                                      ticks: {
                                                min:0,
                                                max:100,
                                                callback: "function(value) {return value + '%'}"
                                      },
                                      scaleLabel: {
                                                display: true,
                                                labelString: "Percentage"
                                      }
                            }]
                    },
                    tooltips: {
                            enabled: true,
                            mode: 'single',
                            callbacks: {
                                      label: "function(tooltipItems, data) { return tooltipItems.yLabel + '%'; }"
                    
                            }
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    height:400,
                    id: "goal_chart"
                   }
  end

  def self.barOptions
     { scales: {
                yAxes: [{
                         ticks: {
                                beginAtZero:true
                         }
                }]
       },
       responsive: true,
       maintainAspectRatio: false,
       height: "400",
       id: "care_chart"
    }
  end

  def amazonUrl
    "https:\/\/s3.amazonaws.com\/hc-timesheets-demo\/"
  end

  def self.genders
    [['Female',1],['Male',2]]
  end

  def self.gender_preferences
   [['None',0],['Female',1],['Male',2]]
  end

  def self.get_image(string)
   "https://s3.amazonaws.com/hc-timesheets-demo/#{string}"
  end

  def self.data_key
    Digest::MD5.hexdigest("ABCDEFGHIJKLMNOPQRSTUVWXZY123456")
  end
 
  def self.get_cipher
    Mcrypt.new(:rijndael_256, :ecb, self.data_key, nil, :zeroes)
  end

  def self.get_decrypt_cipher
    Mcrypt.new(:rijndael_256, :ecb, self.data_key, nil, :zeroes)
  end
 
  def self.get_encrypt_cipher
    Mcrypt.new(:rijndael_256, :ecb, self.data_key, nil, :zeroes)
  end

  def self.get_all(thing)
    self.all.map{|x| x.send(thing)}
  end

  def self.uncrypt(thing)
    if thing.present? && thing.include?("<ENC>")
      info = thing.gsub("<ENC>","")
      cipher = self.get_cipher
      data = Base64.strict_decode64(info)
      decrypted_string = cipher.decrypt(data) rescue thing
      d_string = decrypted_string.strip
      if d_string.present?
        d_string = d_string.encode('UTF-8', :invalid => :replace, :undef => :replace, :replace => " ")
      end
      return d_string
    else
      return thing
    end
  end

  def pad_me(thing)
    if thing.size % 32 != 0
      in_data = thing + (" " * (32 - (thing.size % 32)))
    else
      in_data = thing
    end
    return in_data
  end

  def self.encrypt(thing)
    if thing.present? && !thing.include?("<ENC>")
      data = thing.gsub('–', '-')
      data = data.encode('UTF-8', :invalid => :replace, :undef => :replace, :replace => "")
      cipher = self.get_cipher
      out_data = cipher.encrypt(data)
      encrypted_string = Base64.strict_encode64(out_data)
      return encrypted_string + "<ENC>"
    else
      return thing
    end
  end

  def get_token(length = 86)
    chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789'
    token = ''
    length.times { token << chars[rand(chars.size)] }
    return token
  end

  def self.get_string(length = 8)
    chars = 'abcdefghjkmnpqrstuvwxyz0123456789'
    string = ''
    length.times { string << chars[rand(chars.size)] }
    return string
  end

  def self.get_ext(file)
    mime = self.get_mime(file)
    ext = mime.split("/")[1]
    ext = ext == "jpeg" ? "jpg" : ext
    return ext
  end

  def self.get_mime(file)
    fm = FileMagic.open(:mime)
    return fm.file(file.path, true)
  end

  def self.get_key(file)
    "#{self.get_string}#{Time.now.to_i}.#{self.get_ext(file)}"
  end

  def self.api_format(time)
    time.strftime("%Y-%m-%dT%H:%M:%S%z")
  end
  
  def self.api_format2(time)
    time.strftime("%Y-%m-%dT%H:%M:%S%z")
  end

  def time_zone
   "Central Time (US & Canada)"
  end

  def self.time_zone
   "Central Time (US & Canada)"
  end
end
