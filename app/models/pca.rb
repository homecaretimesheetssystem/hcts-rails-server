class Pca < ApplicationRecord
  self.table_name = "pca"
  belongs_to :user
  belongs_to :agency
  has_many :pca_counties
  has_many :counties, through: :pca_counties
  has_many :pca_qualifications
  has_many :qualifications, through: :pca_qualifications, foreign_key: "qualifications_id"
  has_many :pca_services
  has_many :services, through: :pca_services
  has_many :session_data
  has_many :timesheets, through: :session_data
  has_many :pca_signatures, through: :timesheets
  has_many :recipient_pcas
  has_many :rsvps
  has_many :requests, through: :rsvps
  has_many :recipients, through: :recipient_pcas
  accepts_nested_attributes_for :user
  has_many :direct_requests, class_name: "Request", foreign_key: "requested_id"
  attr_accessor :photo

  before_save :set_photo

  scope :active, -> { where(available:true) }
  scope :nonarchived, -> { joins(:user).where('fos_user.archived = ?', false) }
  scope :archived, -> { joins(:user).where('fos_user.archived = ?', true) }
  scope :from_archived_ts, -> { joins(:timesheets).where("timesheet.archived = ?", true) }
  scope :has_ondemand, -> { joins(:services).where("services.serviceNumber = 11") }
  scope :is_available, -> { has_ondemand.where(available:true) }

  after_find :decrypt_attrs

  before_save :encrypt_attrs
  after_save :check_user_name

  def first_name
    self.firstName
  end

  def last_name
    self.lastName
  end

  def get_direct_requests
    self.direct_requests.direct_available
  end

  def available_requests
    requests = self.agency.requests.available_api(self)
    avail_requests = []
    q_ids = self.qualification_ids
    requests.each do |req|
      if req.qualification_ids - q_ids == []
        avail_requests << req
      end
    end
#    directRequests = self.direct_requests.in_future
#    if directRequests.any?
#      directRequests.each do |req|
#        avail_requests << req
#      end
#    end
    return avail_requests
  end

  def set_user
    self.user.firstName= self.firstName
    self.user.lastName = self.lastName
    self.pca_name = "#{self.firstName} #{self.lastName}"
    self.user.roles = User::PCA_ROLE
  end

  def name 
    "#{self.firstName} #{self.lastName}".titleize
  end

  def homeAddress
    self.home_address
  end

  def self.get_users
    User.nonarchived.where(id:self.pluck(:user_id))
  end 

  def self.get_archived_users
    User.archived.where(id:self.pluck(:user_id))
  end

  def get_firebase_tokens
    if self.available
      self.user.devices.active.pluck(:deviceToken)
    else
      []
    end
  end

  def self.get_firebase_tokens(agency)
    Device.active.joins(:user => :pca).where("pca.agency_id = ?", agency.id).where("pca.id in (?)", self.pluck(:id)).where("pca.available = ?", true).pluck(:deviceToken)
  end

  def self.send_listing_notifications(request)
    successes = 0
    firebase_ids = self.get_firebase_tokens(request.recipient.agency)
    if !firebase_ids.empty?
      firebase = FirebaseService.new
      successes = firebase.send_pca_notification(firebase_ids, request.id)
    end
    return successes
  end

  def send_cancelled_notification(request)
    successes = 0
    firebase_ids = self.get_firebase_tokens(request.recipient.agency)
    if !firebase_ids.empty?
      firebase = FirebaseService.new
      successes = firebase.send_recipient_cancelled(firebase_ids, request)
    end
    return successes
  end

  def setNumber(number)
    number = number.gsub(/\D/, '')
    if number.length == 7
      number = "#{number[0..2]}-#{number[3..6]}"
    elsif number.length == 10
      number = "(#{number[0..2]})#{number[3..5]}-#{number[6..9]}"
    end
    return number
  end


private

  def decrypt_attrs
   if self.pca_name.present?
     self.pca_name = Pca.uncrypt(self.pca_name)
   end
   if self.firstName.present?
     self.firstName = Pca.uncrypt(self.firstName)
   end
   if self.lastName.present?
     self.lastName = Pca.uncrypt(self.lastName)
   end
   if self.umpi.present?
     self.umpi = Pca.uncrypt(self.umpi)
   end
   if self.company_assigned_id.present?
     self.company_assigned_id = Pca.uncrypt(self.company_assigned_id)
   end
   if self.phoneNumber.present?
    self.phoneNumber = self.setNumber(Pca.uncrypt(self.phoneNumber))
   end
   if self.home_address.present?
     self.home_address = Pca.uncrypt(self.home_address)
   end
  end

  def check_user_name
    if self.firstName != self.user.firstName
      self.user.firstName = Pca.uncrypt(self.firstName)
    end
    if self.lastName != self.user.lastName
      self.user.lastName = Pca.uncrypt(self.lastName)
    end
    self.user.save
  end

  def encrypt_attrs
    setname = self.firstName_changed? || self.lastName_changed?
    if setname
      self.pca_name = "#{self.firstName} #{self.lastName}"
    end
    self.pca_name = Pca.encrypt(self.pca_name) 
    self.firstName = Pca.encrypt(self.firstName)
    self.lastName = Pca.encrypt(self.lastName)
    self.umpi = Pca.encrypt(self.umpi)
    self.company_assigned_id = Pca.encrypt(self.company_assigned_id)
    self.phoneNumber = Pca.encrypt(self.phoneNumber)
    self.home_address = Pca.encrypt(self.home_address)\
  end

  def set_photo
    resp = false
    if self.photo.present?
      resp = ImageService.new.set_image(self.photo, self, "profile_photo")
    end
    return resp
  end

end