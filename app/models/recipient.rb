class Recipient < ApplicationRecord
  self.table_name = "recipient"
  belongs_to :user, dependent: :destroy
  belongs_to :agency
  belongs_to :county
  has_many :recipient_pcas, dependent: :destroy
  has_many :pcas, through: :recipient_pcas
  has_many :timesheets
  has_many :session_data, through: :timesheets
  has_many :recipient_signatures, through: :timesheets
  has_many :requests, dependent: :destroy
  has_many :careoption_timesheets, through: :timesheets
  has_many :careoptions, through: :timesheets
  has_many :care_goals, dependent: :destroy
  has_many :goal_care_options, class_name: "Careoption", source: :goal_care_options, through: :care_goals
  has_many :recipient_services, dependent: :destroy
  has_many :services, through: :recipient_services
  accepts_nested_attributes_for :user

  scope :needs_goals, -> { where("id NOT IN (Select distinct(recipient_id) from care_goals)") }
  scope :has_goals, -> { where("id IN (Select distinct(recipient_id) from care_goals)") }
  scope :nonarchived, -> { joins(:user).where('fos_user.archived = ?', false) }
  scope :archived, -> { joins(:user).where('fos_user.archived = ?', true) }
  scope :from_archived_ts, -> { joins(:timesheets).where('timesheet.archived = ?', true) }

  after_find :decrypt_attrs

  before_save :encrypt_attrs
  after_save :check_user_name

  def name
    self.recipientName.titleize
  end

  def recipientName
    "#{self.first_name} #{self.last_name}"
  end

  def firstName
    self.first_name
  end

  def lastName
    self.last_name
  end

  def mANumber
    return Recipient.uncrypt(self.ma_number)
  end

  def companyAssignedId
    return Recipient.uncrypt(self.company_assigned_id)
  end

  def self.get_users
    User.nonarchived.where(id:self.pluck(:user_id))
  end

  def self.get_archived_users
    User.archived.where(id:self.pluck(:user_id))
  end

  def set_user
    self.user.firstName = self.first_name
    self.user.lastName = self.last_name
    self.recipient_name = "#{self.first_name} #{self.last_name}"
    self.user.roles = User::RECIP_ROLE
  end

  def gender_prefer
    case self.gender_preference
      when 1
        return "Female"
      when 2
        return "Male"
      else
        return "None"
    end
  end

  def self.get_frequency(array)
    array.group_by{ |v| v }.map{ |k, v| [k, v.size] }
    # array.each_with_object(Hash.new(0)){|key,hash| hash[key] += 1}
  end

  def compare_care_goals(timesheets = [])
    goals = self.goal_care_options
    resp = []
    if goals.present?
      goals_count = goals.count
      if timesheets.is_a? Timesheet
        timesheets = [timesheets]
      elsif !timesheets.present? 
        timesheets = self.timesheets.finished.active
      end
      tsheets = []
      timesheets.each do |ts|
        if ts.finished
          tsheets << ts
        end
      end
      timesheets = tsheets
      if timesheets.count > 10 
        timesheets = timesheets.first(10)
      end
      timesheets.sort_by{|x| x.session_datum.time_in.timeIn}
      timesheets.each do |ts|
        if ts.finished?
          careOps = ts.careoptions
          remainingCareOps = []
          if careOps.present?
            remainingCareOps = goals - careOps
            g_diff = remainingCareOps.count
          else
            g_diff = goals_count
          end
          perc = ((1 - g_diff/goals_count.to_f) * 100).round(2)
          resp << [ts, remainingCareOps, perc]
        end
      end
    end
    return resp
  end

  def graph_care_goals(timesheet_ids = [])
    goals = self.goal_care_options
    resp = {}
    colors = Recipient.graphColors.shuffle
    if goals.present?
      dataset = []
      dates = []
      goals_count = goals.count
      if timesheet_ids.is_a? Integer
        timesheet_ids = [timesheet_ids]
      elsif !timesheet_ids.present? 
        timesheet_ids = self.timesheets.graphable.pluck(:id)
      end
      tsheets = Timesheet.where(id:timesheet_ids).finished
      timesheets = tsheets.sort_by{|x| x.session_datum.time_in.timeIn}
      timesheets.each do |ts|
        date = "";
        if ts.recipient == self && ts.session_datum.present? && ts.finished
          session_data = ts.session_datum
          if session_data.time_in.present? && session_data.time_in.timeIn.present?
            stamp = session_data.time_in.timeIn.in_time_zone("Central Time (US & Canada)").to_time.to_i * 1000
            careOps = ts.careoptions
            remainingCareOps = []
            if careOps.present?
              remainingCareOps = goals - careOps
              g_diff = remainingCareOps.count
            else
              g_diff = goals_count
            end
            perc = ((1 - g_diff/goals_count.to_f) * 100).round(2)
            dataset << {x:stamp,y:perc}
          end
        end
      end
      resp[:data] = dataset
      color = colors.shift
      resp[:borderColor] = color[1]
      resp[:fill] = false
      resp[:backgroundColor] = color[0]
      resp[:label] = self.name
      resp[:lineTension] = 0
    end
    return resp
  end


  def self.graph_care_goals(timesheet_ids = [])
    resp = []
    if timesheet_ids.is_a? Integer
      timesheet_ids = [timesheet_ids]
    elsif !timesheet_ids.present? 
      return resp
    end
    colors = Recipient.graphColors.shuffle
    tsheets = Timesheet.where(id:timesheet_ids).active.finished
    timesheets = tsheets.sort_by{|x| x.session_datum.time_in.timeIn}
    if timesheets.count > 10 
      timesheets = timesheets.first(10)
    end
    recipients = Recipient.joins(:timesheets).where("timesheet.id in (?)",timesheet_ids).distinct
    recipients.each do |rec|
      datas = {}
      dataset = []
      dates = []
      goals = rec.goal_care_options
      goals_count = goals.count
      if goals_count > 0
        timesheets.each do |ts|
          date = "";
          if ts.recipient == rec && ts.session_datum.present? && ts.finished
            session_data = ts.session_datum
            if session_data.time_in.present? && session_data.time_in.timeIn.present?
              stamp = session_data.time_in.timeIn.in_time_zone("Central Time (US & Canada)").to_time.to_i * 1000
              careOps = ts.careoptions
              remainingCareOps = []
              if careOps.present?
                remainingCareOps = goals - careOps
                g_diff = remainingCareOps.count
              else
                g_diff = goals_count
              end
              perc = ((1 - g_diff/goals_count.to_f) * 100).round(2)
              dataset << {x:stamp,y:perc}
            end
          end
        end
        datas[:data] = dataset
        color = colors.shift
        datas[:borderColor] = color[1]
        datas[:fill] = false
        datas[:backgroundColor] = color[0]
        datas[:label] = rec.name
        datas[:lineTension] = 0
        resp << datas
      end
    end
    return resp
  end
  

  def bar_cares(timesheet_ids = [])
    resp = {}
    if timesheet_ids.is_a? Integer
      timesheet_ids = [timesheet_ids]
    elsif !timesheet_ids.present?
      timesheet_ids = self.timesheets.finished.pluck(:id)
    end
    colors = Recipient.graphColors.shuffle
    if timesheet_ids.count > 10 
      timesheet_ids = timesheet_ids.first(10)
    end
    careoptions = Recipient.get_care_options(timesheet_ids)
    datasets = []
    timesheet_ids.each do |t_id|
      ts = Timesheet.find_by(id:t_id)
      if ts.present? && ts.finished?
        dataset = {}
        careOps = Recipient.get_care_options(t_id)
        data = []
        careoptions.each do |c|
          if careOps.include?(c)
            data << 1
          else
            data << 0
          end
        end
        dataset[:data] = data
        dataset[:label] = "#{ts.pca.name} - #{Timesheet.get_date_range(ts)}"
        color = colors.shift
        dataset[:backgroundColor] = Array.new(data.length, color[0])
        dataset[:borderColor] = Array.new(data.length, color[1])
        dataset[:borderWidth] = 1
        datasets << dataset    
      end
    end
    goals = self.get_goal_care_options
    resp[:labels] = careoptions.map{|x| goals.include?(x) ? "#{x} (CG)" : x }
    resp[:datasets] = datasets
    return resp
  end

  def self.bar_cares(timesheet_ids = [])
    resp = {}
    if timesheet_ids.is_a? Integer
      timesheet_ids = [timesheet_ids]
    elsif !timesheet_ids.present?
      return resp
    end
    if timesheet_ids.count > 10
      timesheet_ids = timesheet_ids.first(10)
    end
    colors = Recipient.graphColors.shuffle
    careoptions = Recipient.get_care_options(timesheet_ids)
    datasets = []
    timesheet_ids.each do |t_id|
      ts = Timesheet.find_by(id:t_id)
      if ts.present? && ts.finished
        dataset = {}
        careOps = Recipient.get_care_options(t_id)
        data = []
        careoptions.each do |c|
          if careOps.include?(c)
            data << 1
          else
            data << 0
          end
        end
        dataset[:data] = data
        dataset[:label] = "#{ts.recipient.name}-#{ts.pca.name} #{Timesheet.get_date_range(ts)}"
        color = colors.shift
        dataset[:backgroundColor] = Array.new(data.length, color[0])
        dataset[:borderColor] = Array.new(data.length, color[1])
        dataset[:borderWidth] = 1
        datasets << dataset    
      end
    end
    resp[:labels] = careoptions
    resp[:datasets] = datasets
    return resp
  end


  def get_goal_care_options
    self.goal_care_options.map{|x| x.name}
  end


  def self.get_care_options(timesheet_ids)
    Careoption.joins(:timesheets).where("timesheet.id in (?)",timesheet_ids).distinct.map{|x| x.name}
  end


  def self.get_range_name(datasets)
    resp = "month"
    dates = []
    datasets[:datasets].each do |d|
      data = d[:data]
      dates << data[0][:x]/100
      dates << data[data.size-1][:x]/100
    end
    dates.sort!
    range = dates.last - dates.first
    if range > 0 
      num_days = range/84600
      if num_days < 2
        resp = "hour"
      elsif num_days < 30
        resp = "day"
      end
    else 
      resp = "hour"
    end
    return resp
  end


  def self.compare_care_goals(timesheets = [])
    resp = []
    if timesheets.is_a? Timesheet
      timesheets = [timesheets]
    end
    timesheets.each do |ts|
      if ts.finished?
        goals = ts.recipient.goal_care_options
        if goals.present?
          goals_count = goals.count
          careOps = ts.careoptions
          remainingCareOps = []
          if careOps.present?
            remainingCareOps = goals - careOps
            g_diff = remainingCareOps.count
          else
            g_diff = goals_count
          end
          perc = ((1 - g_diff/goals_count.to_f) * 100).round(2)
          resp << [ts, remainingCareOps, perc]
        end
      end
    end
    return resp
  end

  def self.get_goals_average(array)
    resp = 100
    if array.is_a?(Array) && !array.empty?
      a = array.transpose[2]
      resp = (a.reduce(:+) /a.size).to_f
    end
    return resp
  end

  def get_goals_average
    timesheets = self.timesheets.finished.active
    arr = Recipient.compare_care_goals(timesheets)
    return Recipient.get_goals_average(arr).round(2)
  end

  def count_careoptions(timesheets = [])
    resp = []
    if timesheets.present? && timesheets.is_a?(Array) 
      timesheets.each do |ts|
        careOps << ts.careoptions
      end
      careOps.flatten!
    elsif timesheets.is_a? Timesheet
      careOps = timesheets.careoptions
    else
      careOps = self.careoptions.distinct
    end 
    if !careOps.empty? 
      resp = Recipient.get_frequency(careOps)
    end
    return resp
  end


  def send_pca_accepted_notification(request, pca)
    successes = 0
    firebase_ids = self.get_firebase_tokens
    if firebase_ids.any?
      firebase = FirebaseService.new
      successes = firebase.send_pca_accepted(firebase_ids, request, pca)
    end
    return successes
  end

  def get_firebase_tokens
    self.user.devices.active.pluck(:deviceToken)
  end

  def send_pca_cancelled_notification(request, pca)
    successes = 0
    firebase_ids = self.get_firebase_tokens
    if firebase_ids.any?
      firebase = FirebaseService.new
      successes = firebase.send_pca_cancelled(firebase_ids, request, pca)
    end
    return successes
  end

  def resetnames
    self.first_name = self.user.firstName
    self.last_name = self.user.lastName
    self.recipient_name = self.user.name
  end

  private 
 
  def decrypt_attrs
    self.recipient_name = Recipient.uncrypt(self.recipient_name).titleize
    self.first_name = Recipient.uncrypt(self.first_name).titleize
    self.last_name = Recipient.uncrypt(self.last_name).titleize
    self.ma_number = Recipient.uncrypt(self.ma_number)
    self.company_assigned_id = Recipient.uncrypt(self.company_assigned_id)
  end

  def check_user_name
    if self.first_name != self.user.firstName
      self.user.firstName = Recipient.uncrypt(self.first_name)
    end
    if self.last_name != self.user.lastName
      self.user.lastName = Recipient.uncrypt(self.last_name)
    end
    self.user.save
  end

  def encrypt_attrs
    if self.first_name_changed? || self.last_name_changed?
      self.recipient_name = "#{self.first_name} #{self.last_name}"
    end
    self.recipient_name = Recipient.encrypt(self.recipient_name)
    self.first_name = Recipient.encrypt(self.first_name)
    self.last_name = Recipient.encrypt(self.last_name)
    self.ma_number = Recipient.encrypt(self.ma_number)
    self.company_assigned_id = Recipient.encrypt(self.company_assigned_id)
  end

end
