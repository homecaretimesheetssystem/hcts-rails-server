class Service < ApplicationRecord
  has_many :pca_services
  has_many :pcas, through: :pca_services
  has_many :session_data
  has_many :recipient_services
  has_many :recipients, through: :recipient_services

  scope :nondemand, -> { where.not(serviceNumber:11) }

  after_find :decrypt_attrs
  before_save :encrypt_attrs

  def service
    self.serviceName
  end

  def name
    self.serviceName.titleize
  end
  
  def short_name
    self.name.gsub("Service", "")
  end

private

  def decrypt_attrs
    self.serviceName = Service.uncrypt(self.serviceName)
  end
 
  def encrypt_attrs
    self.serviceName = Service.encrypt(self.serviceName)
  end
end
