class Verification < ApplicationRecord
  self.table_name = "verification"
  belongs_to :recipient_pca, foreign_key:"recipientPca_id"
  has_one :recipient, through: :recipient_pca
  has_one :pca, through: :recipient_pca
  
  scope :viewable, -> { joins(:recipient_pca => [:recipient, :pca]).where.not(time:nil).where("verification_date < ?", Time.now.beginning_of_day.in_time_zone("Central Time (US & Canada)")) }

  after_find :decrypt_attrs
  before_save :encrypt_attrs

  def get_image
    resp = nil
    if self.verification_photo.present?
      imgs = ImageService.new
      verif_image = imgs.get_image(self.verification_photo)
      type_array = verif_image[1].split("/");
      name = self.pca.name.gsub(".","")
      name = name.gsub("'","")
      file_name = URI.encode(name)
      filename = "#{file_name}.#{type_array[1]}"
      resp = [verif_image[0], filename, verif_image[1]]
    end
    return resp
  end


  def add_image(image)
    resp = false
    if image.present?
      resp = ImageService.new.set_image(image, self, "verification_photo")
      if resp.present?
        self.verification_photo = resp
        self.save
      end
    end
    return resp
  end


private
  def decrypt_attrs
    self.address = Verification.uncrypt(self.address)
    self.latitude = Verification.uncrypt(self.latitude)
    self.longitude = Verification.uncrypt(self.longitude)
  end

  def encrypt_attrs
    self.address = Verification.encrypt(self.address)
    self.latitude = Verification.encrypt(self.latitude)
    self.longitude = Verification.encrypt(self.longitude)
  end


end
