class Agency < ApplicationRecord
 self.table_name = "agency"
 has_many :agency_services
 has_many :services, through: :agency_services
 has_many :email_settings
 has_many :users
 has_many :recipients
 has_many :requests, through: :recipients
 has_many :timesheets, through: :recipients
 has_many :care_goals, through: :recipients
 has_many :pcas
 has_many :admins, through: :users
 belongs_to :state
 accepts_nested_attributes_for :users
 attr_accessor :accept_terms, :image

 after_find :decrypt_attrs
 before_save :set_image, :encrypt_attrs

 START_TIMES = ["12:00AM", "01:00AM", "02:00AM", "03:00AM", "04:00AM", "05:00AM", "06:00AM", "07:00AM", "08:00AM", "09:00AM", "10:00AM", "11:00AM", "12:00PM", "01:00PM", "02:00PM", "03:00PM", "04:00PM", "05:00PM", "06:00PM", "07:00PM", "08:00PM", "09:00PM", "10:00PM", "11:00PM"]

  def is_enabled
    self.get_subscriptions.present?
  end

  def card_id
    self.stripe_source
  end


  def customer_id
    self.stripe_customer_id
  end


  def get_source
    ss = StripeService.new
    resp = nil
    c_id = Agency.uncrypt(self.customer_id)
    if c_id.present? && c_id.include?("<ENC>")
      c_id = Agency.uncrypt(c_id)
    end
    if c_id.present?
      resp = ss.get_customer(c_id)
      if resp["description"].nil?
        resp["description"] = self.name
        resp.save
      end
    else
      cust = ss.get_customer(nil, self.agency_name)
      self.stripe_customer_id = cust.id
      if self.save
        resp = cust
      end
    end
    return resp
  end

  def get_plans
    ss = StripeService.new
    return ss.get_plans
  end

  def get_plan_names
    plans = self.get_plans
    plan_names = []
    plans.each do |plan|
      plan_names << plan.name
    end
    return plan_names
  end

  def get_subscriptions
    cust = self.get_source
    if cust.present?
      ss = StripeService.new
      return ss.get_subscriptions(cust)
    else
      return nil 
    end
  end
  
  def subscription_name
    if self.get_subscription_names.present?
      self.get_subscription_names.join(",")
    else
      "No Active Subscription"
    end
  end

  def card_number
    source = self.get_source
    if source != nil
      return source.sources.data.first.last4
    else
      return "----"
    end
  end

  def get_subscription_names
    subs = self.get_subscriptions
    if subs.present?
      plans = []
      subs.each do |sub|
        plans << sub.plan.name
      end
      return plans 
    else
      return nil
    end
  end

  def get_subscription_id(plan_name)
    subs = self.get_subscriptions
    if subs.present?
      sub_id = nil
      subs.each do |sub|
        if sub.plan.name == plan_name
          sub_id = sub.id
        end
      end
      return sub_id
    else
      return nil
    end
  end 

  def cancel_subscription(sub_id)
    ss = StripeService.new
    unsub = ss.cancel_subscription(sub_id)
    subs = self.get_subscriptions
    return unsub   
  end

  def create_subscription(plan_name)
    cust = self.get_source
    if !self.get_subscriptions.present? && cust.present?
      ss = StripeService.new
      resp = ss.create_subscription(cust, plan_name)
      return resp
    else
      return nil
    end
  end

  def replace_card(token)
    resp = false
    card_id = self.card_id
    cust = self.get_source
    ss = StripeService.new
    card_added = ss.add_card(cust,token)
    if card_added
      self.stripe_source = card_added
      if self.save
        resp = true
        if card_id.present?
          ss.remove_card(cust,card_id)
        end
      end
    end
    return resp
  end

  def set_source_subscription(sub,token)
    resp = false
    cust = self.get_source
    ss = StripeService.new
    new_card = ss.add_card(cust,token)
    if new_card 
      self.stripe_source = new_card
      if self.save 
        resp = ss.create_subscription(cust,sub)
      end
    end
    return resp
  end


  def assignments 
    RecipientPca.where(recipient_id:self.recipients.pluck(:id)).or(RecipientPca.where(pca_id: self.pcas.pluck(:id))).uniq.sort{|x,y| x.recipient.name <=> y.recipient.name}
  end




  def phone_number
    self.phoneNumber
  end

  def image_name
    self.imageName
  end

  def get_emails
    self.email_settings.pluck(:email)
  end

  def get_image
    resp = "default_profile_image.png"
    if self.imageName.present?
      imgs = ImageService.new
      agency_image = imgs.get_image(self.imageName)
      type_array = agency_image[1].split("/");
      name = self.agency_name.gsub(".","")
      name = name.gsub("'","")
      file_name = URI.encode(name)
      filename = "#{file_name}.#{type_array[1]}"
      resp = [agency_image[0], filename, agency_image[1]]
    end
    return resp
  end

  def get_image_url
    if self.imageName.present?
      "https://s3.amazonaws.com/hc-timesheets-demo/#{self.imageName}"
    else
      "default_profile_image.png"
    end
  end

  def get_users
    ids = self.recipients.pluck(:user_id) + self.pcas.pluck(:user_id) + self.users.pluck(:id)
    return User.where(id:ids)
  end

  def name 
    self.agency_name.force_encoding("UTF-8").encode("UTF-8").titleize
  end

  def unserialize_start_time
    if self.start_time.present? 
      startTime = PHP.unserialize(self.start_time)
      self.update(start_time: startTime.empty? ? "12:00AM" : "#{startTime['hour']}:#{startTime['minute']}#{startTime['period']}")
    end
  end

  def startTime
    self.start_time.present? ? Time.parse(self.start_time) : Time.parse(Agency::START_TIMES[0])
  end

  def compare_plan_goals
    recipients = self.recipients.joins(:care_goals).pluck(:id)
    timesheets = Timesheet.finished.active.joins(:recipient).where("timesheet.recipient_id in (?)",recipients)
    return Recipient.compare_care_goals(timesheets)
  end

  def graph_care_goals
    recipients = self.recipients.joins(:care_goals).pluck(:id)
    timesheets = Timesheet.finished.active.joins(:recipient).where("timesheet.recipient_id in (?)",recipients).pluck(:id)
    return Recipient.graph_care_goals(timesheets)
  end

  def bar_cares
    recipients = self.recipients.pluck(:id)
    timesheets = Timesheet.finished.active.joins(:recipient).where(recipient_id:recipients).pluck(:id)
    return Recipient.bar_cares(timesheets)
  end

  def goal_average(recipient = nil)
    if recipient.nil?
      Recipient.get_goals_average(self.compare_plan_goals)
    else
      Recipient.get_goals_average(recipient.compare_plan_goals)
    end
  end

  def deserialize_attrs
    if self.start_time.present?
      startTime = PHP.unserialize(self.start_time)
      puts startTime
      self.start_time = startTime.empty? ? "12:00AM" : "#{startTime['hour']}:#{startTime['minute']}#{startTime['period']}"
    else
      self.start_time = "12:00AM"
    end
  end

  def serialize_attrs
    if self.start_time.present?
      startTime = self.startTime
      hash = {'hour' => startTime.strftime("%I"), 'minute': startTime.strftime("%M"), 'period': startTime.strftime("%p")}
      self.start_time = PHP.serialize(hash)
    end
  end

  def get_start_time_hash
    startTime = self.startTime
    hash = {'hour' => startTime.strftime("%I"), 'minute' => startTime.strftime("%M"), 'period' => startTime.strftime("%p")}
    return hash
  end

  def decrypt_attrs
    self.agency_name = Agency.uncrypt(self.agency_name)
    self.week_start_date = Agency.uncrypt(self.week_start_date) 
    self.phoneNumber = Agency.uncrypt(self.phoneNumber).gsub("-","")
    self.imageName = Agency.uncrypt(self.imageName)
    self.stripe_customer_id = Agency.uncrypt(self.stripe_customer_id)
    self.stripe_source = Agency.uncrypt(self.stripe_source)
    self.stripe_subscription_id = Agency.uncrypt(self.stripe_subscription_id)
    # self.deserialize_attrs
  end

  def encrypt_attrs
    self.agency_name = Agency.encrypt(self.agency_name)
    self.week_start_date = Agency.encrypt(self.week_start_date) 
    self.phoneNumber = Agency.encrypt(self.phoneNumber)
    self.imageName = Agency.encrypt(self.imageName)
    self.stripe_customer_id = Agency.encrypt(self.stripe_customer_id)
    self.stripe_source = Agency.encrypt(self.stripe_source)
    self.stripe_subscription_id = Agency.encrypt(self.stripe_subscription_id)
    # self.serialize_attrs
  end
 
  def set_image
    resp = false
    if self.image.present?
      resp = ImageService.new.set_image(self.image, self, "imageName")
    end
    return resp
  end

end
