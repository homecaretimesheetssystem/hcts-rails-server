class CaregoalsCareoption < ApplicationRecord

  belongs_to :care_goal, foreign_key:"caregoals_id", optional:true
  belongs_to :careoption, foreign_key:"careoptions_id", optional:true

end
