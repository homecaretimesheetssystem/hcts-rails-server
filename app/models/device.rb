class Device < ApplicationRecord
  self.table_name = "device"
  belongs_to :user
  has_many :access_tokens

  scope :active, -> { where(isActive:true) }


end
