class County < ApplicationRecord
 self.table_name = "county"
 has_many :pca_counties
 has_many :pcas, through: :pca_counties
 has_many :recipients

end
