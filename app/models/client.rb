class Client < ApplicationRecord
 self.table_name = "client"
 has_many :auth_codes
 has_many :refresh_tokens

 def allowedGrantTypes
   PHP.unserialize(self.allowed_grant_types)
 end

 def redirectUris
   PHP.unserialize(self.redirect_uris)
 end


 def setGrantTypes(arr)
   self.allowed_grant_types = PHP.serialize(arr)
 end

 def setRedirectUris(arr)
   self.redirect_uris = PHP.serialize(arr)
 end

end
