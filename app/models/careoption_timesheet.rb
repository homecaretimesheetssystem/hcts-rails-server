class CareoptionTimesheet < ApplicationRecord
  self.table_name = "careoption_timesheet"
  belongs_to :timesheet
  belongs_to :careoption, foreign_key: "careOption_id"

end
