class AuthCode < ApplicationRecord
  self.table_name = "auth_code"
  belongs_to :client
  belongs_to :user

end
