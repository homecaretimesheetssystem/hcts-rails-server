class Rsvp < ApplicationRecord
  self.table_name = "rsvp"
  belongs_to :pca
  belongs_to :request
  has_one :recipient, through: :request
  belongs_to :sessionDatum, foreign_key:"sessionData_id", optional:true, dependent: :destroy

  scope :is_accepted, -> { where(accepted:true, declined:false) }
  scope :is_declined, -> { where(accepted:false, declined:true) }
  scope :by_pca, ->(pca) { where(pca_id:pca.id) }

  def decline
    self.update(accepted:false, declined:true)
  end
 
  def accept
    self.update(accepted:true, declined:false)
  end


end