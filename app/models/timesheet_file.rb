class TimesheetFile < ApplicationRecord
  self.table_name = "files"
  belongs_to :timesheet

  def self.add_file(file, timesheet_file, property_name)
    resp = false
    if file.present? && timesheet_file.is_a?(TimesheetFile) && property_name.present?
      resp = ImageService.new.set_image(file, timesheet_file, property_name)
      if resp.present?
        timesheet_file[property_name] = resp
        timesheet_file.save
      end
    end
    return resp
  end

end
