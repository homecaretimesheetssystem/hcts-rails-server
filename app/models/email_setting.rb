class EmailSetting < ApplicationRecord
  belongs_to :agency
  validates :email, presence: true

end
