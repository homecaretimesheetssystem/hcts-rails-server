class TimeOut < ApplicationRecord
  self.table_name = "time_out"
  has_one :session_datum, foreign_key: "timeOut_id"

  scope :by_agency, ->(agency_id) { joins(:session_datum => [:timesheets => [:recipient]]).where("recipient.agency_id = ?", agency_id) }


  after_find :decrypt_attrs
  before_save :encrypt_attrs

private

  def decrypt_attrs
    self.timeOutAddress = TimeOut.uncrypt(self.timeOutAddress)
    self.latitude = TimeOut.uncrypt(self.latitude)
    self.longitude = TimeOut.uncrypt(self.longitude)
  end

  def encrypt_attrs
    self.timeOutAddress = TimeOut.encrypt(self.timeOutAddress)
    self.latitude = TimeOut.encrypt(self.latitude)
    self.longitude = TimeOut.encrypt(self.longitude)
  end

end
