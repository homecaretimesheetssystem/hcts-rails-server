class RecipientSignature < ApplicationRecord
 self.table_name = "recipient_signature"
 belongs_to :timesheet

  after_find :decrypt_attrs
  before_save :encrypt_attrs

private

  def decrypt_attrs
    self.recipientSigAddress = Recipient.uncrypt(self.recipientSigAddress)
    self.latitude = Recipient.uncrypt(self.latitude)
    self.longitude = Recipient.uncrypt(self.longitude)
  end
  
  def encrypt_attrs
    self.recipientSigAddress = Recipient.encrypt(self.recipientSigAddress)
    self.latitude = Recipient.encrypt(self.latitude)
    self.longitude = Recipient.encrypt(self.longitude)
  end

end
