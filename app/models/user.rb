class User < ApplicationRecord
 self.table_name = "fos_user"
 has_many :access_tokens
 has_many :auth_codes
 has_many :devices, dependent: :destroy
 has_many :refresh_tokens, dependent: :destroy
 has_one :pca, dependent: :destroy
 has_one :recipient, dependent: :destroy
 belongs_to :agency, optional:true
 belongs_to :admin, optional:true, dependent: :destroy
 
 ROLES = ["ROLE_ADMIN", "ROLE_AGENCY", "ROLE_PCA", "ROLE_RECIPIENT"]
 ADMIN_SELECT_ROLES = ["ROLE_ADMIN", "ROLE_AGENCY"]
 SELECT_ROLES = ["ROLE_AGENCY", "ROLE_PCA", "ROLE_RECIPIENT"]
 AGENCY_ROLE = "ROLE_AGENCY"
 PCA_ROLE = "ROLE_PCA"
 RECIP_ROLE = "ROLE_RECIPIENT"
 ADMIN_ROLE = "ROLE_ADMIN"

 scope :archived, -> { where(archived:true) }
 scope :nonarchived, -> { where(archived:false) }


 validates :firstName, presence:true
 validates :lastName, presence:true
 validates :username, presence:true, uniqueness:true
 validates :email, presence:true, uniqueness:true

 validates :password, presence: { :on => :create }, confirmation: true
 validates :password_confirmation, :presence => true, :if => :password_changed?

 after_initialize :make_salt, if: :new_record?
 before_create :build_password 
 before_save :serialize_roles
 before_save :build_password, if: :password_changed?
 after_find :deserialize_roles
 attr_accessor :current_password

 def roles_to_readable
   if self.roles == User::ROLES[0]
     return "Admin"
   elsif self.roles == User::ROLES[1]
     return "Agency Admin"
   elsif self.roles == User::ROLES[2]
     return "PCA"
   elsif self.roles == User::ROLES[3]
     return "Recipient"
   else
     return ""
   end
 end


 def name
  "#{self.firstName.titleize} #{self.lastName.titleize}"
 end

 def avatar
  image = "default_profile_image.png"
  if self.pca.present?
    if self.pca.profile_photo.present?
       image = "https://s3.amazonaws.com/hc-timesheets-demo/#{self.pca.profile_photo}"
    elsif self.pca.agency.imageName.present?
      image = "https://s3.amazonaws.com/hc-timesheets-demo/#{self.pca.agency.imageName}"
    end
  elsif self.agency.present? && self.agency.imageName.present?
    image = "https://s3.amazonaws.com/hc-timesheets-demo/#{self.agency.imageName}"
  end
  return image
 end

 def authenticate_password(pass)
   salt = self.salt
   salted = "#{pass}{#{salt}}"
   digest = Digest::SHA512.digest(salted)
   i = 1
   while i < 5000 
     digest = Digest::SHA512.digest("#{digest}#{salted}")
     i += 1
   end
   encoded = Base64.strict_encode64(digest)
   return false unless encoded == self.password
   true
 end


 def auth_roles
   PHP.unserialize(self.roles)
 end

 def deserialize_roles
   self.roles = self.auth_roles.first
 end

 def serialize_roles
  if self.roles.in?(User::ROLES)
    self.roles = PHP.serialize([self.roles])
  end
 end

 def get_agency
   if self.is_agent?
     self.agency
   elsif self.is_pca?
     self.pca.agency
   elsif self.is_recipient?
     self.recipient.agency
   else
     nil
   end
 end


 def get_role
   if self.is_admin?
     "Admin"
   elsif self.is_agent?
     "Agency"
   elsif self.is_pca?
     "PCA"
   elsif self.is_recipient?
     "Recipient"
   else
     "Unknown"
   end
 end

 def is_admin?
   self.roles == User::ROLES[0]
 end

 def is_agent?
   self.roles == User::ROLES[1]
 end

 def is_pca?
   self.roles == User::ROLES[2]
 end

 def is_recipient?
   self.roles == User::ROLES[3]
 end

 def userType
   if self.is_pca?
     return "pca"
   elsif self.is_recipient?
     return "recipient"
   else
     return nil
   end
 end

 def set_login
   access_token = nil
   refresh_token = nil
   if self.is_pca? || self.is_recipient?
     access_token = self.access_tokens.new
     access_token.save
     refresh_token = self.refresh_tokens.new
     refresh_token.save
     self.update(last_login:Time.now)
   end
   return [access_token, refresh_token]
 end

 def gen_pass_token
   self.password_requested_at = Time.now
   self.confirmation_token = loop do
     token = self.get_token
     break token unless User.exists?(confirmation_token: token)
   end
   self.save
 end

 private 
  def build_password
   salt = self.salt
   pass = self.password
   salted = "#{pass}{#{salt}}"
   digest = Digest::SHA512.digest(salted)
   i = 1
   while i < 5000 
     digest = Digest::SHA512.digest("#{digest}#{salted}")
     i += 1
   end
   self.password = Base64.strict_encode64(digest)
  end

  def make_salt
    puts "New Salt"
    random_boolean = [false, true, false, false].sample
    final_int = random_boolean ? 29 : 30
    self.salt = SecureRandom.hex(16)[0..final_int] 
  end

end
