class RequestQualification < ApplicationRecord
  belongs_to :request
  belongs_to :qualification, foreign_key:"qualifications_id"

end
