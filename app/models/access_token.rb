class AccessToken < ApplicationRecord
 self.table_name = "access_token"
 belongs_to :user
 belongs_to :device, optional:true
 after_initialize :gen_token

 def expires
   return Time.at(self.expires_at.to_i).to_datetime
 end
 

 def expires_in
   604800
 end

 protected

 def gen_token
   if self.expires_at.nil?
     self.expires_at = DateTime.now.to_i + self.expires_in
   end
   if self.user_id.present? && self.user.devices.any?
     self.device_id = Device.select(:id).where(:user_id => self.user_id).last.id
   end
   if self.token.nil?
     self.token = loop do
       random_token = self.get_token
       break random_token unless AccessToken.exists?(token: random_token)
     end
   end
 end

end
