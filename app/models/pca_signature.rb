class PcaSignature < ApplicationRecord
  self.table_name = "pca_signature"
  belongs_to :timesheet
  has_one :pca, through: :timesheet

  after_find :decrypt_attrs
  before_save :encrypt_attrs

private

  def decrypt_attrs
    self.pcaSigAddress = Pca.uncrypt(self.pcaSigAddress)
    self.latitude = Pca.uncrypt(self.latitude)
    self.longitude = Pca.uncrypt(self.longitude)
  end

  def encrypt_attrs
    self.pcaSigAddress = Pca.encrypt(self.pcaSigAddress)
    self.latitude = Pca.encrypt(self.latitude)
    self.longitude = Pca.encrypt(self.longitude)
  end

end
