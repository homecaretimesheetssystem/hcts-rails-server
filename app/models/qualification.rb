class Qualification < ApplicationRecord
  has_many :pca_qualifications, foreign_key: "qualifications_id"
  has_many :pcas, through: :pca_qualifications
 
  after_find :decrypt_attrs
  before_save :encrypt_attrs

  def name
   self.qualification
  end

private

  def decrypt_attrs
    self.qualification = Qualification.uncrypt(self.qualification)
  end

  def encrypt_attrs
    self.qualification = Qualification.encrypt(self.qualification)
  end

end
