class RefreshToken < ApplicationRecord
  self.table_name = "refresh_token"
  belongs_to :user
  belongs_to :client
  after_initialize :gen_token


  def expires_in
    1810800
  end


protected

 def gen_token
   if self.expires_at.nil?
     self.expires_at = DateTime.now.to_i + self.expires_in
   end
   if self.client_id.nil?
     self.client_id = Client.select(:id).last.id
   end
   if self.token.nil?
     self.token = loop do
       random_token = self.get_token
       break random_token unless RefreshToken.exists?(token: random_token)
     end
   end
 end
  

end
