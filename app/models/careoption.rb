class Careoption < ApplicationRecord
has_many :caregoals_careoptions, foreign_key: 'careoptions_id'
has_many :care_goals, through: :caregoals_careoptions
has_many :careoption_timesheets
has_many :timesheets, through: :careoption_timesheets

after_find :decrypt_attrs
before_save :encrypt_attrs

 def name 
   self.care_option
 end

private

 def decrypt_attrs
   self.care_option = Careoption.uncrypt(self.care_option)
 end

 def encrypt_attrs
   self.care_option = Careoption.encrypt(self.care_option)
 end

end
