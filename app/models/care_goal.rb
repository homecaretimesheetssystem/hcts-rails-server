class CareGoal < ApplicationRecord
  belongs_to :recipient
  has_many :caregoals_careoptions, foreign_key: "caregoals_id"
  has_many :careoptions, through: :caregoals_careoptions
  has_many :goal_care_options, class_name: "Careoption", source: :careoption, through: :caregoals_careoptions, foreign_key: "careoptions_id"
  
  before_destroy :clear_care_options





  private

  def clear_care_options
    self.goal_care_option_ids = []
  end

end
