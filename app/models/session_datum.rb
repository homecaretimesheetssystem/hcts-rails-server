class SessionDatum < ApplicationRecord
  belongs_to :pca
  belongs_to :service, optional:true
  belongs_to :ratio, optional:true, foreign_key:"ratio_id"
  belongs_to :time_in, foreign_key:"timeIn_id", optional:true
  belongs_to :time_out, foreign_key:"timeOut_id", optional:true
  belongs_to :shared_care_location, foreign_key:"sharedCareLocation_id", optional:true
  has_many :timesheets, inverse_of: :session_datum, foreign_key:"sessionData_id"
  has_one :rsvp, foreign_key:"sessionData_id"
  accepts_nested_attributes_for :time_in
  accepts_nested_attributes_for :time_out, allow_destroy:true

  scope :unfinished, -> { joins(:timesheets).where("timesheet.finished = ?", false) }
  scope :ready_to_sign, -> { joins(:timesheets => :timesheet_file).where("files.timesheetPcaSig is not null") }
  scope :for_api, -> { where.not(:continueTimesheetNumber => nil).joins(:timesheets).where("timesheet.recipient_id IS NOT NULL").order(id: :desc) }

  def get_date_ending
    agency = self.pca.agency
    day_name = agency.week_start_date
    start_time = agency.start_time.present? ? agency.start_time : "12:00AM"
    nowish = DateTime.now + 1.day
    count = 0
    while nowish.strftime("%A") != day_name && count < 8
      count += 1
      nowish = nowish + 1.day
    end
    start_array = start_time.split(":00")
    if start_array[0] == "12"
      if start_array[1] == "AM"
        hour = 0
      else
        hour = 12
      end
    elsif start_array[1] == "PM"
      hour = start_array[0].to_i + 12
    else
      hour = start_array[0].to_i
    end
    new_end = nowish.in_time_zone(self.time_zone).change(hour:hour)
    return new_end
  end


end
