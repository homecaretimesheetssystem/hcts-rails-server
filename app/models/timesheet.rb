class Timesheet < ApplicationRecord
  self.table_name = "timesheet"
  has_many :careoption_timesheets
  has_many :careoptions, through: :careoption_timesheets
  has_one :pca_signature
  has_one :recipient_signature
  belongs_to :session_datum, foreign_key:"sessionData_id"
  has_one :pca, through: :session_datum
  belongs_to :recipient
  has_one :agency, through: :recipient
  belongs_to :verification, optional:true
  has_one :timesheet_file
  accepts_nested_attributes_for :session_datum

  scope :archived, -> { where(archived:true) }
  scope :active, -> { where(archived:[false,nil]) }
  scope :finished, -> { where(finished:true) }
  scope :unfinished, -> { where.not(finished:true) }
  scope :ready_to_sign, -> { joins(:session_datum => :time_in) }
  scope :viewable, -> { joins(:session_datum => [:time_in]).joins({:recipient => :user}).joins({:pca => :user}).where('fos_user.archived = ?', false) }
  scope :viewable_w_archive, -> { joins(:session_datum => [:time_in]).joins({:recipient => :user}).joins({:pca => :user}) }
  scope :by_option, -> { joins(:recipient => [:goal_care_options]) }
  scope :by_recipient, ->(recip_ids) { where(recipient_id:recip_ids) }
  scope :by_agency, ->(agency_id) { joins(:recipient).where("recipient.agency_id = ?",agency_id) }
  scope :graphable, -> { finished.joins(:session_datum => :time_in).order("time_in.timeIn") }
  scope :order_by_time_in, -> { joins(:session_datum => :time_in).order("time_in.timeIn DESC") }



  def compare_to_goals
    self.recipient.compare_care_goals(self)
  end

  def self.compare_to_goals(timesheets = [])
    if timesheets.is_a? Timesheet
      timesheets = [timesheets]
    else
      timesheets = timesheets.order(:recipient_id)
    end
    Recipient.compare_care_goals(timesheets)
  end

  def count_care_options
    self.recipient.present? ? self.recipient.count_careoptions(self) : nil
  end

  def self.count_care_options
    Recipent.first.count_care_options(self)
  end

  def get_pdf
    resp = nil
    if self.timesheet_file.present? && self.timesheet_file.timesheetPdfFile.present?
      imgService = ImageService.new
      pdf = imgService.get_file(self.timesheet_file.timesheetPdfFile)
      pname = self.pca.name.downcase.gsub(".","")
      rname = self.recipient.name.downcase.gsub(".","")
      date = self.get_time_in
      name = "#{rname}-#{pname}#{date}".gsub(" ","")
      filename = "#{URI.encode(name)}.pdf"
      resp = [pdf[0],filename,pdf[1]]
    end
    return resp
  end


  def get_csv
    resp = nil
    if self.timesheet_file.present? && self.timesheet_file.timesheetCsvFile.present?
      imgService = ImageService.new
      csv = imgService.get_file(self.timesheet_file.timesheetCsvFile)
      pname = self.pca.name.downcase.gsub(".","")
      rname = self.recipient.name.downcase.gsub(".","")
      date = self.get_time_in
      name = "#{rname}-#{pname}#{date}".gsub(" ","")
      filename = "#{URI.encode(name)}.csv"
      resp = [csv[0],filename,csv[1]]
    end
    return resp
  end


  def get_pca_sig
    resp = nil
    if self.timesheet_file.present? && self.timesheet_file.timesheetPcaSig.present?
      imgService = ImageService.new
      imgObj = imgService.get_image(self.timesheet_file.timesheetPcaSig)
      type_array = imgObj[1].split("/")
      name = self.pca.name.downcase.gsub(".","")
      name = name.gsub(" ","")
      date = self.get_time_in
      file_name = URI.encode("#{name}#{date}")
      filename = "#{file_name}.#{type_array[1]}"
      sig = imgService.rotate_image(imgObj[0], type_array[1])
      resp = [sig, filename, imgObj[1]]
    end
    return resp
  end
 
    def get_rec_sig
    resp = nil
    if self.timesheet_file.present? && self.timesheet_file.timesheetRecSig.present?
      imgService = ImageService.new
      imgObj = imgService.get_image(self.timesheet_file.timesheetRecSig)
      type_array = imgObj[1].split("/")
      name = "recipient-";
      date = self.get_time_in
      file_name = URI.encode("#{name}#{date}")
      filename = "#{file_name}.#{type_array[1]}"
      sig = imgService.rotate_image(imgObj[0], type_array[1])
      resp = [sig, filename, imgObj[1]]
    end
    return resp
  end


  def get_time_in(no_dash = false)
    resp = ""
    if self.session_datum.present? && self.session_datum.time_in.present? && self.session_datum.time_in.timeIn.present?
      date = self.session_datum.time_in.timeIn.strftime("%D").gsub("/","-")
      if no_dash 
        resp = date
      else
        resp = "-#{date}"
      end
    end
    return resp
  end

  

  def self.get_date_range(timesheets = [])
    range = nil
    if timesheets.present?
      if timesheets.is_a? Timesheet
        timesheets = [timesheets]
      elsif !timesheets.is_a?(Array)
        timesheets = timesheets.to_ary
      end
      dates = timesheets.map{|t| t.get_time_in(true)}.sort
      if dates.count > 1 && dates[0] != dates[dates.count - 1]
        range = "#{dates[0].gsub!('-','/')} - #{dates[dates.count - 1].gsub!('-','/')}"
      else
        range = dates[0]
      end
    end
    return range
  end

end
