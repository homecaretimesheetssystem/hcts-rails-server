class SharedCareLocation < ApplicationRecord
  self.table_name = "shared_care_location"
  has_many :session_data

  after_find :decrypt_attrs
  before_save :encrypt_attrs
 
  def name
    self.location.titleize
  end

private

  def decrypt_attrs
    self.location = SharedCareLocation.uncrypt(self.location)
  end

  def encrypt_attrs
    self.location = SharedCareLocation.encrypt(self.location)
  end

end
