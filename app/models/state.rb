class State < ApplicationRecord

 after_find :decrypt_attrs
 before_save :encrypt_attrs

 def name
   self.stateName.titleize
 end

private
 def decrypt_attrs
   self.stateName = State.uncrypt(self.stateName)
 end

 def encrypt_attrs
   self.stateName = State.encrypt(self.stateName)
 end

end
