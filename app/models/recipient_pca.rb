class RecipientPca < ApplicationRecord
  self.table_name = "recipient_pca"
  belongs_to :pca
  belongs_to :recipient
  has_many :verifications, foreign_key:"recipientPca_id"


end
