class Request < ApplicationRecord
  self.table_name = "request"
  belongs_to :recipient
  has_one :county, through: :recipient
  has_many :request_qualifications
  has_many :qualifications, through: :request_qualifications
  has_many :rsvps, dependent: :destroy
  has_many :pcas, through: :rsvps
  belongs_to :requested_pca, class_name: "Pca", optional:true, foreign_key: "requested_id"


  scope :available, -> { where("request.id NOT IN (select request_id from rsvp where rsvp.accepted = true)") }
  scope :not_for_pca, -> { where(requested_id:nil) }
  scope :for_pca, ->(pca) {where(requested_id:pca.id) }
  scope :in_future, -> { where("end_shift_time > ?", Time.now) } 
  scope :in_county_by_pca, ->(pca) { joins(:recipient).where("recipient.county_id in (?)", pca.counties.pluck(:id)) }
  scope :accepted_by, ->(pca) { joins(:rsvps).where("rsvp.accepted = ?", true) }
  scope :by_gender, ->(pca) { where(gender_preference:[pca.gender,0,nil]) }
  scope :by_years_of_exp, ->(pca) { where("years_of_experience = null or years_of_experience <= #{pca.years_of_experience}") }

  scope :available_api, ->(pca) { available.in_future.not_for_pca.in_county_by_pca(pca).by_gender(pca).by_years_of_exp(pca) }
  scope :accepted_api, ->(pca) { in_future.accepted_by(pca) }
  scope :declined, -> { joins(:rsvps).where("rsvp.declined =?", true) }
  scope :direct_available, -> { where("request.id NOT IN (select request_id from rsvp)") }

  after_find :decrypt_attrs
  before_save :encrypt_attrs


  def additional_requirements
    self.description
  end

  def accepted_pca
    pca = nil
    if self.rsvps.is_accepted.any?
      pca = self.rsvps.is_accepted.first.pca
    end
  end


  def send_pca_notifications
    fs = FirebaseService.new
    if self.requested_pca.present? 
      fids = self.requested_pca.user.devices.pluck(:deviceToken)
      return fs.send_direct_notification(fids, self)
    else
      workers = self.recipient.agency.pcas.is_available
      fids = []
      workers.each do |w|
        fids << w.get_firebase_tokens
      end
      fids.flatten!
      return fs.send_pca_notification(fids, self)
    end

  end
  

  def send_pca_accepted
    resp = nil
    fs = FirebaseService.new
    fids = self.recipient.user.devices.pluck(:deviceToken)
    pca = self.accepted_pca
    if fids.present?
      resp = fs.send_pca_accepted(fids, self, pca)
    end
    return resp
  end

  def send_recipient_cancelled
    resp = nil
    fs = FirebaseService.new
    pca = self.accepted_pca
    if pca.present?
      fids = pca.user.devices.pluck(:deviceToken)
      if fids.present?
        resp = fs.send_recipient_cancelled(fids, self)
      end
    end
    return resp
  end
 
  def send_pca_cancelled(pca)
    resp = nil
    fs = FirebaseService.new
    fids = self.recipient.user.devices.pluck(:deviceToken)
    if fids.present?
      resp = fs.send_pca_cancelled(fids, self, pca)
    end
    return resp
  end

  def send_pca_declined
    resp = nil
    fs = FirebaseService.new
    fids = self.recipient.user.devices.pluck(:deviceToken)
    if fids.present?
      resp = fs.send_pca_declined(fids, self, self.requested_pca)
    end
    return resp
  end
    

private

  def decrypt_attrs
    self.address = Request.uncrypt(self.address)
  end

  def encrypt_attrs
    self.address = Request.encrypt(self.address)
  end

end