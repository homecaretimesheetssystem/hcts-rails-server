class PcaCounty < ApplicationRecord
  self.table_name = "pca_county"
  belongs_to :pca
  belongs_to :county

end
