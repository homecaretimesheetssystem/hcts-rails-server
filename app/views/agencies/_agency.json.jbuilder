json.extract! agency, :id, :agency_name, :state_id, :week_start_date, :start_time, :phoneNumber, :imageName, :plan, :stripe_customer_id, :stripe_subscription_id, :created_at, :updated_at
json.url agency_url(agency, format: :json)
