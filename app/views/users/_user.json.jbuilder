json.extract! user, :id, :agency_id, :admin_id, :username, :email, :enabled, :password, :locked, :expired, :roles, :firstName, :lastName, :created_at, :updated_at
json.url user_url(user, format: :json)
