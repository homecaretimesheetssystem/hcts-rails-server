json.extract! timesheet, :id, :recipient_id, :verification_id, :finished, :archived, :sessionData_id, :created_at, :updated_at
json.url timesheet_url(timesheet, format: :json)
