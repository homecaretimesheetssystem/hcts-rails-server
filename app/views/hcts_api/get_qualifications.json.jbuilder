json.array! @qualifications do |qual|
  json.id qual.id
  json.qualification qual.qualification
end