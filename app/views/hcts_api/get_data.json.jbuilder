json.sessionData @sessionData do |sessionDatum|
  json.id sessionDatum.id
  json.continueTimesheetNumber sessionDatum.continueTimesheetNumber.nil? ? 1 : sessionDatum.continueTimesheetNumber
  json.currentTimesheetNumber sessionDatum.currentTimesheetNumber.nil? ? 1 : sessionDatum.currentTimesheetNumber
  json.flagForFurtherInvestigation sessionDatum.flagForFurtherInvestigation
  json.pca do
    json.id sessionDatum.pca.id
    json.first_name sessionDatum.pca.first_name
    json.last_name sessionDatum.pca.last_name
    json.umpi sessionDatum.pca.umpi
    json.company_assigned_id sessionDatum.pca.company_assigned_id
    json.profilePhoto sessionDatum.pca.profile_photo
    json.home_address sessionDatum.pca.home_address
    json.qualifications sessionDatum.pca.qualifications do |q|
      json.id q.id
      json.qualification q.name
    end
    json.available sessionDatum.pca.available
    json.services sessionDatum.pca.services do |s|
      json.id s.id
      json.service_number s.serviceNumber
      json.service_name s.serviceName
    end
    json.years_of_experience sessionDatum.pca.years_of_experience
    json.gender sessionDatum.pca.gender
    json.counties sessionDatum.pca.counties do |c|
      json.id c.id
      json.name c.name
    end
  end 
  if sessionDatum.service.present?
    json.service do 
      json.id sessionDatum.service.id
      json.service_number sessionDatum.service.serviceNumber
      json.service_name sessionDatum.service.serviceName
    end
  end
  if sessionDatum.time_in.present?
    json.timeIn do
      json.id sessionDatum.time_in.id
      json.timeIn SessionDatum.api_format2(sessionDatum.time_in.timeIn)
      json.estimatedShiftLength sessionDatum.time_in.estimatedShiftLength
      if sessionDatum.time_in.timeInAddress.present?
        json.timeInAddress sessionDatum.time_in.timeInAddress
      end
      if sessionDatum.time_in.latitude.present?
        json.latitude sessionDatum.time_in.latitude
      end
      if sessionDatum.time_in.longitude.present?
        json.longitude sessionDatum.time_in.longitude
      end
      if sessionDatum.time_in.timeInPictureTime.present?
        json.timeInPictureTime SessionDatum.api_format2(sessionDatum.time_in.timeInPictureTime)
      end
    end
  end
  if sessionDatum.time_out.present?
    json.timeOut do 
      json.id sessionDatum.time_out.id
      json.timeOut SessionDatum.api_format2(sessionDatum.time_out.timeOut)
      if sessionDatum.time_out.timeOutAddress.present?
        json.timeOutAddress sessionDatum.time_out.timeOutAddress
      end
      if sessionDatum.time_out.latitude.present?
        json.latitude sessionDatum.time_out.latitude
      end
      if sessionDatum.time_out.longitude.present?
        json.longitude sessionDatum.time_out.longitude
      end
      if sessionDatum.time_out.timeOutPictureTime.present?
        json.timeOutPictureTime SessionDatum.api_format2(sessionDatum.time_out.timeOutPictureTime)
      end
    end
  end
  json.timesheets sessionDatum.timesheets do |ts|
    json.id ts.id
    if ts.recipient.present?
      json.recipient do
        json.id ts.recipient.id
        json.skip_verification ts.recipient.skip_verification
        json.skip_gps ts.recipient.skip_gps
        json.first_name ts.recipient.first_name
        json.last_name ts.recipient.last_name
        json.ma_number ts.recipient.ma_number
        json.company_assigned_id ts.recipient.company_assigned_id
        json.gender_preference ts.recipient.gender_preference
        if ts.recipient.county.present?
          json.county do 
            json.id ts.recipient.county.id
            json.name ts.recipient.county.name
          end
        end
      end
    end
    if ts.timesheet_file.present?
      json.file do 
        json.id ts.timesheet_file.id
        if ts.timesheet_file.timesheetPcaSig.present?
          json.timesheetPcaSig ts.timesheet_file.timesheetPcaSig
        end
        if ts.timesheet_file.timesheetRecSig.present?
          json.timesheetRecSig ts.timesheet_file.timesheetRecSig
        end
      end
    end
    json.careOptionTimesheets ts.careoptions do |co|
      json.careOption do
        json.id co.id
        json.is_iadl co.is_iadl
        json.care_option co.care_option
      end
    end
    json.finished ts.finished ? 1 : 0
    if ts.pca_signature.present?
      json.pca_signature do
        json.id ts.pca_signature.id
        json.pcaSigTime SessionDatum.api_format2(ts.pca_signature.pcaSigTime)
        if ts.pca_signature.pcaSigAddress.present?
          json.pcaSigAddress ts.pca_signature.pcaSigAddress
        end
        if ts.pca_signature.latitude.present?
          json.latitude ts.pca_signature.latitude
        end
        if ts.pca_signature.longitude.present?
          json.longitude ts.pca_signature.longitude  
        end
      end
    end
    if ts.recipient_signature.present?
      json.recipient_signature do
        json.id ts.recipient_signature.id
        json.recipientSigTime SessionDatum.api_format2(ts.recipient_signature.recipientSigTime)
        if ts.recipient_signature.recipientSigAddress.present?
          json.recipientSigAddress ts.recipient_signature.recipientSigAddress
        end
        if ts.recipient_signature.latitude.present?
          json.latitude ts.recipient_signature.latitude
        end
        if ts.recipient_signature.longitude.present?
          json.longitude ts.recipient_signature.longitude
        end
      end
    end
  end
  if sessionDatum.time_out.present?
    json.dateEnding SessionDatum.api_format2(sessionDatum.time_out.timeOut)
  end
  ratio = Ratio.find_by(id:sessionDatum.ratio_id)
  if ratio.present?
    json.ratio do
      json.id ratio.id
      json.ratio ratio.name
    end
  end
  if sessionDatum.shared_care_location.present?
    json.sharedCareLocation do
      json.id sessionDatum.shared_care_location.id
      json.location sessionDatum.shared_care_location.location
    end
  end
end
json.user do
  json.id @current_user.id
  json.userType @current_user.userType
  json.email @current_user.email
end
if @current_user.is_pca?
  json.pca do 
    json.id @current_user.pca.id
    json.first_name @current_user.pca.first_name 
    json.last_name @current_user.pca.last_name
    json.umpi @current_user.pca.umpi
    json.company_assigned_id @current_user.pca.company_assigned_id     
    json.profilePhoto @current_user.pca.profile_photo
    json.home_address @current_user.pca.home_address
    json.about_me @current_user.pca.about_me
    json.qualifications @current_user.pca.qualifications do |q|
      json.id q.id
      json.qualification q.name
    end
    json.available @current_user.pca.available
    json.services @current_user.pca.services.nondemand do |ps|
      json.id ps.id
      json.service_number ps.serviceNumber
      json.service_name ps.serviceName
    end
    json.years_of_experience @current_user.pca.years_of_experience
    json.gender @current_user.pca.gender
    json.counties @current_user.pca.counties do |pc|
      json.id pc.id
      json.name pc.name
    end
    json.direct_requests @current_user.pca.get_direct_requests do |rd|
      json.id rd.id
      json.address rd.address
      json.gender_preference rd.gender_preference
      json.startShiftTime Request.api_format(rd.start_shift_time)
      json.endShiftTime Request.api_format(rd.end_shift_time)
      json.years_of_experience rd.years_of_experience
      json.requested_id rd.requested_id
      json.additional_requirements rd.description
      json.direct rd.requested_id == @current_user.pca.id
      json.recipient do
        json.id rd.recipient.id
        json.skip_verification rd.recipient.skip_verification
        json.skip_gps rd.recipient.skip_gps
        json.first_name rd.recipient.first_name
        json.last_name rd.recipient.last_name
        json.ma_number rd.recipient.ma_number
        json.company_assigned_id rd.recipient.company_assigned_id
        json.gender_preference rd.recipient.gender_preference
        if rd.recipient.county.present?
          json.county do 
            json.id rd.recipient.county.id
            json.name rd.recipient.county.name
          end
        end
      end
    end
  end
else
  json.recipient do 
    json.id @current_user.recipient.id
    json.skip_verification @current_user.recipient.skip_verification
    json.skip_gps @current_user.recipient.skip_gps
    json.first_name @current_user.recipient.first_name
    json.last_name @current_user.recipient.last_name
    json.ma_number @current_user.recipient.ma_number
    json.company_assigned_id @current_user.recipient.company_assigned_id
    json.gender_preference @current_user.recipient.gender_preference
    if @current_user.recipient.county.present?
      json.county do
        json.id @current_user.recipient.county.id
        json.name @current_user.recipient.county.name
      end
    end
  end
end
json.agency do 
  json.id @agency.id
  json.agency_name @agency.name
  json.week_start_date @agency.week_start_date
  json.start_time do
    json.hour @start_time["hour"]
    json.minute @start_time["minute"]
    json.period @start_time["period"]
  end
  json.phone_number @agency.phone_number
  json.state do 
    json.id @agency.state.id
    json.state_name @agency.state.name
  end
  json.image_name @agency.image_name
  json.services @agency.services do |as|
    json.id as.id
    json.service_number as.serviceNumber
    json.service_name as.serviceName
  end
end
if @current_user.is_pca?
  json.recipients @current_user.pca.recipients do |pr|
    json.id pr.id
    json.skip_verification pr.skip_verification
    json.skip_gps pr.skip_gps
    json.first_name pr.first_name
    json.last_name pr.last_name
    json.ma_number pr.ma_number
    if pr.county.present?
     counties = [pr.county]
    else 
     counties = []
    end
    json.counties counties do |prc|
      json.id prc.id
      json.name prc.name
    end
    json.careOptions pr.goal_care_options do |co|
      json.id co.id
      json.is_iadl co.is_iadl
      json.care_option co.care_option
    end
  end
else
 json.pcas @current_user.recipient.pcas do |rp|
   json.pca do
     json.id rp.id
     json.first_name rp.first_name
     json.last_name rp.last_name
     json.umpi rp.umpi
     json.company_assigned_id rp.company_assigned_id
     json.profilePhoto rp.profile_photo
     json.qualifications rp.qualifications do |p_qual|
       json.id p_qual.id
       json.qualification p_qual.qualification
     end
     json.available rp.available
     json.services rp.services do |p_serv|
       json.id p_serv.id
       json.service_number p_serv.serviceNumber
       json.service_name p_serv.serviceName
     end
     json.years_of_experience rp.years_of_experience
     json.gender rp.gender
     json.counties rp.counties do |p_county|
       json.id p_county.id
       json.name p_county.name
     end
   end
   json.verification Array.new do 
   end
   json.is_temp false
 end
end
json.careOptions @care_options do |co|
  json.id co.id
  json.is_iadl co.is_iadl
  json.care_option co.care_option
end
json.ratios @ratios do |rat|
  json.id rat.id
  json.ratio rat.name
end
json.sharedCareLocations @care_locations do |scl|
  json.id scl.id
  json.location scl.location
end
json.amazonUrl @current_user.amazonUrl
json.services @services do |as|
  json.id as.id
  json.service_number as.serviceNumber
  json.service_name as.serviceName
end