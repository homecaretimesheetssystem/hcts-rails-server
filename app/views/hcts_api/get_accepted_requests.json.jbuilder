json.array! @requests do |request|
  json.id request.id
  json.recipient do
    json.id request.recipient.id
    json.skip_verification request.recipient.skip_verification
    json.skip_gps request.recipient.skip_gps
    json.first_name request.recipient.first_name
    json.last_name request.recipient.last_name
    json.ma_number request.recipient.ma_number
    json.company_assigned_id request.recipient.company_assigned_id
    json.gender_preference request.recipient.gender_preference
    json.county do
      json.id request.recipient.county.id
      json.name request.recipient.county.name
    end
  end
  json.address request.address
  json.rsvps request.rsvps do |rsvp| 
    json.id rsvp.id
    json.pca do 
      json.id rsvp.pca.id
      json.first_name rsvp.pca.first_name
      json.last_name rsvp.pca.last_name
      json.umpi rsvp.pca.umpi
      json.company_assigned_id rsvp.pca.company_assigned_id
      json.profilePhoto rsvp.pca.profile_photo
      json.home_address rsvp.pca.home_address
      json.about_me rsvp.pca.about_me
      json.phoneNumber rsvp.pca.phoneNumber
      json.email rsvp.pca.user.email
      json.qualifications rsvp.pca.qualifications do |p_qual|
        json.id p_qual.id
        json.qualification p_qual.qualification
      end
      json.available rsvp.pca.available
      json.services rsvp.pca.services do |p_service|
        json.id p_service.id
        json.service_number p_service.serviceNumber
        json.service_name p_service.serviceName
      end
      json.years_of_experience rsvp.pca.years_of_experience
      json.gender rsvp.pca.gender
      json.counties rsvp.pca.counties do |p_county|
        json.id p_county.id
        json.name p_county.name
      end
    end
    json.accepted rsvp.accepted
    json.declined rsvp.declined
    json.session_data_id rsvp.sessionData_id
  end
  json.gender_preference request.gender_preference
  json.startShiftTime Request.api_format(request.start_shift_time)
  json.endShiftTime Request.api_format(request.end_shift_time)
  json.years_of_experience request.years_of_experience
  if request.description.present?
    json.additional_requirements request.description
  end
  json.direct request.requested_id == @pca.id  
  json.requested_id request.requested_id
end