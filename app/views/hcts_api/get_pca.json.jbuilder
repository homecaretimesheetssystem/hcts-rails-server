json.id @pca.id
json.first_name @pca.first_name
json.last_name @pca.last_name
json.umpi @pca.umpi
json.company_assigned_id @pca.company_assigned_id
json.profilePhoto @pca.profile_photo
json.home_address @pca.home_address
json.about_me @pca.about_me
json.phoneNumber @pca.phoneNumber
json.email @pca.user.email
json.qualifications @pca.qualifications do |qual|
  json.id qual.id
  json.qualification qual.qualification
end
json.available @pca.available
json.services @pca.services do |service|
  json.id service.id
  json.service_number service.serviceNumber
  json.service_name service.serviceName
end
json.years_of_experience @pca.years_of_experience
json.gender @pca.gender
json.counties @pca.counties do |county|
  json.id county.id
  json.name county.name
end
