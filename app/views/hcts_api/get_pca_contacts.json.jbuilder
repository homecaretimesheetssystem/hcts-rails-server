json.pcas @pcas do |pca|
  json.id pca.id
  json.name pca.name
  json.phoneNumber pca.phoneNumber
  json.email pca.user.email
  json.first_name pca.first_name
  json.last_name pca.last_name
  json.umpi pca.umpi
  json.company_assigned_id pca.company_assigned_id
  json.profilePhoto pca.profile_photo
  json.home_address pca.home_address
  json.about_me pca.about_me
  json.qualifications pca.qualifications do |p_qual|
    json.id p_qual.id
    json.qualification p_qual.qualification
  end
  json.available pca.available
  json.services pca.services do |p_service|
    json.id p_service.id
    json.service_number p_service.serviceNumber
    json.service_name p_service.serviceName
  end
  json.years_of_experience pca.years_of_experience
  json.gender pca.gender.present? ? pca.gender : 0
  json.counties pca.counties do |p_county|
    json.id p_county.id
    json.name p_county.name
  end
end