json.extract! recipient, :id, :user_id, :agency_id, :skip_verification, :skip_gps, :first_name, :last_name, :ma_number, :company_assigned_id, :gender_preference, :county_id, :home_address, :created_at, :updated_at
json.url recipient_url(recipient, format: :json)
