json.extract! verification, :id, :time, :address, :latitude, :longitude, :start_date, :end_date, :verification_date, :frequency, :verification_photo, :first_time, :current, :verified, :recipientPca_id, :archivedRecipient_id, :archivedPca_id, :created_at, :updated_at
json.url verification_url(verification, format: :json)
