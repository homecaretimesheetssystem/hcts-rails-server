json.extract! pca, :id, :user_id, :agency_id, :firstName, :lastName, :umpi, :company_assigned_id, :phoneNumber, :home_address, :available, :years_of_experience, :gender, :created_at, :updated_at
json.url pca_url(pca, format: :json)
