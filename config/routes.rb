Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

 post 'api/v1/login' => 'hcts_api#login', as: :hcts_api_login
 get 'api/v1/data' => 'hcts_api#get_data', as: :hcts_api_data
 post 'api/v1/create/sessiondata' => 'hcts_api#create_session_data', as: :hcts_api_create_session_data
 post 'api/v1/sessiondata/:id' => 'hcts_api#update_session_data', as: :hcts_api_update_session_data_post
 patch 'api/v1/sessiondata/:id' => 'hcts_api#update_session_data', as: :hcts_api_update_session_data_patch
 post 'api/v1/create/timesheet' => 'hcts_api#create_timesheet', as: :hcts_api_create_timesheet
 post 'api/v1/timesheet/:id' => 'hcts_api#update_timesheet', as: :hcts_api_update_timesheet_post
 patch 'api/v1/timesheet/:id' => 'hcts_api#update_timesheet', as: :hcts_api_update_timesheet_patch
 post 'api/v1/create/time/in' => 'hcts_api#create_time_in', as: :hcts_api_create_time_in
 post 'api/v1/timein/:id' => 'hcts_api#update_time_in', as: :hcts_api_update_time_in_post
 patch 'api/v1/timein/:id' => 'hcts_api#update_time_in', as: :hcts_api_update_time_in_patch
 post 'api/v1/create/time/out' => 'hcts_api#create_time_out', as: :hcts_api_create_time_out
 post 'api/v1/timeout/:id' => 'hcts_api#update_time_out', as: :hcts_api_update_time_out_post
 patch 'api/v1/timeout/:id' => 'hcts_api#update_time_out', as: :hcts_api_update_time_out_patch
 delete 'api/v1/careoptions/:id/delete' => 'hcts_api#empty_careoptions', as: :hcts_api_clear_careoptions
 post 'api/v1/careoptions' => 'hcts_api#set_careoptions', as: :hcts_api_set_careoptions
 post 'api/v1/create/files' => 'hcts_api#create_timesheet_files', as: :hcts_api_create_timesheet_files
 post 'api/v1/files/:id' => 'hcts_api#update_timesheet_files', as: :hcts_api_update_timesheet_files_post
 patch 'api/v1/files/:id' => 'hcts_api#update_timesheet_files', as: :hcts_api_update_timesheet_files_patch
 post 'api/v1/create/pca/signature' => 'hcts_api#create_pca_sign', as: :hcts_api_create_pca_sign
 post 'api/v1/recipientpassword' => 'hcts_api#verify_recipient_pass', as: :hcts_api_verify_recipient_pass
 post 'api/v1/create/recipient/signature' => 'hcts_api#create_recip_sign', as: :hcts_api_create_recip_sign
 post 'api/v1/finish/timesheets' => 'hcts_api#finish_timesheets', as: :hcts_api_finish_timesheets
 post 'api/v1/delete/timesheets' => 'hcts_api#delete_timesheets', as: :hcts_api_delete_timesheets
 get 'api/v1/pcas/:id' => 'hcts_api#get_pca', as: :hcts_api_get_pca
 get 'api/v1/counties' => 'hcts_api#get_counties', as: :hcts_api_get_counties
 get 'api/v1/qualifications' => 'hcts_api#get_qualifications', as: :hcts_api_get_qualifications
 post 'api/v1/devices' => 'hcts_api#set_device', as: :hcts_api_set_device
 post 'api/v1/requests' => 'hcts_api#set_request', as: :hcts_api_set_request
 get 'api/v1/requests/:id' => 'hcts_api#get_request', as: :hcts_api_get_request
 delete 'api/v1/requests/:id' => 'hcts_api#delete_request', as: :hcts_api_delete_request
 get 'api/v1/recipient/:id/requests' => 'hcts_api#get_requests_by_recipient', as: :hcts_api_get_requests_by_recipient
 get 'api/v1/pcas/:pca_id/available_requests' => 'hcts_api#get_available_requests', as: :hcts_api_get_available_requests
 get 'api/v1/pcas/:pca_id/accepted_requests' => 'hcts_api#get_accepted_requests', as: :hcts_api_get_accepted_requests
 post 'api/v1/pcas/:pca_id' => 'hcts_api#update_pca', as: :hcts_api_update_pca_post
 patch 'api/v1/pcas/:pca_id' => 'hcts_api#update_pca', as: :hcts_api_update_pca_patch
 post 'api/v1/pcas/:pca_id/image' => 'hcts_api#set_profile_image', as: :hcts_api_set_profile_image
 post 'api/v1/rsvps' => 'hcts_api#accept_request', as: :hcts_api_accept_request
 delete 'api/v1/rsvp/:rsvp_id' => 'hcts_api#decline_request', as: :hcts_api_decline_request
 get 'api/v1/pca/:pca_id/verify/:recipient_id' => 'hcts_api#need_verif', as: :hcts_api_check_verification
 post 'api/v1/verify/:verif_id' => 'hcts_api#verify', as: :hcts_api_verification_post
 patch 'api/v1/verify/:verif_id' => 'hcts_api#verify', as: :hcts_api_verification_patch
 post 'api/v1/verifcation/:verif_id/photo' => 'hcts_api#verification_photo', as: :hcts_api_verification_photo
 get 'api/v1/pca_contacts' => 'hcts_api#get_pca_contacts', as: :hcts_api_pca_contacts

 
 

 get 'recipient_dashboard' => 'home#recipient_dashboard', as: :recipient_dashboard
 get 'dashboard' => 'home#dashboard', as: :dashboard
 get 'privacy_policy' => 'home#privacy_policy', as: :privacy_policy
 get 'login' => 'home#login', as: :login
 post 'login' => 'home#login_post', as: :login_post
 get 'logout' => 'home#logout', as: :logout
 get 'sidebar-me' => 'home#testpage', as: :test_page
 get 'settings' => 'home#settings_redirect', as: :settings_redirect
 get 'users/archive/:id' => 'users#archive', as: :archive_user
 get 'users/dearchive/:id' => 'users#dearchive', as: :dearchive_user
 get 'agencies/:id/email' => 'agencies#email_settings', as: :agency_email_settings
 post 'agencies/:id/email' => 'agencies#email_submit', as: :agency_email_submit
 delete 'agencies/:agency_id/email/:id' => 'agencies#email_delete', as: :agency_email_delete
 get 'agencies/:id/subscription' => 'agencies#subscription', as: :agency_subscription_settings
 post 'agencies/:id/cancel_subscription' => 'agencies#cancel_subscription', as: :agency_cancel_subscription
 post 'agencies/:id/subscription' => 'agencies#subscription_submit', as: :agency_subscription_submit
 get 'agencies/:id/image' => 'agencies#get_image', as: :get_agency_image
 get 'assignments/' => 'assignments#index', as: :assignments
 get 'assignments/' => 'assignments#index', as: :assignments_index
 get 'assignments/new' => 'assignments#new', as: :new_assignment
 get 'assignments/:id/edit' => 'assignments#edit', as: :edit_assignment
 post 'assignments/new' => 'assignments#create', as: :recipient_pcas
 post 'assignments/:id/edit' => 'assignments#update', as: :update_assignment
 delete 'assignments/:id' => 'assignments#destroy', as: :destroy_assignment 
 get 'timesheets/get-zip' => 'timesheets#download_zip', as: :get_zip
 get 'timesheets/archive-all' => 'timesheets#archive_all', as: :archive_timesheets
 get 'timesheets/:id/archive' => 'timesheets#archive', as: :archive_timesheet
 get 'timesheets/:id/worker-sig' => 'timesheets#get_pca_sig', as: :get_pca_sig
 get 'timesheets/:id/recipient-sig' => 'timesheets#get_rec_sig', as: :get_rec_sig
 get 'timesheets/:id/download-pdf' => 'timesheets#download_pdf', as: :get_pdf
 get 'timesheets/:id/download-csv' => 'timesheets#download_csv', as: :get_csv
 get 'timesheets/:id/download-worker-sig' => 'timesheets#download_pca_sig', as: :download_pca_sig
 get 'timesheets/:id/download-recipient-sig' => 'timesheets#download_rec_sig', as: :download_rec_sig
 get 'verifications/:id/get-image' => 'verifications#get_image', as: :get_verif_image
 get 'get_time_in_form' => 'timesheets#get_time_in_form', as: :get_time_in_form

 get 'goals/' => 'goals#index', as: :goals_index
 get 'goals/new' => 'goals#new', as: :new_goal
 get 'goals/:id/edit' => 'goals#edit', as: :edit_goal
 post 'goals/new' => 'goals#create', as: :care_goals
 patch 'goals/:id' => 'goals#update', as: :care_goal
 get 'cares/' => 'care#index', as: :cares_index

 get 'users/:id/reset_pass' => 'users#reset_password', as: :reset_password
 post 'users/:id/reset_pass' => 'users#set_password', as: :set_password

 get 'forgot' => 'home#forgot', as: :forgot
 post 'forgot' => 'home#forgot_post', as: :forgot_post
 get 'reset' => 'home#reset_pass', as: :reset_pass
 post 'reset' => 'home#reset_pass_post', as: :reset_pass_post


  resources :verifications
  resources :timesheets
  resources :recipients
  resources :pcas
  resources :agencies
  resources :users
 get 'sign-up' => 'home#signup', as: :sign_up
 root 'home#login'


end
